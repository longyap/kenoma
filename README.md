# Kenoma

Instance-independent client for [Pleroma](https://pleroma.social/). Built with [Ionic React](https://ionicframework.com/docs/react)

![screenshot](https://i.imgur.com/7Mah3X7.png)

## Requirements
You will need the following things installed on your computer.

* [Git](https://git-scm.com/downloads)
* [Node.js](https://nodejs.org/en/)

## Usage
 
To start Kenoma locally on http://localhost:3000
``` bash
# install dependencies
npm install -g yarn @ionic/cli
yarn

# serve with hot reload at localhost:8080
yarn start

# build for production with minification
yarn build

# run tests
yarn test

#linting
yarn lint
```

## Note
 Uses [Pleroma-api](https://git.pleroma.social/pleroma/pleroma-api) as a dependency. 
