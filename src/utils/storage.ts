import { Plugins } from '@capacitor/core';
import { getPlatforms } from '@ionic/react';
const { Storage } = Plugins
export const STORAGE_KEY = 'kenoma'

class StorageClass {
  oauth?: { userToken: string } = {
    userToken: '',
  }
  config: { locale: string, platform: string[], instance: string, settings: any } = {
    locale: '',
    platform: getPlatforms(),
    instance: '',
    settings: {}
  }
}
export const getStorage = async () => {
  const storage = await Storage.get({ key: STORAGE_KEY })
  return ((storage.value && !!storage.value) ? JSON.parse(storage.value) : new StorageClass() as StorageClass)
}

export const setConfig = async (config: Object) => {
  const storage = await getStorage()
  await Storage.set({ key: STORAGE_KEY, value: JSON.stringify({ ...storage, config: { ...storage.config, ...config }})})
}

export const setOauth = async (oauth: Object) => {
  const storage = await getStorage()
  await Storage.set({ key: STORAGE_KEY, value: JSON.stringify({ ...storage, oauth }) })
}
