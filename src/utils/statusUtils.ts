import { find, uniqBy } from 'lodash';
import Status from '../entities/status'
import Mention from '../entities/mention'
import { RefObject } from 'react';
import { AUTOCOMPLETE_TYPES } from '../constants';

// TODO: tests for this function are missed
export const containMutedWords = (status: Status, muteWords: string[]) => {
  const item = new Status(status)
  const statusText = (item.text || '').toLowerCase()
  const statusSummary = (item.summary || '').toLowerCase()

  return muteWords ? muteWords.filter((muteWord) => (
    statusText.includes(muteWord.toLowerCase()) || statusSummary.includes(muteWord.toLowerCase())
  )) : []
}

export const createAutocompleteRequest = (text: string, selectionIndex: number): { type: string, request: string } | null => {
  const words = text.split(' ')
  if (!words.length || text[selectionIndex - 1] === ' ') return null
  let searchWord = ''
  words.reduce((acc, word, currentIndex) => {
    const commonLength = acc.length + word.length + currentIndex
    if (commonLength >= selectionIndex && !searchWord.length) {
      searchWord = word
    }
    return acc.concat(word)
  }, '')
  const detectedAutocompleteType = find(AUTOCOMPLETE_TYPES, ({ char }: { char: string }) => searchWord.includes(char))

  if (searchWord && detectedAutocompleteType) {
    let request = ''
    if (searchWord[0] === AUTOCOMPLETE_TYPES[detectedAutocompleteType.name].char) {
      request = searchWord.slice(1)
    } else if (detectedAutocompleteType.name === AUTOCOMPLETE_TYPES.emojis.name) {
      const parts = searchWord.split(detectedAutocompleteType.char)
      request = parts[parts.length - 1]
    } else {
      request = searchWord
    }
    return { type: detectedAutocompleteType.name, request }
  } else {
    return null
  }
}

export const insertAutocompleteResult = (
  text: string,
  insertText: string,
  selectionIndex: number,
  autocompleteType: string,
  simpleInsert?: boolean
)
  : { text: string, selectionIndex: number } => {
  if (simpleInsert) {
    return {
      text: `${text.slice(0, selectionIndex)} ${insertText} ${text.slice(selectionIndex)}`,
      selectionIndex: selectionIndex + insertText.length + 2
    }
  }
  const { char } = AUTOCOMPLETE_TYPES[autocompleteType]
  const words = text.split(' ')
  if (!words.length) return { text, selectionIndex }
  let selectionIndexRes = 0
  const textRes = words.reduce((acc, word, currentIndex) => {
    const commonLength = acc.length + word.length
    if (commonLength >= selectionIndex && !selectionIndexRes) {
      const parts = word.split(char)
      const newText = `${parts[0]}${autocompleteType !== AUTOCOMPLETE_TYPES.emojis.name ? char : ''}${insertText} `

      selectionIndexRes = acc.length + newText.length
      return acc.concat(newText)
    }
    return acc.concat(currentIndex !== words.length - 1 ? `${word} ` : word)
  }, '')

  return {
    text: textRes,
    selectionIndex: selectionIndexRes
  }
}

export const getStatusMentionsList = (status?: Status): Mention[] => {
  if (!status) return []
  const result = status.mentions ? [...status.mentions] : []

  if (status.reblog) {
    result.push(new Mention(status.reblog.account))
    if (status.reblog.pleroma && status.reblog.pleroma.in_reply_to_account_acct) {
      result.push(new Mention({
        acct: status.reblog.pleroma.in_reply_to_account_acct,
        id: status.reblog.in_reply_to_account_id || ''
      }))
    }
    if (status.reblog.mentions) {
      result.push(...status.reblog.mentions)
    }
  } else {
    result.push(new Mention(status.account))
  }
  return uniqBy(result, 'id')
}

export const focusReactRefField = ({ current }: RefObject<any>, selectionStart: number) => {
  if (!current) return;
  current.focus()
  current.selectionStart = selectionStart
  current.selectionEnd = selectionStart
}
