import isEmpty from 'lodash/isEmpty'

const commonReq = (value: any) => {
  if (Array.isArray(value)) return !!value.length
  if (value === undefined || value === null) {
    return false
  }

  if (value === false) {
    return true
  }

  if (value instanceof Date) {
    // invalid date won't pass
    return !isNaN(value.getTime())
  }

  if (typeof value === 'object') {
    for (const _ in value) return true
    return false
  }

  return !!String(value).length
}
/* RULES */
export const required = (val: any): boolean => typeof val === 'string' ? commonReq(val.trim()) : val.trim()
export const sameAs = (val: any, sameAsVal: any): boolean => val === sameAsVal
/* END OF RULES */
const RULES: {[key: string]: any} = {
  required,
  sameAs
}

const validationErrors: any = {
  required: () => 'This field is required',
  sameAs: (fieldName: string, sameAsField: any) => `Should be the same as ${sameAsField}`
}

export const getValidationError = (validatorName: string, fieldName?: string, ruleArguments?: any): string => validationErrors[validatorName](fieldName, ruleArguments)

export const validate = (state: {[key: string]: any}, validator: {[key: string]: (string | string[])[]}): {[key: string]: string[]} | boolean => {
  const errorsObject = Object.fromEntries(Object.entries(validator)
    .map(([field, rule]) => {
      const errorMessage = rule
        .map((rule: string | string[]) => {
          const [ruleName, ...ruleArgs] = Array.isArray(rule) ? rule : [rule]
          if (Array.isArray(rule) && ruleArgs) {
            const validationRes = !RULES[ruleName](state[field], ...ruleArgs.map((ruleField: string) => state[ruleField]))

            return validationRes ? getValidationError(ruleName, field, ...ruleArgs) : null
          } else {
            const validationRes = !RULES[ruleName](state[field])

            return validationRes ? getValidationError(ruleName, field) : null
          }
        })
        .filter(val => val)
      return [field, errorMessage]
    })
    .filter(([field, errors]) => errors.length)
  )
  return isEmpty(errorsObject) ? true : errorsObject
}