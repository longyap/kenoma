import { IntlShape } from 'react-intl';
import { uiActions } from '../reducers/ui'

export const notFoundErrorCatcher = (error: Error, entity: string, dispatch: Function, history: any, intlProp: IntlShape, route: String = '/') => {
  if (error.name === 'NotFoundError'){
    history.push(route)
    dispatch(uiActions.openAlert({
      type: 'danger',
      message: intlProp.formatMessage({
        id: `error.${entity}NotFound`,
        defaultMessage: `${entity[0].toUpperCase().concat(entity.slice(1))} not found`
      }) }
    ))
  }
}
