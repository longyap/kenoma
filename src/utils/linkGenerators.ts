export const generateProfileLink = (id: string, screenName: string): string => {
  if (!id) {
    throw Error('user id should be sent')
  }
  const complicated = !screenName || isExternal(screenName)
  return `/users/${complicated ? id : screenName}`
}

const isExternal = (screenName: string) => screenName && screenName.includes('@')

export const generateTagLink = (tag: string): string => tag ? `/tag/${tag}` : '#'
