import Mention from "../entities/mention"

export const mentionMatchesUrl = (mention: Mention, url: string, instanceName: string): boolean => {
  let [username, instance] = mention.acct.split('@') // tslint:disable-line
  if (!instance) {
    instance = instanceName.replace(/(^\w+:|^)\/\//, '')
  }
  const matchstring = new RegExp(`://${instance}/.*${username}$`, 'g')
  return !!url.match(matchstring)
}

export const isTag = (target: any): boolean => (target.rel.match(/(?:^|\s)tag(?:$|\s)/) || target.className.match(/hashtag/))

export const extractTagFromUrl = (url: string): string | boolean => {
  const regex = /tag[s]*\/(\w+)$/g
  const result = regex.exec(url)

  return result ? result[1] : false
}
