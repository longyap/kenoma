export const hasOverflow = (el: HTMLElement) => el.clientHeight < el.scrollHeight

export const isScrolledDown = (el: HTMLElement) => {
  const bottom = el.scrollTop + el.clientHeight

  return bottom >= el.scrollHeight - 50
}

export const isScrolledUp = (el: HTMLElement) => el.scrollTop < 200

export const scrollDown = (el: HTMLElement) => el.scrollTop = el.scrollHeight - el.clientHeight

export const scrollTo = (el: HTMLElement, scrollTop: number) => el.scrollTop = scrollTop
