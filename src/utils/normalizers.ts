import { CustomEmoji } from "emoji-picker-element/shared"

export const convertPleromaEmojiToMastodon = (data: any): CustomEmoji  => ({
  name: data.shortcode,
  shortcodes: data.shortcode ? [data.shortcode] : [],
  url: data.url
})
