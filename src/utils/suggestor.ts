import Account from "../entities/account"

export const getUserSuggestions = (q: string, data?: Account[]) => {
  if (!q || !data) return []

  const result = data
    .map(account => ({
      ...account,
      acct_lc: account.acct ? account.acct.toLowerCase() : '',
      display_name_lc: account.display_name ? account.display_name.toLowerCase() : ''
    }))
    .filter(user =>
      user.acct_lc.startsWith(q) ||
      user.display_name_lc.startsWith(q)
    )
    .slice(0, 20)
    .sort((a, b) => {
      let aScore = 0
      let bScore = 0

      aScore += a.acct_lc.startsWith(q) ? 2 : 0
      bScore += b.acct_lc.startsWith(q) ? 2 : 0
      aScore += a.display_name_lc.startsWith(q) ? 1 : 0
      bScore += b.display_name_lc.startsWith(q) ? 1 : 0

      const diff = (bScore - aScore) * 10

      const nameAlphabetically = a.display_name > b.display_name ? 1 : -1
      const screenNameAlphabetically = a.acct > b.acct ? 1 : -1

      return diff + nameAlphabetically + screenNameAlphabetically
    })

  return result
}

export const getDomainSuggestions = (q: string, data?: String[]) => {
  if (!q || !data) return []

  return data.filter(domain => domain.toLowerCase().includes(q))
}
