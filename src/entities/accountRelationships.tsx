export default class AccountRelationships {
  following: boolean = false
  followed_by: boolean = false
  blocking: boolean = false
  muting: boolean = false
  requested: boolean = false
  constructor (data?: Partial<AccountRelationships>) {
    Object.assign(this, data)
  }
}
