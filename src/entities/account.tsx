import Status from "./status"
import AccountRelationships from "./accountRelationships"
import isEmpty from 'lodash/isEmpty'

export default class Account {
  relationships?: AccountRelationships
  id: string = ''
  acct: string = ''
  avatar: string = ''
  avatar_static: string = ''
  display_name: string = ''
  statuses_count: number = 0
  followers_count: number = 0
  following_count: number = 0
  note?: string = ''
  created_at?: string = ''
  header_static?: string = ''
  locked: boolean = false
  statuses: Status[] = []
  followers: Account[] = []
  following: Account[] = []
  pleroma?: {
    unread_conversation_count: number
    unread_notifications_count: number,
    relationship?: any,
    hide_followers: boolean,
    hide_followers_count: boolean
    hide_follows: boolean
    hide_follows_count: boolean
  } = {
    unread_conversation_count: 0,
    unread_notifications_count: 0,
    hide_followers: false,
    hide_followers_count: false,
    hide_follows: false,
    hide_follows_count: false
  }

  constructor (data?: Partial<Account>) {
    Object.assign(this, data)
    if (this.pleroma && this.pleroma.relationship && !isEmpty(this.pleroma.relationship)) {
      this.relationships = { ...this.pleroma.relationship }
      delete this.pleroma.relationship
    }
  }
}