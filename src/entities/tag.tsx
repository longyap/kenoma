export default class Tag {
  name: string = ''
  url: string = ''
  history?: any[]
  constructor (data?: Partial<Tag>) {
    Object.assign(this, data)
  }
}
