export default class Mention {
  id: string = ''
  username: string = ''
  acct: string = ''
  url: string = ''

  constructor (data?: Partial<Mention>) {
    Object.assign(this, data)
  }
}
