import Account from './account'
import Attachment from './attachment'
import Mention from './mention'
import EmojiReaction from './emoji_reaction'
import Tag from './tag'

export default class Status {
  id: string = ''
  uri: string = ''
  created_at: string =  ''
  account: Account = new Account()
  content: string = ''
  visibility: string = 'public'
  sensitive: boolean = false
  spoiler_text: string = ''
  media_attachments: Attachment[] = []
  application?: any
  mentions: Mention[] = []
  tags: Tag[] = []
  // TODO: replace with Emoji class
  emojis: any[] = []
  reblogs_count: number = 0
  favourites_count: number = 0
  replies_count: number = 0
  url?: string
  in_reply_to_id?: string | null
  in_reply_to_account_id?: string | null
  // TODO: replace with poll class
  poll?: any
  reblog?: Status
  language?: string
  favourited: boolean = false
  reblogged: boolean = false
  muted: boolean = false
  bookmarked: boolean = false
  pinned: boolean = false
  pleroma?: {
    local: boolean
    conversation_id?: string
    direct_conversation_id?: string
    in_reply_to_account_acct?: string
    expires_at?: string
    thread_muted?: boolean
    emoji_reactions: EmojiReaction[]
    content?: any
    spoiler_text?: any
  }
  // kenoma-only
  text: string = ''
  summary: string = ''
  favourited_by?: Account[]
  reblogged_by?: Account[]
  context?: {
    ancestors: Status[]
    descendants: Status[]
  }

  // for API requests
  status?: string
  media_ids?: string[]
  media?: any[]

  constructor (data?: Partial<Status>) {
    Object.assign(this, data)
    this.text = this.pleroma && this.pleroma.content ? this.pleroma.content['text/plain'] : this.content
    this.summary = this.pleroma && this.pleroma.spoiler_text ? this.pleroma.spoiler_text['text/plain'] : this.spoiler_text
  }
}
