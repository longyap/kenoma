import Account from './account'
import Status from './status'

export default class Notification {
  id: string = ''
  type: string = ''
  created_at: string = ''
  account: Account = new Account()
  status?: Status = new Status()
  remote_url?: string
  url?: string
  pleroma: { is_seen: boolean } = { is_seen : false }
  emoji?: string

  constructor (data?: Partial<Notification>) {
    Object.assign(this, data)
  }
}
