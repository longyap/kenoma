import Account from "./account"

export default class EmojiReaction {
  name: string = ''
  count: number = 0
  me: boolean = false
  accounts?: Account[]

  constructor (data?: Partial<EmojiReaction>) {
    Object.assign(this, data)
  }
}
