import Account from './account'
import Status from './status'
import Tag from './tag'

export default class SearchResult {
  hashtags: Tag[] = []
  accounts: Account[] = []
  statuses: Status[] = []

  constructor (data?: Partial<Notification>) {
    Object.assign(this, data)
  }
}
