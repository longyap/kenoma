export default class Attachment {
  id: string = ''
  type: string = ''
  remote_url?: string
  url?: string
  pleroma?: any

  constructor (data?: Partial<Attachment>) {
    Object.assign(this, data)
  }
}
