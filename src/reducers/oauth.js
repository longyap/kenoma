const initialState = {
  clientId: undefined,
  clientSecret: undefined,
  userToken: undefined,
  appToken: undefined,
  authorizationCode: undefined,
  error: undefined
}

var reducers = {}

reducers.addApp = (state, { app }) => ({
  ...state,
  clientId: app.client_id,
  clientSecret: app.client_secret,
  redirectUri: app.redirect_uri
})

reducers.setApp = (state, { app }) => ({
  ...state,
  ...app
})

reducers.setAuthorizationCode = (state, { code }) => ({
  ...state,
  authorizationCode: code
})

reducers.setUserToken = (state, { token }) => ({
  ...state,
  userToken: token
})

reducers.setError = (state, { error }) => ({
  ...state,
  error
})

reducers.clearApp = (state) => initialState

export const oauthActions = {
  addApp: (app) => ({
    type: 'addApp',
    payload: { app }
  }),
  setApp: (app) => ({
    type: 'setApp',
    payload: { app }
  }),
  setAuthorizationCode: (code) => ({
    type: 'setAuthorizationCode',
    payload: { code }
  }),
  setUserToken: (token) => ({
    type: 'setUserToken',
    payload: { token }
  }),
  setError: (error) => ({
    type: 'setError',
    payload: { error }
  }),
  clearError: () => ({
    type: 'setError',
    payload: undefined
  }),
  clearApp: () => ({
    type: 'clearApp',
    payload: {}
  })
}

export const oauthReducer = (state = initialState, action) => {
  const fn = reducers[action.type] || ((state) => state)
  return fn(state, action.payload)
}
