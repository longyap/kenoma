const initialState = {
  tabs: {
    timeline: false
  },
  alert: {
    isOpen: false,
    type: '',
    message: ''
  }
}

const reducers = {}

reducers.setScrollPosition = (state, { tab, position }) => ({
  ...state,
  tabs: {
    ...state.tabs,
    [tab]: position
  }
})
reducers.openAlert = (state, { type, message }) => ({
  ...state,
  alert: { isOpen: true, type, message }
})
reducers.closeAlert = (state) => ({
  ...state,
  alert: { ...initialState.alert }
})

export const uiActions = {
  setScrollPosition: (tab, position) => ({
    type: 'setScrollPosition',
    payload: { tab, position }
  }),
  openAlert: ({ type, message }) => ({
    type: 'openAlert',
    payload: { type, message }
  }),
  closeAlert: () => ({
    type: 'closeAlert'
  })
}

export const uiReducer = (state = initialState, action) => {
  const fn = reducers[action.type] || ((state) => state)

  return fn(state, action.payload)
}
