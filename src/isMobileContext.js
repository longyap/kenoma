import React from 'react'

export const IsMobileContext = React.createContext({ isMobile: false })
