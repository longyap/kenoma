import { setConfig, setOauth, getStorage } from './../utils/storage';
import pleroma from 'pleroma-api'
import { oauthActions } from '../reducers/oauth'
import reduce from 'lodash/reduce'
import Account from '../entities/account';

const gotoLogin = ({ instance, clientId, redirectUri }: any) => {
  const data = {
    response_type: 'code',
    client_id: clientId,
    redirect_uri: redirectUri,
    scope: 'read write follow'
  }

  const dataString = reduce(data, (acc, v, k) => {
    const encoded = `${k}=${encodeURIComponent(v)}`

    return acc ? `${acc}&${encoded}` : encoded
  }, '')

  const url = `${instance}/oauth/authorize?${dataString}`
  window.location.href = url
}

class OauthLoginParams {
  client_name?: string
  username?: any
  password?: any
  instance?: any
}

export const oauthThunks = {
  setInstance: ({ instance } : { instance: string }) => {
    return async (dispatch: Function, getState: Function) => {
      const config = { config: { instance }}

      setConfig(config.config)
      dispatch(pleroma.reducers.api.actions.addConfig(config))
    }
  },
  createApp: ({ config, params }: { config: Object, params: Object }) => {
    return async (dispatch: Function, getState: Function) => {
      try{
        const result = await pleroma.api.oauth.createApp({ config, params })
        if (!result || result.state !== 'ok') {
          dispatch(oauthActions.clearApp())
          dispatch(oauthActions.setError('Couldn\'t register application'))
          return
        }
        const app = result.data
        dispatch(oauthActions.addApp(app))
        return result
      } catch (e) {
        dispatch(oauthActions.clearApp())
        dispatch(oauthActions.setError('Couldn\'t register application'))
        throw e
      }

    }
  },
  gotoLogin: ({ params }: { params: OauthLoginParams }) => async (dispatch: Function, getState: Function) => {
    const config = getState().api.config
    try {
      const result = await dispatch(oauthThunks.createApp({ config, params }))
      const app = getState().oauth
      await setOauth(app)
      if (!result || result.state !== 'ok') return
      gotoLogin({
        ...app,
        instance: config.instance
      })
    } catch (e) {
      throw(e)
    }
  },
  loginWithUsername: ({ params } : { params: OauthLoginParams }) => async (dispatch: Function, getState: Function) => {
    const config = getState().api.config
    const createAppRes = await dispatch(oauthThunks.createApp({ config, params: { client_name: params.client_name } }))
    const app = getState().oauth

    await setOauth(app)
    if (createAppRes.state !== 'ok') return
    const tokenRes = await pleroma.api.oauth.getTokenWithPassword({ config, params: { ...params, client_id: app.clientId, client_secret: app.clientSecret } })

    if (tokenRes.state !== 'ok') {
      dispatch(oauthActions.clearApp())
      dispatch(oauthActions.setError('Couldn\'t get access token'))
      return
    }
    const token = await dispatch(pleroma.reducers.api.actions.addConfig({ config: { accessToken: tokenRes.data.access_token } }))

    await dispatch(oauthActions.setUserToken(token.payload.config.accessToken))
    await dispatch(oauthThunks.verifyCredentials())
    setOauth(getState().oauth)
    return getState()
  },
  verifyCredentials: () => async (dispatch: Function, getState: Function) => {
    const config = getState().api.config
    const { state, data }: { state: string, data: Account } = await pleroma.api.users.verifyCredentials({ config })
    if (state !== 'ok') {
      dispatch(oauthActions.clearApp())
      dispatch(oauthActions.setError('Invalid token, log in again'))
      return
    }
    return dispatch(pleroma.reducers.users.actions.setCurrentUser({ user: data }))
  },
  updateCredentials: (params: FormData) => async (dispatch: Function, getState: Function) => {
    const config = getState().api.config
    const { state, data }: { state: string, data: Account} = await pleroma.api.users.updateCredentials({ config, params })

    if (state !== 'ok') {
      dispatch(oauthActions.clearApp())
      dispatch(oauthActions.setError('Invalid token, log in again'))
      return
    }
    return dispatch(pleroma.reducers.users.actions.setCurrentUser({ user: data }))
  },
  updateCurrentUser: (options?: { isNotificationsRoute?: boolean, isConversationsRoute?: boolean }) =>
    async (dispatch: Function, getState: Function) => {
      const { api: { config }, users: { currentUser } } = getState()
      if (!currentUser || !config) return getState()

      const { data } = await pleroma.api.users.get({ config, params: { id: currentUser.id } })
      const unreadNotificationsCount = data.pleroma ? data.pleroma.unread_notifications_count : 0

      if (currentUser.pleroma.unread_notifications_count !== unreadNotificationsCount && (!options || !options.isNotificationsRoute)) {
        dispatch(pleroma.reducers.users.actions.updateUnreadNotificationsCount({ unreadNotificationsCount }))
      }
  },
  setCodeAndGetToken: ({ code }: { code: String }) => async (dispatch: Function, getState: Function) => {
    await dispatch(oauthActions.setAuthorizationCode(code))
    const config = getState().api.config
    const { clientId, clientSecret } = getState().oauth
    const params = {
      client_id: clientId,
      client_secret: clientSecret,
      code
    }
    const result = await pleroma.api.oauth.getTokenWithCode({ config, params })
    if (result.state !== 'ok') {
      dispatch(oauthActions.clearApp())
      dispatch(oauthActions.setError('Couldn\'t get access token'))
      return
    }
    await dispatch(pleroma.reducers.api.actions.addConfig({ config: { accessToken: result.data.access_token } }))
    await dispatch(oauthActions.setUserToken(result.data.access_token))
    await dispatch(oauthThunks.verifyCredentials())
    setOauth(getState().oauth)
  },
  logout: () => async (dispatch: Function, getState: Function) => {
    const { api: { config }, oauth: { clientId, clientSecret, userToken } } = getState()
    const params = {
      client_id: clientId,
      client_secret: clientSecret,
      token: userToken
    }
    try {
      await pleroma.api.oauth.revokeToken({ config, params })
    } catch (e) {
      console.error('Server error')// tslint:disable-line
    }
    await Promise.all([
      dispatch(oauthActions.clearApp()),
      dispatch(pleroma.reducers.users.actions.setCurrentUser({ user: null })),
      dispatch(pleroma.reducers.notifications.actions.clearNotifications()),
      dispatch(pleroma.thunks.api.stopAllFetchers())
    ])
    setOauth(getState().oauth)
  },
  signUp: (params: any) => async (dispatch: Function, getState: Function) => {
    const config = getState().api.config
    const createAppRes = await dispatch(oauthThunks.createApp({ config, params: { client_name: `Kenoma_${Date.now()}` } }))
    const app = getState().oauth

    await setOauth(app)
    if (createAppRes.state !== 'ok') return
    const tokenRes = await pleroma.api.oauth.getTokenWithCode({ config, params: { grant_type: 'client_credentials', client_id: app.clientId, client_secret: app.clientSecret } })

    if (tokenRes.state !== 'ok') {
      dispatch(oauthActions.clearApp())
      dispatch(oauthActions.setError('Couldn\'t get access token'))
      return
    }
    await dispatch(pleroma.reducers.api.actions.addConfig({ config: { accessToken: tokenRes.data.access_token } }))
    const result = await pleroma.api.users.register({ config: getState().api.config, params })
    if (result.state !== 'ok') {
      dispatch(oauthActions.clearApp())
      dispatch(oauthActions.setError(JSON.parse(result.data.error) || 'Couldn\'t register'))
      throw JSON.parse(result.data.error) || 'Couldn\'t register'
    }
    await dispatch(pleroma.reducers.api.actions.addConfig({ config: { accessToken: result.data.access_token } }))
    await dispatch(oauthActions.setUserToken(result.data.access_token))
    setOauth({ userToken: result.data.access_token })
    return result
  },
  changePassword: (params: any) => async (dispatch: Function, getState: Function) => {
    const config = getState().api.config
    const result = await pleroma.api.oauth.changePassword({ config, params })

    if (result.state !== 'ok' || result.data.error) {
      throw result.data.error || 'Server error'
    } else {
      await dispatch(oauthThunks.logout())
      return result
    }
  },
  changeEmail: (params: any) => async (dispatch: Function, getState: Function) => {
    const config = getState().api.config
    const result = await pleroma.api.oauth.changeEmail({ config, params })

    if (result.state !== 'ok' || result.data.error) {
      throw result.data.error || 'Server error'
    } else {
      return result
    }
  },
  changeLocale: ({ locale }: { locale: String }) => async (dispatch: Function) => {
    dispatch(pleroma.reducers.api.actions.addConfig({ config: { locale } }))
    const storage = await getStorage()
    setConfig({ ...storage.config, locale })
  },
  resetPassword: (queries: any) => async (dispatch: Function, getState: Function) => {
    const config = getState().api.config
    const result = await pleroma.api.oauth.resetPassword({ config, queries })
    if (result.state !== 'ok') {
      throw (result.data && result.data.status === 404)
        ? new Error(`Sorry... We can't find such user`)
        : new Error('Something went wrong')
    } else {
      return getState()
    }
  }
}
