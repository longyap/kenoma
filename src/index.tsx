import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import pleroma from 'pleroma-api'
import { intlReducer, updateIntl, IntlProvider } from 'react-intl-redux'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { oauthReducer, oauthActions } from './reducers/oauth'
import { uiReducer } from './reducers/ui'
import { Provider } from 'react-redux'
import { oauthThunks } from './thunks/oauth'
import * as serviceWorker from './serviceWorker'
import { getStorage, setOauth } from './utils/storage'
import { setupConfig } from '@ionic/react'
import 'dayjs/locale/en'
import 'dayjs/locale/ru'
import { APP_ID } from './constants'
import { NOTIFICATIONS_FILTERING } from './components/settings/timeline_settings'
import { composeWithDevTools } from 'redux-devtools-extension';
import { sortBy } from 'lodash'
import { convertPleromaEmojiToMastodon } from './utils/normalizers'
import createThunkErrorHandlerMiddleware from 'redux-thunk-error-handler'

const reducer = combineReducers({
  statuses: pleroma.reducers.statuses.reducer,
  users: pleroma.reducers.users.reducer,
  notifications: pleroma.reducers.notifications.reducer,
  followRequests: pleroma.reducers.followRequests.reducer,
  conversations: pleroma.reducers.conversations.reducer,
  api: pleroma.reducers.api.reducer,
  oauth: oauthReducer,
  ui: uiReducer,
  intl: intlReducer,
  config: pleroma.reducers.config.reducer
})

const forbiddenErrorHandler = (err: any) => {
  if (err.name === 'ForbiddenError') {
    return async (dispatch: Function, getState: Function) => {
      await Promise.all([
        dispatch(oauthActions.clearApp()),
        dispatch(pleroma.reducers.users.actions.setCurrentUser({ user: null })),
        dispatch(pleroma.reducers.notifications.actions.clearNotifications()),
        dispatch(pleroma.thunks.api.stopAllFetchers())
      ])
      setOauth(getState().oauth)
    }
  }
}

const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(
    createThunkErrorHandlerMiddleware({ onError: forbiddenErrorHandler }),
    thunkMiddleware
  ))
)

declare global {
  interface Window {
    VERSION: string | undefined
    store: any;
    reducer: any;
    pleroma: any
  }
}

const initStorage = async () => getStorage()

const prepareEmojis = async() => {
  await store.dispatch(pleroma.thunks.config.fetchCustomEmoji({}))
  // @ts-ignore
  const emojis = store.getState().config.emojis

  if (emojis) {
    store.dispatch(pleroma.reducers.config.actions.addEmojis({
      emojis: sortBy(emojis.map((emoji: any) => convertPleromaEmojiToMastodon(emoji)), 'shortcode')
    }))
  }
}

let mount: any;
const load = async () => {
  const { oauth, config } = await initStorage()
  if (config) {
    const storeConfig = { ...config }
    delete storeConfig.settings
    store.dispatch(pleroma.reducers.api.actions.addConfig({ config: storeConfig }))
    const messages = await import(`./i18n/${config.locale || 'en'}.json`).then(({ default: messages }) => messages)

    store.dispatch(updateIntl({
      locale: config.locale,
      messages
    }))
  }
  if (config && config.settings) {
    const currentState = oauth.userToken ? await store.dispatch(pleroma.thunks.config.fetchConfig({ appId: APP_ID })) : { config: { [APP_ID]: {} } }

    const localConfig = {
      notificationsFiltering: {},
      theme: 'pleroma-light'
    }
    NOTIFICATIONS_FILTERING.reduce((acc: { [key: string]: boolean }, cur: { key: string, text: string }) => {
      acc[cur.key] = true
      return acc
    }, localConfig.notificationsFiltering)
    store.dispatch(pleroma.reducers.config.actions.addConfig({ config: { [APP_ID]: {...localConfig, ...currentState.config[APP_ID], ...config.settings} } }))
  }
  if (config.instance) {
    prepareEmojis()
  }
  if (oauth) {
    store.dispatch(
      oauthActions.setApp({
        ...(oauth as object),
        error: undefined
      })
    )
    if (oauth.userToken) {
      store.dispatch(
        pleroma.reducers.api.actions.addConfig({ config: { accessToken: oauth.userToken } })
      )
      store.dispatch<any>(oauthThunks.verifyCredentials())
    }
    const version =  process.env.REACT_APP_VERSION

    window.VERSION = version ? version.substr(0, 8) : ''

    mount = ReactDOM.render(
      <Provider store={store}>
        <IntlProvider locale={config.locale || 'en'}>
          <App />
        </IntlProvider>
      </Provider>,
    document.getElementById('root'), mount)
  }
}

setupConfig({
  animated: false
})
load()
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
