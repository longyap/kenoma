import React from 'react'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { IonPage, IonContent } from '@ionic/react'
import Account from '../../entities/account'
import { ProfileCardSm } from '../profile_card/profile_card_sm'

interface FollowRequestsInterface {
  followRequests: Account[]
}

const FollowRequestsComponent = (props: FollowRequestsInterface) => (
  <IonPage className='page-margin'>
    <IonContent>
      <div className='page-title mb-4 md:mb-6'>
        <FormattedMessage id='followRequests' defaultMessage='Follow requests' />
      </div>
      <div>{props.followRequests.map(item => (
        <ProfileCardSm
          key={item.id}
          account={item}
          highlighted={false}
          actions={['followRequest']}
        />
      ))}</div>
    </IonContent>
  </IonPage>
)

const mapStateToProps = ({ followRequests: { followRequestsByIds, list } }: {
  followRequests: { followRequestsByIds: { [key: string]: Partial<Account> }, list: string[] } }) => ({
  followRequests: list.map((id) => new Account(followRequestsByIds[id]))
})
export const FollowRequests = connect(mapStateToProps)(FollowRequestsComponent)