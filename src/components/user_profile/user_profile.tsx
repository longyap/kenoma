import React, { SyntheticEvent, Component } from 'react'
import { RouteComponentProps, withRouter } from "react-router"
import { IonPage, IonContent, withIonLifeCycle } from '@ionic/react'
import { debounce, forIn } from 'lodash'
import Icons from '../common/icons/icons'
import Account from '../../entities/account'
import Pleroma from 'pleroma-api'
import { connect } from 'react-redux'
import { ProfileCard } from '../profile_card/profile_card'
import { Status } from '../status/status'
import { ProfileCardSm } from '../profile_card/profile_card_sm'
import { injectIntl, IntlShape } from 'react-intl'
import { notFoundErrorCatcher } from '../../utils/api'

interface UserProfileComponentInterface extends RouteComponentProps {
  id: string,
  getUser: Function,
  user: Account,
  followers: Account[],
  following: Account[],
  isOtherUser: boolean,
  loadOlder: Function,
  loadingOlder: boolean,
  startFetchingUserStatuses: Function,
  stopFetchingUserStatuses: Function,
  fetchUsersFollowers: Function,
  fetchUsersFollowing: Function,
  intl: IntlShape,
}
interface UserProfileStateInterface {
  loading: boolean,
  tabName: string
}

class UserProfileComponent extends Component<UserProfileComponentInterface, UserProfileStateInterface> {
  constructor (props: UserProfileComponentInterface) {
    super(props)
    this.state = {
      loading: false,
      tabName: ''
    }
  }
  scrollElement: React.RefObject<any> = React.createRef()

  async ionViewDidEnter () {
    const tabName = this.props.location.search

    this.setState({
      loading: true,
      tabName: tabName.length ? tabName.substring(1) : ''
    })
    await this.props.getUser().then(() => {
      this.updateFetchers()
    })
  }

  ionViewWillLeave () {
    this.props.stopFetchingUserStatuses()
  }

  async componentDidUpdate (previousProps: UserProfileComponentInterface) {
    const {
      id,
      stopFetchingUserStatuses,
      getUser,
      location, user } = this.props

    if (id !== previousProps.id) {
      this.setState({ loading: true })
      await stopFetchingUserStatuses()
      await getUser().then(() => {
        this.updateFetchers()
      })
      this.setState({ loading: false })
    }
    if (location !== previousProps.location && user) {
      this.setState({
        tabName: location.search.length ? location.search.substring(1) : '',
        loading: true
      }, () => {
        this.updateFetchers()
      })
    }
  }

  updateFetchers = () => {
    const {
      stopFetchingUserStatuses, startFetchingUserStatuses,
      fetchUsersFollowers, fetchUsersFollowing
    } = this.props

    switch (this.state.tabName) {
      case 'followers': {
        stopFetchingUserStatuses()
        fetchUsersFollowers()
        break;
      }
      case 'following' : {
        stopFetchingUserStatuses()
        fetchUsersFollowing()
        break;
      }
      default: {
        startFetchingUserStatuses()
      }
    }
    this.setState({ loading: false })
  }

  tryLoadMore = (loading: boolean, fn: Function) => (event: SyntheticEvent) => {
    const { current } = this.scrollElement

    if (loading || !this.props.user || !this.props.user.id) return
    if ((current as HTMLElement).scrollHeight - (current as HTMLElement).scrollTop - (current as HTMLElement).offsetHeight <= 500) {
      fn && fn(this.state.tabName)
    }
  }

  render () {
    const { loading, tabName } = this.state
    const { user, isOtherUser, loadingOlder, loadOlder, history, followers, following } = this.props

    return (
      <IonPage className='page-margin'>
        <div className='cursor-pointer ml-6 md:ml-0' onClick={() => history.goBack()}>
          <Icons.ChevronRight rotateDeg={180} className='header text-highlighted w-8 h-8' />
        </div>
        <IonContent>
          <div
            className='pt-4 pb-12 md:pb-0 px-6 md:pl-0 md:pr-4 overflow-y-auto overflow-x-hidden max-h-full'
            onScroll={debounce(this.tryLoadMore(loading, loadOlder), 100)}
            ref={this.scrollElement}
          >
            {user && <div>
              <ProfileCard
                account={user}
                fullView={true}
                isOtherUser={isOtherUser}
              />
              { (tabName === 'statuses' || !tabName) && user.statuses && user.statuses.map(status => <Status key={status.id} status={status} userId={user.id} />)}
              { tabName === 'followers' && followers && followers.map((follower: Account) => <ProfileCardSm
                  key={follower.id}
                  account={follower}
                  highlighted={false}
                  actions={['follow']}
                />
              )}
              { tabName === 'following' && following && following.map((following: Account) =>
                <ProfileCardSm
                key={following.id}
                account={following}
                highlighted={false}
                actions={['follow']}
              />)}
            </div>
            }
            <div className='w-6 h-6 mx-auto my-4 text-secondary'>
              {(loadingOlder || loading) && <Icons.Cog className='w-10 h-10 a-loader-spin' />}
            </div>
          </div>
        </IonContent>
      </IonPage>
    )
  }
}

const mapStateToProps = ({ users: { usersByIds, currentUser }, api }: { users: { usersByIds: any, currentUser: Account }, api: any }) => ({
  usersByIds,
  currentUser,
  loadingOlder: api.userStatuses.loadingOlder || api.userFollowers.loadingOlder || api.userFollowing.loadingOlder,
})
const mapDispatchToProps = (dispatch: Function) => ({ dispatch })
const mergeProps = (
  { usersByIds, currentUser, loadingOlder }: { usersByIds: any, currentUser: Account, loadingOlder: boolean },
  { dispatch }: { dispatch: Function },
  ownProps: any) => {
  const id = ownProps.match.params.id
  let user = usersByIds[id]
  if (!user) {
    forIn(usersByIds, item => {
      if (item.acct === id) { user = item }
    })
  }

  return {
    id,
    user,
    // @ts-ignore
    followers: (user && user.followers) ? user.followers.map(id => new Account(usersByIds[id])) : [],
    // @ts-ignore
    following: (user && user.following) ? user.following.map(id => new Account(usersByIds[id])) : [],
    loadingOlder,
    isOtherUser: currentUser && user ? currentUser.id !== user.id : false,
    getUser: () => dispatch(Pleroma.thunks.users.fetchUser({ params: { id } }))
      .catch((e: any) => notFoundErrorCatcher(e, 'user', dispatch, ownProps.history, ownProps.intl)),
    startFetchingUserStatuses: () => dispatch(Pleroma.thunks.api.startFetchingUserStatuses({ params: { id: user ? user.id : id }})),
    stopFetchingUserStatuses: () => dispatch(Pleroma.thunks.api.stopFetchingUserStatuses()),
    loadOlder: (entity: string) => {
      if (entity === 'statuses' || entity === '') {
        return dispatch(Pleroma.thunks.api.loadOlderUserStatuses({ params: { id: user.id } }))
      }
      return dispatch(Pleroma.thunks.api.loadOlderUserList({
        params: { id: user.id },
        queries: { with_relationships: true },
        entity
      }))
    },
    fetchUsersFollowers: async () => {
      const state = user ? await dispatch(Pleroma.thunks.users.fetchUserFollowers({ params: { id: user.id }, queries: { with_relationships: true } })) : null;
      return state
    },
    fetchUsersFollowing: () => user ? dispatch(Pleroma.thunks.users.fetchUserFollowing({ params: { id: user.id }, queries: { with_relationships: true } })) : null,
    ...ownProps
  }
}
const UserProfileIonicComponent = withIonLifeCycle(UserProfileComponent)
export const UserProfile = injectIntl(withRouter(connect(mapStateToProps, mapDispatchToProps, mergeProps)(UserProfileIonicComponent)))
