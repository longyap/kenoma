import React from 'react'
import { Tooltip } from '../common/tooltip/tooltip'
import { ProfileCardSm } from '../profile_card/profile_card_sm'
import Account from '../../entities/account'
import Icons from '../common/icons/icons'

const UsersListTooltip = (props: any) => {
  const renderAccountsList = () => props.accounts ? props.accounts.map((item: Account, idx: number) => <ProfileCardSm
    highlighted={false}
    account={item}
    key={idx}
    size='sm'
  />) : null

  return (
    <Tooltip
      onMouseEnter={props.onMouseEnter}
      containerCssClass='users-list-tooltip overflow-y-auto'
      target={props.children} >
      {props.loading
        ? <Icons.Cog className='pt-2 text-secondary w-6 h-6 a-loader-spin' />
        : renderAccountsList()}
    </Tooltip>
  )
}

export { UsersListTooltip }
