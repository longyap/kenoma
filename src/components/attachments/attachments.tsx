import React, { useState, useEffect, SyntheticEvent } from 'react'
import { IonPopover } from '@ionic/react'
import chunk from 'lodash/chunk'
import './attachments.css'
import Attachment from '../../entities/attachment'
import { connect } from 'react-redux'
import { APP_ID } from '../../constants'
import Icons from '../common/icons/icons'

type AttachmentsInterface = {
  attachments: Attachment[],
  imagesPerRow?: number,
  hideAttachments: boolean,
  onFileDelete?: any
}

const getAttachmentClass = (itemsPerRow: number) => `flex-1-${itemsPerRow} h-full`
const getRowHeight = (itemsPerRow: number, media_uploader_mode: boolean): string => media_uploader_mode && itemsPerRow === 1 ? '38%' : `${(100 / (itemsPerRow + 0.6))}%`

export const AttachmentsComponent = ({ attachments, imagesPerRow, hideAttachments, onFileDelete }: AttachmentsInterface) => {
  const [ scaled, setScaled ] = useState(false)
  const [ currentIndex, setCurrentIndex ] = useState(0)
  const [ gallery, setGallery ] = useState([])
  const media_uploader_mode: boolean = !!onFileDelete

  useEffect(() => {
    let gallery = []
    gallery = chunk(attachments, imagesPerRow || 3)
    const length = gallery.length

    if (gallery[length - 1].length === 1 && gallery.length > 1) {
      gallery[length - 2].push(gallery[length - 1][0])
      gallery.length = length - 1
    }
    // @ts-ignore
    setGallery(gallery)
  }, [attachments, imagesPerRow])

  const renderGallery = () => gallery.map((galleryRow: Attachment[], index) => {
    const isLowRow = galleryRow.every(({ type }) => type !== 'image' && type !== 'video') || hideAttachments

    return (
      <div
        key={index}
        className='h-0 relative'
        style={{'paddingBottom': isLowRow ? media_uploader_mode ? '2.5rem' : '1.5rem' : getRowHeight((galleryRow as any[]).length, media_uploader_mode)}}>
        <div className='flex items-stretch h-full absolute top-0 left-0 right-0 bottom-0' >
          {renderGalleryRow(galleryRow, isLowRow)}
        </div>
      </div>)
})

  const renderGalleryRow = (galleryRow: any, isLowRow: boolean) => galleryRow.map(({ id, type, remote_url }: Attachment, index: number) => {
    const hideAttachmentIconName = type === 'video' || type === 'image' ? `${type[0].toUpperCase()}${type.slice(1)}` : 'File'

    return (
      <div
        className={`p-1 cursor-pointer relative overflow-hidden w-full h-full flex items-center ${!hideAttachments && getAttachmentClass(galleryRow.length)}`}
        key={index}
        onClick={() => scale(index)}
      >
        { onFileDelete && <div
          onClick={(e) => deleteFile(e, id)}
          className={`z-10 button-error rounded-full h-10 w-10 flex items-center justify-center absolute right-0 ${!isLowRow && 'mt-4'} mr-2`}
        >
          <Icons.Delete className='icon w-6 h-6 cursor-pointer' />
        </div> }
        { hideAttachments
          ? React.createElement(Icons[hideAttachmentIconName], { className: 'w-8 h-8 text-highlighted link' })
          : type === 'video'
            ? <video className='w-full h-full object-cover' controls={!onFileDelete} key={index}>
              <source src={remote_url} />
            </video>
            : type === 'image'
              ? <img alt='img' key={index} className='min-h-full object-cover w-full' src={remote_url} />
              : <Icons.File className='w-8 h-8 text-highlighted link' />
        }
      </div>)
  })

  const deleteFile = (e: any, id: string) => {
    e.stopPropagation()
    onFileDelete && onFileDelete(id)
  }

  const scale = (currentIndex: number) => {
    if (attachments[currentIndex].type !== 'video' || hideAttachments) {
      setScaled(!scaled)
      setCurrentIndex(currentIndex)
    }
  }

  const close = () => {
    setScaled(false)
    setCurrentIndex(0)
  }

  const showPrevImage = (e: SyntheticEvent) => {
    e.stopPropagation()
    setCurrentIndex(!currentIndex ? attachments.length - 1 : currentIndex - 1)
  }

  const showNextImage = (e: SyntheticEvent) => {
    e.stopPropagation()
    setCurrentIndex(currentIndex === attachments.length - 1 ? 0 : currentIndex + 1)
  }

  return (
    <div className='mb-3'>
      <div className='flex flex-col' >
        {renderGallery()}
      </div>
      {scaled && <IonPopover
        cssClass='gallery'
        isOpen={scaled}
        onDidDismiss={close}
      >
        <div className='fixed overlay flex items-center justify-between top-0 left-0 w-screen h-screen z-50' onClick={close}>
        {attachments.length > 1 && <span onClick={showPrevImage} className='cursor-pointer p-2 flex align-center justify-center foreground rounded-full'>
          <Icons.ChevronRight className='h-6 w-6 link' rotateDeg={180} />
        </span>}
        <div className='w-full flex items-center justify-center relative m-4 cursor-pointer'>
          {attachments.map((attachment: Attachment, index: number) => (
            attachment.type === 'video'
              ? <video key={index} className={`top-0 bottom-0 m-auto absolute ${currentIndex === index ? 'opacity-100 z-10' : 'opacity-0'}`} controls={true}>
                <source src={attachment.remote_url} />
              </video>
              : <img
                key={index}
                alt='img'
                src={attachment.remote_url}
                className={`top-0 bottom-0 m-auto absolute ${currentIndex === index ? 'opacity-100 z-10' : 'opacity-0'}`}
                style={{transition: 'opacity .9s', maxHeight: '90vh'}}
              />
          ))}
        </div>
        {attachments.length > 1 && <span onClick={showNextImage} className='cursor-pointer p-2 flex align-center justify-center rounded-full foreground'>
          <Icons.ChevronRight rotateDeg={0} className='h-6 w-6 link' />
        </span>}
      </div></IonPopover>}
    </div>
  )
}

const mapStateToProps = ({ api: { config } }: { api: { config: any }}) => ({
  hideAttachments: (config && config[APP_ID]) ? config[APP_ID].hideAttachments : false
})
const mergeProps = ({ hideAttachments }: { hideAttachments: true }, dispatchProps: any, ownProps: any) => ({
  hideAttachments,
  ...ownProps
})
export const Attachments = connect(mapStateToProps, null, mergeProps)(AttachmentsComponent)