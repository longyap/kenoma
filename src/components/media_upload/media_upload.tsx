import React, { Component } from 'react'
import Pleroma from 'pleroma-api'
import { connect } from 'react-redux'
import Icons from '../common/icons/icons'
import { FormattedMessage } from 'react-intl'
import { uniqBy } from 'lodash'
import Attachment from '../../entities/attachment'

export type MediaUploadType = {
  disabled?: boolean,
  config?: object,
  onUpload: Function,
  className?: string,
  _component?: any,
  ref?: any
}
type MediaUploadState = {
  media: any[],
  loading: boolean,
  error: boolean,
  hightlight?: boolean
}
class MediaUploadComponent extends Component<MediaUploadType, MediaUploadState> {
  constructor (props: MediaUploadType) {
    super(props)

    this.state = {
      media: [],
      loading: false,
      error: false
    }
  }
  fileInputRef: React.RefObject<any> = React.createRef();

  openFileDialog = () => {
    if (this.props.disabled) return
    this.fileInputRef.current.click()
  }

  onDragOver = (e: any) => {
    e.preventDefault()
    if (this.props.disabled) return
    this.setState({ hightlight: true })
  }

  onFilesAdded = (e: any) => {
    e.preventDefault()
    if (this.props.disabled) return
    const files = this.fileListToArray(e.target.files || e.dataTransfer.files)

    this.uploadFiles(files)
  }

  fileListToArray = (files: any) => {
    const array = []
    for (let i = 0; i < files.length; i++) {
      array.push(files.item(i))
    }
    return array
  }

  uploadFiles = async (files: File[]) => {
    this.setState({ loading: true })
    const { onUpload, config } = this.props
    const promises: any[] = []

    files.forEach(file => {
      const body = new FormData()
      body.append('file', file)

      promises.push(Pleroma.api.media.upload({ config, body }))
    })
    await Promise.all(promises)
      .then(results => {
        const media = results.map(({ data }) => data)
        const newMediaArr = uniqBy([...this.state.media, ...media], 'id')
        onUpload && onUpload(newMediaArr)
        this.setState({ error: false, loading: false, media: newMediaArr })
      })
      .catch(e => {
        this.setState({ error: (e.data && e.data.error) || e, loading: false })
      })
  }

  clear = () => this.setState({ media: [], loading: false, error: false })
  updateMedia = (media: Attachment[]) => this.setState({ media })

  render () {
    const { className } = this.props
    const { error, loading } = this.state
    const disabled = this.props.disabled || loading

    return (
      <div className={className}>
        <div
          onDrop={this.onFilesAdded}
          onDragOver={this.onDragOver}
          onClick={this.openFileDialog}
          style={{ cursor: disabled ? 'default' : 'pointer' }}
          className='flex items-center'
        >
          <Icons.Attachment className={`text-secondary link icon w-6 h-6 ${error && 'border-error'}`} />
          <span className={`hidden md:block font-semibold text-sm text-secondary link icon ${error && 'border-error'}`}>
            <FormattedMessage defaultMessage='Add File' id='mediaUpload.addFile' />
            </span>
          <input
            ref={this.fileInputRef}
            className='hidden'
            type='file'
            multiple={true}
            disabled={disabled}
            onChange={this.onFilesAdded}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ api: { config } }: { api: { config: any }}) => ({ config })
export const MediaUpload = connect(mapStateToProps, null, null, { forwardRef: true })(MediaUploadComponent)
