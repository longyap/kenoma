import React, { Component } from 'react'
import Icons from '../common/icons/icons'

export const AVATAR_SHADOW_OVERLAY = 'inset 0 0 0 2000px rgba(73, 73, 73, 0.4)'

export type AvatarUploadType = {
  disabled?: boolean,
  onUpload: Function,
  className?: string,
  _component?: any
}
type AvatarUploadState = {
  file: File | null,
  error: boolean,
  hightlight?: boolean
}
export class AvatarUpload extends Component<AvatarUploadType, AvatarUploadState> {
  constructor (props: AvatarUploadType) {
    super(props)

    this.state = {
      file: null,
      error: false
    }
  }
  fileInputRef: React.RefObject<any> = React.createRef();

  openFileDialog = () => {
    if (this.props.disabled) return
    this.fileInputRef.current.click()
  }

  onDragOver = (e: any) => {
    e.preventDefault()
    if (this.props.disabled) return
    this.setState({ hightlight: true })
  }

  onFileAdded = (e: any) => {
    e.preventDefault()
    if (this.props.disabled) return
    const file = this.fileListToArray(e.target.files || e.dataTransfer.files)

    this.setState({ file })
    this.props.onUpload(file)
  }

  fileListToArray = (files: any) => {
    const array = []

    for (let i = 0; i < files.length; i++) {
      array.push(files.item(i))
    }
    return array[0]
  }

  clear = () => this.setState({ file: null, error: false })

  render () {
    const { className } = this.props
    const { error } = this.state
    const disabled = this.props.disabled


    return (
      <div
        onDrop={this.onFileAdded}
        onDragOver={this.onDragOver}
        onClick={this.openFileDialog}
        style={{ cursor: disabled ? 'default' : 'pointer' }}
        className={`flex w-full h-full ${className}`}
      >
        <Icons.Camera className={`text-accented icon w-6 h-6 m-4 ${error && 'border-error'}`} />
        <input
          ref={this.fileInputRef}
          className='hidden'
          type='file'
          accept="image/*"
          disabled={disabled}
          onChange={this.onFileAdded}
        />
      </div>
    )
  }
}
