import React from 'react'
import dayjs from 'dayjs'
import LocalizedFormat from 'dayjs/plugin/localizedFormat'
import { ProfileActions } from '../profile_actions/profile_actions'
import { FormattedMessage } from 'react-intl'
import Account from '../../entities/account'
import { Link } from 'react-router-dom'
import Icon from '../common/icons/icons'

dayjs.extend(LocalizedFormat)

class ProfileCardType {
  account: Account = new Account()
  fullView: boolean = false
  isOtherUser: boolean = false
  hideActions?: boolean = false
  className?: string = 'mb-12'
}

interface Tab {
  name: string,
  field: string,
  disable?: boolean
}

export const ProfileCard = ({ account, fullView, isOtherUser, hideActions, className }: ProfileCardType) => {
  const TABS = [
    {
      name: 'statuses',
      field: 'statuses_count',
      disable: false
    },
    {
      name: 'following',
      field: 'following_count',
      disable: account.pleroma ? account.pleroma.hide_follows : false
    },
    {
      name: 'followers',
      field: 'followers_count',
      disable: account.pleroma ? account.pleroma.hide_followers : false
    }
  ]
  const renderLink = ({ name, field }: Tab) => (
    <span>
      <span className='block text-xs text-secondary'>
        <FormattedMessage
          id={`profileCard.${name}`}
          defaultMessage={name[0].toUpperCase().concat(name.substr(1))}
        />
      </span>{
        // @ts-ignore
        account[field]
      }
    </span>
  )
  return (
  <div className={`flex flex-wrap justify-center ${className}`}>
    <div className='relative w-full'>
      <img
        alt=''
        className='w-full rounded-lg object-cover profile-card__banner'
        src={`${account.header_static}`}
      />
      <div className={`absolute top-0 right-0 md:ml-0 mr-4 mt-4 flex items-center ${(!hideActions && isOtherUser) ? '' : 'text-right'}`}>
        { account.locked && <Icon.FilledLock className='mr-4 text-secondary' />}
        { !hideActions && isOtherUser
          ? <ProfileActions className='absolute top-0 right-0 md:ml-0 mr-4 mt-4' account={account} />
          : <Link
              className='button button-outline rounded p-2 rounded icon-light overlay'
              to='/settings/profile'
            >
              <FormattedMessage
                id='profileCard.editProfile'
                defaultMessage='Edit Profile'
              />
          </Link>}
      </div>
    </div>
    <div className='flex items-center flex-col -mt-8 z-10 w-full'>
      <img className='w-16 h-16 rounded-lg' alt={account.acct} src={account.avatar_static} />
      <div className='flex flex-col items-center mb-4'>
        {account.display_name && <h2 className='text-base text font-bold mt-4 mb-0' dangerouslySetInnerHTML={{__html: account.display_name}} />}
        <p className='text-sm text-secondary'>{account.acct}</p>
      </div>
      <div className='mb-4 w-full'>
        <ul className='list-reset flex items-top link font-bold'>
          {TABS.map(item => item.disable
            ? (<li key={item.name} className='w-1/3 text-center'>
                {renderLink(item)}
              </li>)
            : (<Link
              to={{ search: `?${item.name}` }}
              key={item.name}
              className='w-1/3 text-center'
            >
              {renderLink(item)}
            </Link>)
          )}
        </ul>
      </div>
    {fullView && isOtherUser && account.relationships && account.relationships.followed_by && <div className='w-full text-xs text-secondary mb-2'>
      <FormattedMessage id='profileCard.followsYou' defaultMessage='Follows you' />
    </div>}
    {fullView && account.note && <div className='md:block px-8 w-full text-xs text text-center' dangerouslySetInnerHTML={{ __html: account.note }} />}
    </div>
  </div>
)
}
