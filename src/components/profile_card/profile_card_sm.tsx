import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Account from '../../entities/account'
import { IsMobileContext } from '../../isMobileContext'
import { FollowButton, MuteButton, BlockButton, FollowRequestButtons } from '../action_buttons/action_buttons'
import AccountRelationships from '../../entities/accountRelationships'

type ButtonAction = 'follow' | 'mute' | 'block' | 'followRequest'

interface ProfileCardSm {
  account: Account,
  disableLink?: boolean,
  onClick?: Function,
  onActionButtonClick?: Function,
  highlighted: boolean,
  size?: String,
  actions?: ButtonAction[]
}
export const ProfileCardSm = ({ account, disableLink, onClick, onActionButtonClick, highlighted = false, size, actions }: ProfileCardSm) => {
  const [formattedAccount, updateFormattedAccount] = useState(new Account(account))
  const switchRelationship = (item: AccountRelationships) => {
    onActionButtonClick && onActionButtonClick(item)
    updateFormattedAccount({
      ...formattedAccount,
      relationships: { ...item }
    })
  }
  const classes = size === 'sm'
    ? {
      wrapper: 'px-2 py-1',
      avatar: 'w-6 h-6',
      avatarStyle: { minWidth: '1.5rem' },
      displayName: 'text-sm',
      acct: 'text-xs'
    }
    : {
      wrapper: 'px-4 py-2',
      avatar: 'w-10 h-10',
      avatarStyle: { minWidth: '2.5rem' },
      displayName: 'text-lg',
      acct: 'text-sm'
    }
  const renderChildren = () => (
    <IsMobileContext.Consumer>
      { ({ isMobile }) => {
        let innerContentWidth = 40
        if (!isMobile && actions) {
          innerContentWidth += 120 * actions.length
        }
        return (
          <div className={`${classes.wrapper} flex items-center cursor-pointer overflow-x-hidden ${highlighted ? 'highlighted' : ''}`}>
            <img
              className={`${classes.avatar} rounded-lg overflow-hidden`}
              alt={account.acct}
              src={account.avatar_static}
              style={classes.avatarStyle}
            />
            <div className='px-4' style={{maxWidth: `calc(100% - ${innerContentWidth}px)`}}>
              <h2 className={`${classes.displayName} my-0 text truncate`} dangerouslySetInnerHTML={{ __html: account.display_name }} />
              <p className={`${classes.acct} text-secondary truncate profile-acct`}>{account.acct}</p>
            </div>
            { (!isMobile && (actions ? actions.includes('follow') : false)) && <FollowButton
              account={formattedAccount}
              className='ml-auto'
              onStatusChange={switchRelationship}
            />}
            { (!isMobile && (actions ? actions.includes('mute') : false)) && <MuteButton
              account={formattedAccount}
              className='ml-auto'
              onStatusChange={switchRelationship}
            />}
            { (!isMobile && (actions ? actions.includes('block') : false)) && <BlockButton
              account={formattedAccount}
              className='ml-auto'
              onStatusChange={switchRelationship}
            />}
            { (!isMobile && (actions ? actions.includes('followRequest') : false)) && <FollowRequestButtons
              account={formattedAccount}
              className='ml-auto'
              onStatusChange={switchRelationship}
            />}
          </div>)
      } }
    </IsMobileContext.Consumer>
  )
  return disableLink
    ? <li onClick={() => onClick && onClick()} className='profile-search-result'>{renderChildren()}</li>
    : <Link to={`/users/${account.id}`}>{renderChildren()}</Link>
}
