import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  injectIntl,
  FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import { IonPage, withIonLifeCycle, IonContent } from '@ionic/react'
import map from 'lodash/map'
import Icons from '../common/icons/icons'
import Pleroma from 'pleroma-api'

class ChatOverviewComponent extends Component {
  ionViewDidEnter () {
    const { startFetchingConversations } = this.props
    
    startFetchingConversations && startFetchingConversations()
  }
  ionViewWillLeave() {
    const { stopFetchingConversations } = this.props

    stopFetchingConversations && stopFetchingConversations()
  }

  renderConversationsList = (conversations) => map(conversations, ({ accounts, last_status, id }) => {
    const participantsMarkup = accounts.length > 1
      ? <ul className='avatar-group list-reset flex relative'>
        {accounts.map(({ avatar, acct, id }, index) => index < 2
          ? <li
            key={acct}
            className={`rounded-lg avatar overflow-hidden border-2 border-white absolute ${index ? 'bottom-0 left-0' : 'top-0 right-0'}`}
          >
            <img src={avatar} alt={acct} />
          </li>
          : null
        )}
      </ul>
      : <div className='w-10 h-10 rounded-lg overflow-hidden border-2 border-white'>
          {!!accounts.length && <img src={accounts[0].avatar} alt={accounts[0].acct} />}
        </div>

    return (
      <li key={id} className='overflow-hidden text' >
        <Link to={`/chatroom/${id}`} className='flex items-center pl-4 pb-4 pt-3 pr-3 no-underline button-inline' href='#'>
          {participantsMarkup}
          <div
            className='flex-1 pl-6 pr-4'
            style={{ maxWidth: 'calc(100% - 5.5rem)', width: 'calc(100% - 5.5rem)' }}
          >
            {last_status.spoiler_text && <h3 className='text-base text-highlighted'>{last_status.spoiler_text}</h3>}
            <p className='whitespace-no-wrap overflow-hidden' style={{textOverflow: 'ellipsis'}} dangerouslySetInnerHTML={{ __html: last_status.content }} />
          </div>
          <Icons.ChevronRight className='icon arrow-right block h-6 w-6 ml-auto' />
        </Link>
      </li>
    )
  })

  render () {
    const { conversations } = this.props

    return (
      <IonPage className='page-margin'>
        <IonContent>
          <div className='px-6 page-title mb-4 md:mb-6'>
            <FormattedMessage id='chats' defaultMessage='Chats' />
          </div>
          <ul className='list-reset mb-12 md:mb-0 px-6 md:pl-0 md:pr-4'>
            {this.renderConversationsList(conversations)}
          </ul>
        </IonContent>
      </IonPage>
    )
  }
}

const mapStateToProps = ({ conversations }) => ({
  conversations: conversations.list.map(id => conversations.conversationsByIds[id])
})
const mapDispatchToProps = (dispatch) => ({
  startFetchingConversations: () => dispatch(Pleroma.thunks.api.startFetchingConversations({})),
  stopFetchingConversations: () => dispatch(Pleroma.thunks.api.stopFetchingConversations())
})

const ChatOverviewIonicComponent = withIonLifeCycle(ChatOverviewComponent)
export const ChatOverview = injectIntl(connect(mapStateToProps, mapDispatchToProps)(ChatOverviewIonicComponent))
