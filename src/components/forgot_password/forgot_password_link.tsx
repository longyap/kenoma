import React from 'react'
import { Link } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import { Tooltip } from '../common/tooltip/tooltip'
import { connect } from 'react-redux'

const ForgotPasswordLinkComponent = ({ disabled }: { disabled: boolean }) => disabled
  ? <Tooltip
      containerCssClass='users-list-tooltip'
      target={
        <span className='self-start mb-4 cursor-not-allowed  text-xs logo-login'>
          <FormattedMessage defaultMessage='Forgot password?' id='auth.forgotPassword' />
        </span>}
    >
      <FormattedMessage
        id='auth.passwordResetIsNotAllowed'
        defaultMessage='Password reset is disabled. Please contact your instance administrator'
      />
    </Tooltip>
  : <Link
      to='forgot-password'
      className='text-xs logo-login mb-4 w-full'
    >
      <FormattedMessage defaultMessage='Forgot password?' id='auth.forgotPassword' />
    </Link>

const mapStateToProps = ({ config }: { config: any }) => ({
  disabled: (config.nodeinfo && config.nodeinfo.metadata) ? !config.nodeinfo.metadata.mailerEnabled : true
})

export const ForgotPasswordLink = connect(mapStateToProps)(ForgotPasswordLinkComponent)
