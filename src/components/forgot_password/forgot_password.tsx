import React, { useState, SyntheticEvent } from 'react'
import { Input } from '../input/input'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { oauthThunks } from '../../thunks/oauth'
import { Error } from '../common/alerts/alerts'
import { FormattedMessage } from 'react-intl'
import { InstanceSwitcher } from '../instance_selection/instance_switcher'

interface ForgotPasswordInterface {
  resetPassword: Function
}

const ForgotPasswordComponent = ({ resetPassword }: ForgotPasswordInterface) => {
  const [email, setEmail] = useState('')
  const [isLoading, toggleLoading] = useState(false)
  const [showSuccessText, toggleSuccessText] = useState(false)
  const [errorText, setErrorText] = useState('')

  const onSubmit = async (e: SyntheticEvent) => {
    e.preventDefault()
    try {
      toggleLoading(true)
      resetPassword && await resetPassword(email)
      toggleSuccessText(true)
    } catch (e) {
      setErrorText(e.message)
    } finally {
      toggleLoading(false)
    }
  }

  const renderSuccessText = () => (
    <div className='pop-out message mt-4 mb-4 text-center'>
      <FormattedMessage
        id="auth.linkWasSent"
        defaultMessage="We will send you a link to reset your password"
      />
    </div>
  )
  return (
    <form
      className='px-8 pt-6 pb-8 mt-10 flex items-center flex-col'
      onSubmit={onSubmit}
    >
      {showSuccessText && renderSuccessText()}
      {errorText && <Error error={errorText} />}
      <InstanceSwitcher />
      <Input
        value={email}
        onChange={(e: any) => setEmail(e.target.value)}
        message='Your email or username'
        className='w-full mb-4'
      />
      <button
        className='login-btn'
        disabled={isLoading}
      >
        Submit
      </button>
      <div className='flex items-center'>
        <Link to='/' className='logo-login py-2 mr-4'>Log in</Link>
        <Link to='/sign-up' className='logo-login py-2'>Sign up</Link>
      </div>
    </form>
  )
}

const mapDispatchToProps = (dispatch: Function) => ({
  resetPassword: (email: string) => dispatch(oauthThunks.resetPassword({ email }))
})
export const ForgotPassword = connect(null, mapDispatchToProps)(ForgotPasswordComponent)
