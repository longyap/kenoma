import React, { Component } from 'react'
import { IonModal, IonFab, IonFabButton } from '@ionic/react'
import Status from '../../entities/status'
import { PostStatusForm } from './post_status_form'
import Icons from '../common/icons/icons'
import { injectIntl, WrappedComponentProps } from 'react-intl'
import { focusReactRefField } from '../../utils/statusUtils'

interface PostStatusModalProps extends WrappedComponentProps {
  onPost?: Function,
  onClose?: Function,
  repliedStatus?: Status
}
interface PostStatusModalState {
  isOpen: boolean
}
class PostStatusModalComponent extends Component<PostStatusModalProps, PostStatusModalState> {
  constructor (props: PostStatusModalProps) {
    super(props)
    this.state = {
      isOpen: false
    }
  }

  componentDidMount () {
    if (this.props.repliedStatus) {
      this.setState({ isOpen: true })
    }
  }

  postStatusForm: React.RefObject<any> = React.createRef()

  renderFabPanel = () => (
    <IonFab vertical='bottom' horizontal='end' className='mr-6 mb-12' >
      <IonFabButton
        className='flex button-primary rounded-full w-14 h-14 cursor-pointer'
        onClick={() => {this.setState({ isOpen: true })}}
      >
        <Icons.Edit className='w-8 h-8 m-4' />
      </IonFabButton>
    </IonFab>)

  open = () => {
    const { current } = this.postStatusForm

    current && focusReactRefField(current.statusTextRef, current.state.status.length)
  }

  close = () => {
    this.postStatusForm.current && this.postStatusForm.current.close()
    this.props.onClose && this.props.onClose()
    this.setState({ isOpen: false })
  }

  onPost = () => {
    this.close()
    const { onPost } = this.props

    onPost && onPost()
  }

  render () {
    const { repliedStatus, intl } = this.props
    const { isOpen } = this.state

    return <div>
      {!repliedStatus && this.renderFabPanel()}
      <IonModal
        isOpen={isOpen}
        onDidDismiss={this.close}
        onDidPresent={this.open}
      >
        <div onClick={this.close} className='absolute ml-5 mt-4'>
          <Icons.Close className='w-6 h-6 cursor-pointer link' />
        </div>
        <PostStatusForm
          onPost={this.onPost}
          intl={intl}
          repliedStatus={repliedStatus}
          ref={this.postStatusForm}
        />
      </IonModal>
    </div>
  }
}

export const PostStatusModal = injectIntl(PostStatusModalComponent)
