import React, { Component, createRef, RefObject } from 'react'
import { connect } from 'react-redux'
import Pleroma from 'pleroma-api'
import Icons from '../common/icons/icons'
import { WrappedComponentProps, FormattedMessage } from 'react-intl'
import { getStatusMentionsList, createAutocompleteRequest, insertAutocompleteResult, focusReactRefField } from '../../utils/statusUtils'
import { Select } from '../common/select/select'
import { MediaUpload } from '../media_upload/media_upload'
import { PollForm } from '../poll/poll_form'
import Status from '../../entities/status'
import Account from '../../entities/account'
import { Autocomplete } from '../common/autocomplete/autocomplete'
import debounce from 'lodash/debounce'
import Attachment from '../../entities/attachment'
import { Attachments } from '../attachments/attachments'
import { getUserSuggestions } from '../../utils/suggestor'
import { EmojiPanel } from '../emoji_panel/emoji_panel'
import { STATUS_VISIBILITY_OPTIONS, AUTOCOMPLETE_TYPES, SEARCH_DEBOUNCE_MS } from '../../constants'
import { EmojiDB } from '../emoji_panel/emoji_db'
import { EmojiClickEventDetail, CustomEmoji } from 'emoji-picker-element/shared'
import { Error as ErrorLabel } from '../common/alerts/alerts'

interface PostStatusFormInterface extends WrappedComponentProps{
  repliedStatus?: Status,
  currentUser: Account,
  users: any[],
  postStatus: Function,
  onPost: Function,
  _component?: any,
  searchUsers: Function,
  emojis: CustomEmoji[]
}
type PostStatusFormState = {
  spoiler_text: string,
  status: string,
  media: Attachment[],
  visibility: string,
  in_reply_to_id: string | null,
  showPollPanel: boolean,
  showEmojiPanel: boolean,
  poll: {
    options: any[]
  },
  suggestions: Account[] | EmojiClickEventDetail[],
  autocompleteRequest: string,
  searchRequestsCounter: number,
  selectionStart: number,
  lastFocusedElem: RefObject<any> | null,
  emojiDB: EmojiDB | null,
  errorText: string
}
const defaultFormData = {
  spoiler_text: '',
  status: '',
  media: [],
  visibility: 'public',
  in_reply_to_id: null,
  showPollPanel: false,
  showEmojiPanel: false,
  poll: {},
  suggestions: [],
  autocompleteRequest: '',
  searchRequestsCounter: 0,
  selectionStart: 0,
  lastFocusedElem: null,
  emojiDB: null,
  errorText: ''
}
class PostStatusFormComponent extends Component<PostStatusFormInterface, PostStatusFormState> {
  constructor (props: PostStatusFormInterface) {
    super(props)

    this.state = JSON.parse(JSON.stringify(defaultFormData))
  }
  mediaUploadRef: RefObject<any> = createRef()
  pollFormRef: RefObject<any> = createRef()
  spoilerTextRef: RefObject<any> = createRef()
  statusTextRef: RefObject<any> = createRef()
  autocompleteRef: RefObject<any> = createRef()

  componentDidMount () {
    const { repliedStatus, emojis } = this.props
    if (repliedStatus) {
      this.setState({
        in_reply_to_id: repliedStatus.id,
        visibility: repliedStatus.visibility,
        status: this.buildMentionsString(),
        spoiler_text: repliedStatus.spoiler_text ? `Re: ${repliedStatus.spoiler_text}` : ''
      })
    }
    this.setState({ emojiDB: new EmojiDB({ emojis }) })
  }

  buildMentionsString = () => {
    const { currentUser, repliedStatus } = this.props
    const mentions = getStatusMentionsList(repliedStatus)

    return mentions.reduce((result, mention) => currentUser.id !== mention.id
      ? `@${mention.acct} ${result}`
      : result,
    this.state.status)
  }

  getStatus = () => {
    const { spoiler_text, status, media, visibility, in_reply_to_id }: Partial<Status> = this.state

    if (!status && !media.length) {
      throw new Error(this.props.intl.formatMessage({ id: 'postStatusForm.emptyStatusError' }))
    }
    const statusObj: Partial<Status> = {
      spoiler_text,
      status,
      media_ids: media.map(({ id }) => id),
      in_reply_to_id,
      visibility,
    }
    if (Object.keys(this.state.poll).length) {
      statusObj.poll = {
        ...this.state.poll,
        options: this.state.poll.options.filter(option => option.length)
      }
    }
    return statusObj
  }

  postStatus = async () => {
    const { postStatus, onPost } = this.props
    try {
      postStatus && await postStatus(this.getStatus())
      this.close()
      onPost && onPost()
    } catch (e) {
      this.setState({ errorText: e.message }, () => {
        setTimeout(() => {
          this.setState({ errorText: '' })
        }, 4000)
      })
    }
  }

  clear = () => {
    const { repliedStatus } = this.props

    this.setState(Object.assign(
      JSON.parse(JSON.stringify(defaultFormData)),
      { in_reply_to_id: repliedStatus ? repliedStatus.in_reply_to_id : null }
    ))
    if (this.mediaUploadRef.current && (this.mediaUploadRef.current as any).clear) {
      (this.mediaUploadRef.current as any).clear()
    }
    if (this.pollFormRef.current && (this.pollFormRef.current as any).clear) {
      (this.pollFormRef.current as any).clear()
    }
  }

  close = () => {
    this.clear()
    this.setState({ showPollPanel: false })
  }

  getTextFieldHandlers = (ref: RefObject<any>) => ({
    onChange: (e: any) => { e.persist(); this.onTextFieldChange(ref, e) },
    onClick: () => this.setState({ selectionStart: ref.current.selectionStart}),
    onFocus: () => this.setState({ lastFocusedElem: ref })
  })

  onTextFieldKeyDown = (e: any) => {
    e.persist()
    if (e.key === 'Enter') {
      e.preventDefault()
    }
  }

  onTextFieldChange = (fieldRef: RefObject<any>, e: any) => {
    const newValue = e.target.value
    const { selectionStart } = e.target
    const autocompleteRequest = createAutocompleteRequest(newValue, selectionStart)
    const isStatusFieldTyped = fieldRef === this.statusTextRef

    this.setState((prevValue) => ({
      status: isStatusFieldTyped ? newValue : prevValue.status,
      spoiler_text: isStatusFieldTyped ? prevValue.spoiler_text : newValue,
      autocompleteRequest: autocompleteRequest ? autocompleteRequest.request : '',
      selectionStart,
      showEmojiPanel: false
    }))
    if (autocompleteRequest &&
      (isStatusFieldTyped || (!isStatusFieldTyped && autocompleteRequest.type === AUTOCOMPLETE_TYPES.emojis.name))) {
      this.openAutocomplete(e, autocompleteRequest.type)
    } else {
      this.closeAutocomplete()
    }
  }

  openAutocomplete = (e: any, autocompleteType: string) => {
    if (this.autocompleteRef && this.autocompleteRef.current && !this.autocompleteRef.current.state.isOpen) {
      this.autocompleteRef.current.open(e, autocompleteType)
    }
    if (autocompleteType === AUTOCOMPLETE_TYPES.users.name) {
      this.debouncedUsersSearch()
    } else if (autocompleteType === AUTOCOMPLETE_TYPES.emojis.name) {
      this.debouncedEmojisSearch()
    }
  }

  closeAutocomplete = () => this.autocompleteRef && this.autocompleteRef.current && this.autocompleteRef.current.close()

  onAutocompleteSelect = ({ res, type } : { res: any, type: string }, simpleInsert?: boolean) => {
    if (type === AUTOCOMPLETE_TYPES.users.name) {
      this.insertText(res.acct, type)
    } else if (type === AUTOCOMPLETE_TYPES.emojis.name) {
      this.insertText(res.unicode || `:${res.name}:`, type, simpleInsert)
      this.setState({ showEmojiPanel: false })
    }
  }

  searchUsers = async () => {
    const { autocompleteRequest } = this.state
    this.setState(prevState => ({
      suggestions: getUserSuggestions(autocompleteRequest, this.props.users),
      searchRequestsCounter: prevState.searchRequestsCounter + 1
    }))
    const { q } = this.props.searchUsers && await this.props.searchUsers(autocompleteRequest)

    this.setState(prevState => ({ searchRequestsCounter: prevState.searchRequestsCounter - 1 }))
    if (q === this.state.autocompleteRequest) {
      const suggestions = getUserSuggestions(this.state.autocompleteRequest, this.props.users)
      this.setState({ suggestions })
    }
  }

  searchEmoji = () => {
    (this.state.emojiDB as any)
      .search(this.state.autocompleteRequest)
      .then(({ res, q }: { res: EmojiClickEventDetail[], q: string }) => {
        if (q === this.state.autocompleteRequest) {
          this.setState({ suggestions: res })
        }
      })
  }

  debouncedUsersSearch = debounce(this.searchUsers, SEARCH_DEBOUNCE_MS)
  debouncedEmojisSearch = debounce(this.searchEmoji, SEARCH_DEBOUNCE_MS)

  insertText = (insertedText: string, type: string, simpleInsert?: boolean) => {
    const isStatusInFocus = this.state.lastFocusedElem === this.statusTextRef
    const autocompleteText = isStatusInFocus ? this.state.status : this.state.spoiler_text
    const { text, selectionIndex } = insertAutocompleteResult(autocompleteText, insertedText, this.state.selectionStart, type, simpleInsert)

    this.setState(prevState => ({
      status: isStatusInFocus ? text : prevState.status,
      spoiler_text: isStatusInFocus ? prevState.spoiler_text : text,
      selectionStart: selectionIndex
    }), () => {
      focusReactRefField((this.state.lastFocusedElem as RefObject<any>), selectionIndex)
    })
  }

  toggleEmojiPanel = () => {
    this.setState(prevState => ({ showEmojiPanel: !prevState.showEmojiPanel }))
    focusReactRefField((this.state.lastFocusedElem as any), this.state.selectionStart)
  }

  onMediaDelete = (id: string) => {
    const media = this.state.media.filter(item => item.id !== id)

    if ((this.mediaUploadRef as any).current && (this.mediaUploadRef as any).current.updateMedia) {
      (this.mediaUploadRef as any).current.updateMedia(media)
    }
    this.setState(({ media }))
 }

  renderOptionsPanel = () => (
    <div className='flex items-start mb-4' >
      <MediaUpload
        className='mr-4'
        onUpload={(media: Attachment[]) => this.setState({ media })}
        ref={this.mediaUploadRef}
      />
      <div
        className='flex items-center cursor-pointer mr-4'
        onClick={() => this.setState(({ showPollPanel }) => ({ showPollPanel: !showPollPanel }))}
      >
        <Icons.Poll className='text-secondary link icon w-6 h-6' />
        <span className='hidden md:block font-semibold text-sm text-secondary link icon'>
          <FormattedMessage defaultMessage='Add Poll' id='postStatusForm.addPoll' />
        </span>
      </div>
      <div
        className='flex items-center cursor-pointer'
        onClick={this.toggleEmojiPanel}
      >
        <Icons.Emoji className='text-secondary link icon w-6 h-6' />
        <span className='hidden md:block font-semibold text-sm text-secondary link icon'>
          <FormattedMessage defaultMessage='Add Emojis' id='postStatusForm.addEmojis' />
        </span>
      </div>
    </div>)

  onKeyDown = (e: any) => {
    e.persist()
    if (e.key === 'Enter' && (e.ctrlKey || e.metaKey)) {
      this.postStatus()
    }
  }

  render() {
    const { intl } = this.props
    const {
      spoiler_text,
      status, visibility,
      showPollPanel,
      showEmojiPanel,
      in_reply_to_id,
      media,
      suggestions,
      searchRequestsCounter,
      errorText
    } = this.state
    const visibilityOptions = STATUS_VISIBILITY_OPTIONS.map(item => ({
      ...item,
      text: intl.formatMessage({ id: item.textId })
    }))
    const spoilerTextPlaceholder = intl.formatMessage({ id: 'postStatusForm.title' })
    const statusPlaceholder = intl.formatMessage({ id: 'postStatusForm.text' })

    return (
      <div
        className='flex flex-col md:justify-between h-full py-4 px-5 pb-16 md:pb-4 foreground overflow-y-auto'
        onKeyDown={this.onKeyDown}
      >
        <div>
          <div className='flex justify-end mb-4 md:mb-16'>
            <button
              onClick={this.postStatus}
              className='button-primary rounded-lg px-4 py-2 leading-tight text-sm font-bold md:hidden h-10'>
              <FormattedMessage id='postStatusForm.publish' defaultMessage='Publish' />
            </button>
          </div>
          {!!errorText && <ErrorLabel error={errorText} />}
          <Autocomplete
            ref={this.autocompleteRef}
            results={suggestions}
            onSelect={this.onAutocompleteSelect}
            isLoading={!!searchRequestsCounter}
          >
            <textarea
              className='w-full input focus:outline-none mb-4 rounded-sm text-lg text resize-none'
              value={spoiler_text}
              placeholder={spoilerTextPlaceholder}
              ref={this.spoilerTextRef}
              onKeyDown={this.onTextFieldKeyDown}
              rows={1}
              {...this.getTextFieldHandlers(this.spoilerTextRef)}
            />
            <textarea
              className='w-full rounded-sm input mb-2 text-sm focus:outline-none text'
              value={status}
              placeholder={statusPlaceholder}
              ref={this.statusTextRef}
              {...this.getTextFieldHandlers(this.statusTextRef)}
            />
          </Autocomplete>
          { showPollPanel &&
            <PollForm
              onChange={(val: any) => this.setState({ poll: val })}
              ref={this.pollFormRef}
            />
          }
          { showEmojiPanel && <EmojiPanel onSelect={(res: { type: string, res: any }) => this.onAutocompleteSelect(res, true)} />}
          { this.renderOptionsPanel() }
          { media.length > 0 && <Attachments
            attachments={media}
            onFileDelete={this.onMediaDelete}
            hideAttachments={false}
          /> }
        </div>
        <div className='flex justify-between w-full items-end mb-4'>
          <Select
            multiple={false}
            defaultValue={(visibilityOptions.find(({ value }) => visibility === value) || { value: '', text: '' }).value}
            options={visibilityOptions}
            onSelect={(visibility: string) => this.setState({ visibility })}
            fullWidth={true}
            disabled={Boolean(in_reply_to_id)}
          />
          <button
            onClick={this.postStatus}
            className='ml-auto hidden md:block button-primary rounded-lg px-4 py-2 leading-tight text-sm font-bold h-10'
          >
            <FormattedMessage defaultMessage='Publish' id='postStatusForm.publish' />
          </button>
        </div>
      </div>
    )
  }
}
const mapStateToProps = ({ users, config: { emojis } }: { users: any, config: { emojis: any[] } }) => ({
  currentUser: users.currentUser,
  users: Object.values(users.usersByIds),
  emojis: emojis || [],
})
const mapDispatchToProps = (dispatch: Function) => ({ dispatch })
const mergeProps = (stateProps: any, { dispatch }: { dispatch: Function }, ownProps: any) => ({
  postStatus: async (params: any) => {
    const response = await dispatch(Pleroma.thunks.statuses.postStatus({ params }))
    dispatch(Pleroma.reducers.users.actions.updateCurrentUser({ ...stateProps.currentUser, statuses_count: stateProps.currentUser.statuses_count + 1 }))
    return response
  },
  searchUsers: (q: string) => dispatch(Pleroma.thunks.api.usersSearch({ queries: { q } })),
  ...stateProps,
  ...ownProps
})
export const PostStatusForm = connect(mapStateToProps, mapDispatchToProps, mergeProps, { forwardRef: true })(PostStatusFormComponent)
