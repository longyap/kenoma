import React from 'react'
import { IonPage } from '@ionic/react'
import Icons from '../common/icons/icons'
import { Route, useLocation, Redirect } from 'react-router'
import { InstanceSelection } from '../instance_selection/instance_selection'
import { SignUp } from '../sign_up/sign_up'
import { MobileLogin } from '../mobile_login/mobile_login'
import { connect } from 'react-redux'
import { oauthActions } from '../../reducers/oauth'
import { ForgotPassword } from '../forgot_password/forgot_password'

import './login_layout.css'

type LoginLayoutType = { clearError?: Function }
export const LoginLayoutComponent = ({ clearError }: LoginLayoutType) => {
  const location = useLocation()
  React.useEffect(() => {
    clearError && clearError()
  // eslint-disable-next-line
  }, [location.pathname])
  return (
    <IonPage className='login-layout items-center background-login pt-6 md:pt-10'>
      <div className='w-full max-w-xs max-h-full'>
        <div className='mb-24 flex flex-col items-center'>
          <Icons.Logo className='logo-login w-8 h-8' />
          <p className='logo-login my-0 font-bold'>Kenoma</p>
        </div>
        <Route path='/login' render={() => <MobileLogin />}/>
        <Route path='/sign-up' component={SignUp} />
        <Route path='/forgot-password' component={ForgotPassword} />
        <Route path='/' exact={true} render={() => <InstanceSelection />} />
        <Redirect to='/' />
      </div>
    </IonPage>)
}

const mapDispatchToProps = (dispatch: Function) => ({
  clearError: () => dispatch(oauthActions.setError(''))
})
export const LoginLayout = connect(null, mapDispatchToProps)(LoginLayoutComponent)
