import React from 'react'
import { connect } from 'react-redux'
import { Route, withRouter } from 'react-router-dom'
import { pathToRegexp } from 'path-to-regexp'
import {
  IonTabs,
  IonRouterOutlet,
  IonTabButton,
  IonTabBar,
  IonContent,
  IonBadge,
} from '@ionic/react'
import { HomeTimeline } from '../timeline/home_timeline'
import Icons from '../common/icons/icons'
import { Thread } from '../thread/thread.tsx'
import { UserProfile } from '../user_profile/user_profile.tsx'
import { PublicTimeline } from '../timeline/public_timeline'
import { LocalTimeline } from '../timeline/local_timeline'
import { PostStatusModal } from '../post_status/post_status_modal.tsx'
import { NotificationsPage } from '../notifications/notificationsPage.tsx'
import { ChatOverview } from '../chats/chats'
import { ChatRoom } from '../chat_room/chat_room.tsx'
import { FollowRequests } from '../follow_requests/follow_requests.tsx'
import { THREAD_URL } from './layout.tsx'
import { uiActions } from '../../reducers/ui'
import { TagPage } from '../tag/tag_page'
import { Search } from '../search/search'
import { ProfileSettingsPage } from '../settings/profile_settings'
import { MobileSettings } from '../settings/mobile_settings'
import { LocaleSelectPage } from '../settings/locale_select'
import { TimelineSettingsPage } from '../settings/timeline_settings'
import { Toast } from '../common/alerts/alerts'
import { MutesBlocksPage } from '../mutes_blocks/mutes_blocks'

const MODAL_PAGES_LIST = [pathToRegexp('/chatroom/:id')]

const MobileLayoutComponent = ({ scrollPageToTop, location, unread_notifications_count }) => {
  const renderRouter = () => (
    <IonRouterOutlet>
      <Route path={THREAD_URL} component={Thread} />
      <Route exact path='/' component={HomeTimeline} />
      <Route path='/users/:id' component={UserProfile} />
      <Route path='/timelines/public' component={PublicTimeline} />
      <Route path='/timelines/local' component={LocalTimeline} />
      <Route path='/conversations' component={ChatOverview} />
      <Route path='/chatroom/:id' component={ChatRoom} />
      <Route path='/follow-requests' component={FollowRequests} />
      <Route path='/notifications' component={NotificationsPage} />
      <Route path='/settings/profile' component={ProfileSettingsPage}/>
      <Route path='/settings/timeline' component={TimelineSettingsPage}/>
      <Route path='/settings/language' component={LocaleSelectPage}/>
      <Route path='/settings/mutes-blocks' component={MutesBlocksPage} />
      <Route path='/settings' component={MobileSettings} />
      <Route path='/tag/:tag' component={TagPage} />
      <Route path='/search' component={Search} />
    </IonRouterOutlet>
  )
  const showTabs = !MODAL_PAGES_LIST.filter(regexp => regexp.test(location.pathname)).length

  return (
    <IonContent>
      <Toast />
      <PostStatusModal />
      <IonTabs>
        {renderRouter()}
        <IonTabBar slot='bottom' style={{display: showTabs ? 'flex': 'none'}}>
          <IonTabButton tab='tab1' href='/' onClick={() => scrollPageToTop('timeline', true)}>
            <Icons.Logo className='h-8 w-8 link-navbar' />
          </IonTabButton>
          <IonTabButton tab='tab2' href='/conversations'>
            <Icons.Envelope className='h-8 w-8 link-navbar' />
          </IonTabButton>
          <IonTabButton tab='tab3' href='/search'>
            <Icons.Search className='h-8 w-8 link-navbar' />
          </IonTabButton>
          <IonTabButton tab='tab4' href='/notifications'>
            <Icons.Bell className='h-8 w-8 link-navbar' />
            {!!unread_notifications_count && <IonBadge>{unread_notifications_count}</IonBadge>}
          </IonTabButton>
          <IonTabButton tab='tab5' href='/settings'>
            <Icons.Cog className='h-8 w-8 link-navbar' />
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonContent>
  )
}

const mapDispatchToProps = (dispatch) => ({
  scrollPageToTop: (tab, scrollPosition) => dispatch(uiActions.setScrollPosition(tab, scrollPosition))
})
export const MobileLayout = withRouter(connect(null, mapDispatchToProps)(MobileLayoutComponent))