import React, { Component } from 'react'
import { Route, withRouter, RouteComponentProps } from 'react-router-dom'
import {
  IonRouterOutlet,
  IonMenu,
  IonSplitPane,
} from '@ionic/react'
import { MobileLayout } from './mobile_layout'
import { HomeTimeline } from '../timeline/home_timeline'
import { NavBar } from '../nav/nav_bar'
import { Thread } from '../thread/thread'
import { UserProfile } from '../user_profile/user_profile'
import { PublicTimeline } from '../timeline/public_timeline'
import { LocalTimeline } from '../timeline/local_timeline'
import { PostStatusModal } from '../post_status/post_status_modal'
import { NotificationsPage } from '../notifications/notificationsPage'
import { ChatOverview } from '../chats/chats'
import { ChatRoom } from '../chat_room/chat_room'
import { FollowRequests } from '../follow_requests/follow_requests'
import { TagPage } from '../tag/tag_page'
import { Search } from '../search/search'
import { IsMobileContext } from '../../isMobileContext'
import { Settings } from '../settings/settings'
import { pathToRegexp } from 'path-to-regexp'
import { ProfileSettings } from '../settings/profile_settings'
import { LocaleSelect } from '../settings/locale_select'
import { TimelineSettings } from '../settings/timeline_settings'
import { connect } from 'react-redux'
import Notification from '../../entities/notification'
import pleroma from 'pleroma-api'
import { APP_ID } from '../../constants'
import { MutesBlocks } from '../mutes_blocks/mutes_blocks'
import { Toast } from '../common/alerts/alerts'

export const THREAD_URL = '/notice/:id'
const FULLWIDTH_PAGES_LIST = [
  pathToRegexp('/settings/profile'),
  pathToRegexp('/settings/timeline'),
  pathToRegexp('/settings/language')
]

interface LayoutComponentInterface  extends RouteComponentProps {
  unread_notifications_count: number,
  startFetchingNotifications: Function,
  stopFetchingNotifications: Function
}
class LayoutComponent extends Component<LayoutComponentInterface>{
  componentDidMount() {
    const { startFetchingNotifications } = this.props

    startFetchingNotifications && startFetchingNotifications()
  }

  componentWillUnmount() {
    const { stopFetchingNotifications } = this.props

    stopFetchingNotifications && stopFetchingNotifications()
  }

  render() {
    const { unread_notifications_count, location } = this.props
    const fullWidthView = FULLWIDTH_PAGES_LIST.filter(regexp => regexp.test(location.pathname)).length

    return this.context.isMobile
    // @ts-ignore
      ? <MobileLayout unread_notifications_count={unread_notifications_count} />
      : (<IonSplitPane contentId='main'>
        <Toast />
        <IonMenu
          contentId='main'
          // @ts-ignore
          cssCustomProperties={{width: '5rem'}}
          className='sidebar'
        >
          <NavBar />
        </IonMenu>
        <div id='main' className='mt-16 mx-auto'>
          <IonRouterOutlet className={`ml-16 ${fullWidthView && 'menu-content--full-w relative'}`}>
            <Route exact={true} path='/' component={HomeTimeline} />
            <Route path='/timelines/public' component={PublicTimeline} />
            <Route path='/timelines/local' component={LocalTimeline} />
            <Route path='/users/:id' component={UserProfile} />
            <Route path='/conversations' component={ChatOverview} />
            <Route path='/chatroom/:id' component={ChatRoom} />
            <Route path='/follow-requests' component={FollowRequests} />
            <Route path={THREAD_URL} component={Thread} />
            <Route path='/tag/:tag' component={TagPage} />
            <Route path='/search' component={Search} />
            <Route path='/notifications' component={NotificationsPage} />
            <Route path='/settings/profile' component={() => <Settings><ProfileSettings /></Settings>} />
            <Route path='/settings/timeline' component={() => <Settings><TimelineSettings /></Settings>} />
            <Route path='/settings/language' component={() => <Settings><LocaleSelect /></Settings>} />
            <Route path='/settings/mutes-blocks' component={() => <Settings><MutesBlocks /></Settings>} />
          </IonRouterOutlet>
          <PostStatusModal />
        </div>
      </IonSplitPane>)
  }
}

LayoutComponent.contextType = IsMobileContext
const mapDispatchToProps = (dispatch: Function) => ({dispatch})
const mapStateToProps = ({ config, notifications: { notificationsByIds } }: { config: any, notifications: { notificationsByIds: { [key: string]: Notification } } }) => ({
  settings: (config && config[APP_ID]) ? config[APP_ID].notificationsFiltering : [],
  unread_notifications_count: Object.values(notificationsByIds).filter(item => !item.pleroma.is_seen).length,
})
export const mergeProps = (stateProps: any, { dispatch }: { dispatch: Function }, ownProps: any) => {
  // @ts-ignore
  const exclude_types = Object.keys(stateProps.settings).reduce((acc, cur) => !stateProps.settings[cur] ? acc.concat(cur) : acc, [])

  const queries: { exclude_types?: any } = {}
  if (exclude_types.length) {
    queries.exclude_types = exclude_types
  }
  return {
    ...stateProps,
    ...ownProps,
    startFetchingNotifications: () => dispatch(pleroma.thunks.api.startFetchingNotifications({ queries })),
    stopFetchingNotifications: () => dispatch(pleroma.thunks.api.stopFetchingNotifications()),
  }
}

export const Layout = withRouter(connect(mapStateToProps, mapDispatchToProps, mergeProps)(LayoutComponent))
