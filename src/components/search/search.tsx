import React, { Component, SyntheticEvent } from 'react'
import Pleroma from 'pleroma-api'
import { connect } from 'react-redux'
import { injectIntl, WrappedComponentProps, FormattedMessage } from 'react-intl'
import { IonPage, IonContent, IonSearchbar, IonHeader, withIonLifeCycle } from '@ionic/react'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import Icons from '../common/icons/icons'
import { SearchResults } from './search_results'
import debounce from 'lodash/debounce'
import SearchResult from '../../entities/searchResult'

interface Search extends WrappedComponentProps, RouteComponentProps {
  search: Function,
  deleteRecentSearch: Function,
  searchCache: string[]
}
type SearchState = {
  search: string,
  loading: boolean,
  searchResults: any[],
  activeTab: number,
  noSearchResults: boolean,
  prevLocation: string | null,
  recentSearchMenuIsOpen: boolean
}

const defaultSearchState = {
    searchResults: [],
    activeTab: 0,
    noSearchResults: false
}

class SearchComponent extends Component<Search, SearchState> {
  constructor(props: Search) {
    super(props)

    this.state = {
      loading: false,
      search: '',
      prevLocation: null,
      recentSearchMenuIsOpen: false,
      ...defaultSearchState
    }
  }
  popover: React.RefObject<any> = React.createRef()
  searchInput: React.RefObject<any> = React.createRef()

  ionViewDidEnter() {
    const { history } = this.props as any

    if (history.location.state && history.location.state.prevLocation) {
      this.setState({ prevLocation: history.location.state.prevLocation })
    }
    if (history.location.search && history.location.search.length) {
      this.setState({ search: decodeURI(history.location.search.slice(1)) }, () => {
        this.search()
      })
    }
  }

  ionViewWillLeave() {
    this.setState({ search: '' })
    this.clearSearchResults()
  }

  search = async () => {
    this.setState({ loading: true })
    const res: { results: SearchResult, q: string } = await this.props.search(this.state.search)
    if (res.q === this.state.search) {
      const searchResults = Object.entries(res.results).reduce((result: any[], item: any[]) =>
        !!item[1].length ? result.concat({ key: item[0], data: [...item[1]] }) : result,
      [])

      this.setState({
        searchResults,
        noSearchResults: !searchResults.length && !!this.state.search.length,
        loading: false
      })
    }
  }

  debouncedSearch = debounce(this.search, 150)

  setSearchState = (val: string) => {
    this.setState({ search: val })
    this.props.history.replace({ search: val })
  }

  clearSearchResults = () => {
    this.setState({...defaultSearchState})
  }

  searchInputChange = (e: any) => {
    this.setState({ recentSearchMenuIsOpen: !e.detail.value.length })
    this.initiateSearchRequest(e.detail.value)
  }

  initiateSearchRequest  = (val: string) => {
    this.setSearchState(val)
    this.clearSearchResults()
    if (!!val.length) {
      this.debouncedSearch()
    }
  }

  goBack = () => {
    this.setState({ search: '' })
    this.props.history && this.state.prevLocation
      ? this.props.history.push(this.state.prevLocation)
      : this.props.history.go(-1)
  }
  hideRecentSearches = (e: any) => {
    const path = e.path || (e.composedPath && e.composedPath());

    if (path) {
      if (!path.includes(document.getElementById('search'))) {
        (document.getElementById('root') as any).removeEventListener('click', this.hideRecentSearches)
        this.setState({ recentSearchMenuIsOpen: false })
      }
    }
  }

  openRecentSearches = async (e: SyntheticEvent) => {
    // HACK to hide recent searches
    (document.getElementById('root') as any).addEventListener('click', this.hideRecentSearches)
    this.setState({ recentSearchMenuIsOpen: true })
  }

  deleteRecentSearch = (item: string, e: SyntheticEvent) => {
    e.stopPropagation()
    this.props.deleteRecentSearch(item)
  }

  renderRecentSearches = () => (
    <div className='fixed rounded foreground recent-search ml-2'>
      {this.props.searchCache.map((item: string) => <div
        className='text cursor-pointer flex items-center justify-between px-4 py-1'
        key={item}
        onClick={() => this.initiateSearchRequest(item)}
      >
        <span>{item}</span>
        <span
          onClick={(e) => this.deleteRecentSearch(item, e)}
          className='w-4 h-4'
        >
          <Icons.Close className='w-4 h-4' />
        </span>
      </div>)}
    </div>
  )

  render() {
    const { intl } = this.props
    const { search, loading, searchResults, noSearchResults } = this.state
    const searchInputPlaceholder = intl.formatMessage({ id: 'search.placeholder' })

    return (
      <IonPage className='py-4 px-6'>
        <IonHeader translucent={true} className='ion-header mb-6'>
          <div className='flex items-center'>
            <div onClick={this.goBack} className='w-8'>
              <Icons.ChevronRight rotateDeg={180} className='w-8 h-8 text-secondary text-highlighted' />
            </div>
            <div id='search' className='relative w-full'>
              <IonSearchbar
                onIonChange={this.searchInputChange}
                onFocus={this.openRecentSearches}
                className='pa-0 w-full'
                debounce={0}
                placeholder={searchInputPlaceholder}
                value={search}
                searchIcon={''}
                ref={this.searchInput}
              />
              {this.state.recentSearchMenuIsOpen && this.renderRecentSearches()}
            </div>
          </div>
        </IonHeader>
        <IonContent>
          <div>
            <div className='flex justify-center text-secondary'>
              {loading && <Icons.Cog className='w-10 h-10 a-loader-spin' />}
            </div>
            { !loading && !noSearchResults && <SearchResults searchResults={searchResults} />}
            { noSearchResults && <div className='text-center note'>
              <FormattedMessage defaultMessage='No results were found' id='search.noResults' />
            </div>}
          </div>
        </IonContent>
      </IonPage>
    )
  }
}

const mapStateToProps = ({ api: { searchCache } }: { api: { searchCache: string[] } }) => ({
  searchCache
})
const mapDispatchToProps = (dispatch: Function) => ({
  search: (q: string) => dispatch(Pleroma.thunks.api.search({ queries: { q, with_relationships: true } })),
  deleteRecentSearch: (request: string) => dispatch(Pleroma.reducers.api.actions.removeItemFromSearchCache({ request }))
})
export const Search = connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(withIonLifeCycle(SearchComponent))))
