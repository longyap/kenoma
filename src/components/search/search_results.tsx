import React, { useState } from 'react'
import { Status } from '../status/status'
import { ProfileCardSm } from '../profile_card/profile_card_sm'
import { FormattedMessage } from 'react-intl'
import { IonButtons, IonButton } from '@ionic/react'
import { HashtagSearchResult } from '../hashtag/hashtag_search_result'
import StatusInterface from '../../entities/status'
import Tag from '../../entities/tag'

type SearchResults = {
  searchResults: any[]
}

const COMPONENTS_CONFIG: any = {
  accounts: {
    component: ProfileCardSm,
    propName: 'account',
    props: {
      actions: ['follow']
    },
    formattedMessage: 'people'
  },
  statuses: {
    component: Status,
    propName: 'status',
    formattedMessage: 'posts'
  },
  hashtags: {
    component: HashtagSearchResult,
    propName: 'tag',
    formattedMessage: 'hashtags'
  }
}

export const SearchResults = ({ searchResults }: SearchResults) => {
  const activeTabState = useState(0)

  const createResultComponent = (entity: { key: string, data: any[] }, item: StatusInterface | Account | Tag, key: number) => {
    const componentConfig = COMPONENTS_CONFIG[entity.key]
    const props = { [componentConfig.propName]: item, key, ...componentConfig.props }

    return React.createElement(componentConfig.component, props)
  }
  return (<div>
    <IonButtons className='flex mb-8'>
    {searchResults.map((entity, index) => (<IonButton
        key={index}
        onClick={() => {activeTabState[1](index)}}
        className={`${activeTabState[0] === index && 'text border-dark'} border-b text-secondary flex-grow cursor-pointer capitalize font-normal`}
      >
        <FormattedMessage id={`search.${COMPONENTS_CONFIG[entity.key].formattedMessage}`} defaultMessage={COMPONENTS_CONFIG[entity.key].formattedMessage} />
      </IonButton>))}
    </IonButtons>
    { searchResults.map((entity, index) => activeTabState[0] === index ? entity.data.map((item: StatusInterface | Account | Tag, index: number) => createResultComponent(entity, item, index)) : null)}
  </div>)
}