import React, { useState } from 'react'
import EmojiReaction from '../../entities/emoji_reaction'
import { connect } from 'react-redux'
import Pleroma from 'pleroma-api'
import { FormattedMessage } from 'react-intl'
import { UsersListTooltip } from '../users_list_tooltop/users_list_tooltip'

const REACTION_ITEM_CLASS = 'cursor-pointer mr-2 mb-1 px-2 rounded-full flex items-center box-border reaction'
const MAX_REACTIONS_NUMBER = 12
interface EmojiReactionsInterface {
  emojiReactions: EmojiReaction[]
  statusId: String,
  reblogStatusId?: String,
  userId?: String,
  toggleEmojiReaction: Function,
  loadEmojiReactions: Function
}

const EmojiReactionComponent = ({
  emojiReactions,
  statusId,
  reblogStatusId,
  toggleEmojiReaction,
  loadEmojiReactions,
  userId
}: EmojiReactionsInterface) => {
  const loading = useState(false)
  const isShowMoreLinkOpen = useState(false)
  const overweightReactionsNumber = emojiReactions.length > MAX_REACTIONS_NUMBER
  const computedEmojiReactions = overweightReactionsNumber && !isShowMoreLinkOpen[0] ? emojiReactions.slice(0, MAX_REACTIONS_NUMBER) : emojiReactions

  const loadReactions = async() => {
    const notFullyLoaded = emojiReactions.some(reaction => !reaction.accounts)

    if (notFullyLoaded) {
      loading[1](true)
      loadEmojiReactions && await loadEmojiReactions({ statusId, userId, reblogStatusId })
      loading[1](false)
    }
  }

  const onEmojiSelect = ({ res }: any) => {
    const selectedEmoji = res.unicode || res.name
    const existedReaction = emojiReactions.find((item: EmojiReaction) => item.name.codePointAt(0) === selectedEmoji.codePointAt(0));
    const reacted = existedReaction ? existedReaction.me : false

    toggleEmojiReaction(selectedEmoji, statusId, reacted)
  }

  return (<div>
    <div className='text flex flex-wrap'>
      {computedEmojiReactions.map(item => <UsersListTooltip
        key={item.name}
        accounts={item.accounts}
        loading={loading[0]}
      >
        <span>
          <button
            className={`${REACTION_ITEM_CLASS} ${item.me ? 'own-reaction' : ''}`}
            onClick={() => onEmojiSelect({ res: item })}
            onMouseEnter={loadReactions}
          >
            {item.name}
            <span className='text-xs ml-1 text-secondary'>{item.count}</span>
          </button>
        </span>
      </UsersListTooltip>)}
      </div>
      {overweightReactionsNumber && <button className='w-full link focus:outline-none my-2' onClick={() => isShowMoreLinkOpen[1](!isShowMoreLinkOpen[0])}>
        <FormattedMessage
          id={`show.${isShowMoreLinkOpen[0] ? 'less' : 'more'}`}
          defaultMessage={`Show ${isShowMoreLinkOpen[0] ? 'less' : 'more'}`}
        />
      </button>}
  </div>)
}

const mapDispatchToProps = (dispatch: Function) => ({
  toggleEmojiReaction: (emoji: String, statusId: String, reacted: boolean) => dispatch(Pleroma.thunks.reactions.toggleReaction({ params: { statusId, emoji }, reacted })),
  loadEmojiReactions: (params: { statusId: String, userId: String, reblogStatusId: String }) =>  dispatch(Pleroma.thunks.reactions.getReactions({ params }))
})
export const EmojiReactions = connect(null, mapDispatchToProps)(EmojiReactionComponent)
