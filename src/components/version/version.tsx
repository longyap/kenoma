import React from 'react'
import { GITLAB_REPO_URL } from '../../constants'

export const Version = ({ cssClass }: { cssClass?: string }) => {
  const version = window.VERSION

  return (
    <div className={`flex items-center ml-4 text mt-auto ${cssClass}`}>
      <span>Version: </span>
      <a
        target='_blank'
        rel='noopener noreferrer'
        className='link cursor-pointer'
        href={`${GITLAB_REPO_URL}/${version}`}>
          {version}
      </a>
    </div>
  )
}
