import React, { Component, SyntheticEvent } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { withRouter } from 'react-router-dom'
import { oauthThunks } from '../../thunks/oauth'
import { FormattedMessage } from 'react-intl'
import Pleroma from 'pleroma-api'
import { oauthActions } from '../../reducers/oauth'
import { Input, generateErrorMessage } from '../input/input'
import { validate } from '../../utils/validators'
import Icons from '../common/icons/icons'
import { InstanceSwitcher } from './instance_switcher'
import { IsMobileContext } from '../../isMobileContext'
import { ForgotPasswordLink } from '../forgot_password/forgot_password_link'
import { Tooltip } from '../common/tooltip/tooltip'
import { Error } from '../common/alerts/alerts'

const computeInstanceName = (instance: string): string => instance.includes('https') ? instance : `https://${instance}`
interface InstanceSelectionInterface extends RouteComponentProps{
  config: any;
  instance: string;
  getNodeInfo: Function,
  setInstance: Function;
  areRegistrationsOpen: boolean,
  login: Function;
  error: string;
}
type InstanceSelectionState = {
  instance: string,
  errors: { [key: string]: any },
  loading: boolean
}
class InstanceSelectionComponent extends Component<InstanceSelectionInterface, InstanceSelectionState> {
  constructor (props: InstanceSelectionInterface) {
    super(props)
    this.state = {
      instance: '',
      errors: {},
      loading: false
    }
  }
  validator = {
    instance: ['required'],
  }

  componentDidMount () {
    const { instance, getNodeInfo } = this.props

    if (instance) {
      getNodeInfo && getNodeInfo()
    }
  }

  onSubmit = async (e: SyntheticEvent) => {
    e.preventDefault()
    this.setState({ loading: true })
    const validationRes = validate(this.state, this.validator)
    if (typeof validationRes === 'object') {
      this.setState({ errors: validationRes })
    } else {
      this.props.setInstance && await this.props.setInstance(this.state)
    }
    this.setState({ loading: false })
  }

  login = () => {
    const { login, history } = this.props

    if (this.context.isMobile) {
      history.push('/login')
    } else {
      login && login()
    }
  }

  signUp = async () => {
    const {areRegistrationsOpen, history } = this.props

    if (areRegistrationsOpen) {
      history.push('/sign-up')
    }
  }

  render () {
    const { error, areRegistrationsOpen } = this.props
    const { instance, errors, loading } = this.state

    return (this.props.instance)
    ? <div className='flex flex-col items-center'>
      <InstanceSwitcher />

      {error && <Error error={error} />}
      <ForgotPasswordLink />
      <button
        onClick={this.login}
        className='login-btn'>
          <FormattedMessage defaultMessage='Login' id='auth.login' />
      </button>
      { areRegistrationsOpen
        ? <div onClick={this.signUp} className='logo-login cursor-pointer'>Sign up</div>
        : <Tooltip
          placement={this.context.isMobile ? 'top' : 'auto'}
          target={<div className='logo-login cursor-not-allowed'>Sign up</div>}
          containerCssClass='users-list-tooltip'
        >
          <FormattedMessage id='auth.signUpIsNotAllowed' defaultMessage='Sorry...The registrations are not allowed on this instance' />
        </Tooltip>
      }
    </div>
    : <form className='px-8 pt-6 pb-8 mt-10 flex items-center flex-col' onSubmit={this.onSubmit}>
      <div className='mb-4 w-full'>
        <label className='block logo-login text-sm text-center mb-6'>
          Enter your instance URL
        </label>
        <Input
          value={instance}
          className='mb-4'
          placeholder='https://pleroma.soykaf.com'
          onChange={(e: any) => this.setState({ instance: e.target.value })}
          error={errors.instance}
          errorMessage={generateErrorMessage([...errors.instance || []])}
        />
      </div>
      {error && <Error error={error} />}
      <button
        className='login-btn'
        disabled={loading}
      >
        {loading
          ? <Icons.Cog className='w-4 h-4 a-loader-spin' />
          : <FormattedMessage defaultMessage='Set Instance' id='auth.setInstance' />}
      </button>
    </form>
  }
}

const mapDispatchToProps = (dispatch: Function) => ({dispatch})
const mapStateToProps = (state: any) => ({
  error: state.oauth.error,
  config: state.api.config,
  areRegistrationsOpen: state.config.nodeinfo ? state.config.nodeinfo.openRegistrations : true,
})
const mergeProps = (stateProps: any, { dispatch }: { dispatch: Function }, ownProps: any) => ({
  ...stateProps,
  ...ownProps,
  instance: stateProps.config.instance ? stateProps.config.instance.indexOf('undefined') !== -1 ? null : stateProps.config.instance : null,
  getNodeInfo: async () => {
    const result = await Pleroma.api.configs.getInstanceConfigurations({ config: {...stateProps.config, instance: computeInstanceName(stateProps.config.instance) } })
    const nodeinfo = result.reduce((acc: any, { value }: { value: any }) => (value.data ? { ...acc, ...value.data } : acc), {})

    return dispatch(Pleroma.reducers.config.actions.addConfig({ config: { nodeinfo } }))
  },
  login: async () => {
    try{
      dispatch(oauthThunks.gotoLogin({ params: { client_name: `Kenoma_${Date.now()}` } }))
    } catch (e) {
      dispatch(oauthActions.setError('Something went wrong'))
    }
  },
  setInstance: async (state: { instance: string }) => {
    const errorMessage = 'Something is wrong with instance. Try later'
    const computedInstance = computeInstanceName(state.instance)

    try{
      dispatch(oauthActions.setError(''))
      const result = await Pleroma.api.configs.getInstanceConfigurations({ config: {...stateProps.config, instance: computedInstance } })
      const nodeinfo = result.reduce((acc: any, { value }: { value: any }) => (value.data ? { ...acc, ...value.data } : acc), {})

      if (!nodeinfo) {
        throw errorMessage
      } else {
        // @ts-ignore
        return await Promise.allSettled([
          dispatch(oauthThunks.setInstance({ instance: computedInstance })),
          dispatch(Pleroma.reducers.config.actions.addConfig({ config: { nodeinfo } }))
        ])
      }
    } catch {
      return dispatch(oauthActions.setError(errorMessage))
    }
  }
})
InstanceSelectionComponent.contextType = IsMobileContext
export const InstanceSelection = withRouter(connect(mapStateToProps, mapDispatchToProps, mergeProps)(InstanceSelectionComponent))
