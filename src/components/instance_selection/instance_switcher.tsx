import React from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router'
import Icons from '../common/icons/icons'
import { oauthThunks } from '../../thunks/oauth'

interface InstanceSwitcherType extends RouteComponentProps {
  instance: string | null,
  clearInstance: Function
}

const InstanceSwitcherComponent = ({ instance, clearInstance, history }: InstanceSwitcherType) => {
  const setAnotherInstance = async () => {
    clearInstance && await clearInstance()
    history.push('/')
  }

  return (
    <div className='w-full'>
      <div
        className='text-xs logo-login mb-4 cursor-pointer flex items-center w-full'
        onClick={setAnotherInstance}
      >
        <Icons.ChevronRight rotateDeg={180} className='w-4 h-4' />
        <span>Set another instance</span>
      </div>
      <div className='mb-4 w-full'>
        <div className='text-sm mb-4 text-secondary'>{instance}</div>
      </div>
    </div>
  )
}

const mapStateToProps = ({ api: { config: { instance } } } : { api: { config: { instance: string | null }}}) => ({
  instance
})
const mapDispatchToProps = (dispatch: Function) => ({
  clearInstance: () => dispatch(oauthThunks.setInstance({ instance: 'https://undefined' }))
})
export const InstanceSwitcher = connect(mapStateToProps, mapDispatchToProps)(withRouter(InstanceSwitcherComponent))