import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pleroma from 'pleroma-api'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import Icons  from '../common/icons/icons'
import { Status } from '../status/status'
import { IonPage, withIonLifeCycle, IonContent } from '@ionic/react'
import { notFoundErrorCatcher } from '../../utils/api'
import { injectIntl } from 'react-intl'
import StatusType from '../../entities/status'

interface ThreadComponentInterface extends RouteComponentProps {
  id?: string,
  status: StatusType,
  getStatusWithContext: Function
}

let updateThreadFetcher: any = null

class ThreadComponent extends Component<ThreadComponentInterface> {
  async ionViewDidEnter() {
    this.toggleUpdate()
  }
  async ionViewWillLeave() {
    this.toggleUpdate()
  }

  async componentDidUpdate (previousProps: ThreadComponentInterface) {
    if (this.props.id !== previousProps.id) {
      updateThreadFetcher = null
      await this.toggleUpdate()
    }
  }

  toggleUpdate = () => {
    if (updateThreadFetcher) {
      clearInterval(updateThreadFetcher)
      updateThreadFetcher = null
    } else {
      const { getStatusWithContext } = this.props

      getStatusWithContext()
      updateThreadFetcher = setInterval(getStatusWithContext, 5000)
    }
  }

  render () {
    const { status, history } = this.props

    return (
      <IonPage className='page-margin'>
        <div className='cursor-pointer ml-6 md:ml-0' onClick={() => history.goBack()}>
          <Icons.ChevronRight rotateDeg={180} className='header text-highlighted w-8 h-8' />
        </div>
        <IonContent>
          <div className='pt-4 pb-12 md:pb-0 px-6 md:pl-0 md:pr-4 overflow-y-auto overflow-x-hidden'>
            {status && <div>
              {status.context && status.context.ancestors.map((status: StatusType) =>
                <Status key={status.id} status={status} toggleThreadFetcher={this.toggleUpdate} />
              )}
              {!status.reblog && <Status status={status} toggleThreadFetcher={this.toggleUpdate} />}
              {status.context && status.context.descendants.map((status: StatusType) =>
                <Status key={status.id} status={status} toggleThreadFetcher={this.toggleUpdate} />
              )}
            </div>}
          </div>
        </IonContent>
      </IonPage>
    )
  }
}

const mapStateToProps = ({ api: { config }, statuses: { statusesByIds } }: { api: { config: any }, statuses: { statusesByIds: any } }) => ({
  config,
  statusesByIds
})
const mapDispatchToProps = (dispatch: Function) => ({ dispatch })
const mergeProps = (stateProps: any, { dispatch }: { dispatch: Function }, ownProps: any) => {
  const id = ownProps.match.params.id
  const status = stateProps.statusesByIds[id]

  return {
    id,
    status,
    getStatusWithContext: () => dispatch(Pleroma.thunks.statuses.getStatusWithContext({ config: stateProps.config, params: { id } }))
      .catch((e: Error) => {
        if (e.name === 'NotFoundError') {
          dispatch(Pleroma.reducers.statuses.actions.deleteStatus({ statusId: id }))
        }
        notFoundErrorCatcher(e, 'status', dispatch, ownProps.history, ownProps.intl)
      })
  }
}

const ThreadIonicComponent = withIonLifeCycle(ThreadComponent)
export const Thread = injectIntl(connect(mapStateToProps, mapDispatchToProps, mergeProps)(withRouter(ThreadIonicComponent)))
