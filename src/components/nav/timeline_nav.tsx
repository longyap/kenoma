import React, { SyntheticEvent, useState } from 'react'
import { NavLink } from 'react-router-dom'
import Icons from '../common/icons/icons'
import { FormattedMessage } from 'react-intl'
import { Popover } from '../common/popover/popover'

const TIMELINE_LINKS = [{
  link: '/',
  label: 'home'
}, {
  link: '/timelines/public',
  label: 'public'
}, {
  link: '/timelines/local',
  label: 'local'
}]

export const TimelineNav = ({ activeLink }: { activeLink: string }) =>  {
  const link = TIMELINE_LINKS.find(({ label }) => (activeLink === label))
  const isOpen = useState(false)
  const popover: React.RefObject<any> = React.createRef()

  const open = (e: SyntheticEvent) => {
    e.persist()
    isOpen[1](true)
    popover.current.open(e)
  }
  const close = () => {
    isOpen[1](false)
    popover.current.close()
  }
  return link
    ? (<span className='flex md:mb-6 mb-4 px-6 md:px-0 h-12'>
      <Popover
        ref={popover}
        onClose={() => isOpen[1](false)}
      >
        <ul className='list-reset'>
        {TIMELINE_LINKS.map(({ label, link }) => (<li
            key={link}
            onClick={close}
          >
            <NavLink
              className='inline-block no-underline px-2 link-navbar capitalize'
              to={link}
              exact={link === '/'}
            >
              <FormattedMessage defaultMessage={label} id={`timelineNav.${label}`} />
            </NavLink>
          </li>))}
        </ul>
      </Popover>
      <span
        className='page-title flex items-center'
        onClick={open}
      >
        <FormattedMessage defaultMessage={link.label} id={`timelineNav.${link.label}`} />
        <Icons.ChevronRight rotateDeg={isOpen[0] ? 270 : 90} />
      </span>
    </span>)
  : null
}
