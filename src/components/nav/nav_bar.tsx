import React from 'react'
import { IonBadge } from '@ionic/react'
import { connect } from 'react-redux'
import { NavLink, withRouter, RouteComponentProps } from 'react-router-dom'
import Icons from '../common/icons/icons'
import Account from '../../entities/account'
import Notification from '../../entities/notification'
import Pleroma from 'pleroma-api'

export const locales = [ 'en', 'ru' ]

const BADGE_STYLE = {minWidth: '1.25rem', right: '1rem'}

interface NavBarInterface extends RouteComponentProps {
  currentUser: Account,
  unreadNotificationsCount: number,
  unprocessedRequestsCount: number,
  startFetchingFollowRequests: Function,
  stopFetchingFollowRequests: Function
}
class NavBarComponent extends React.Component<NavBarInterface> {
  componentDidUpdate (prevProps: NavBarInterface) {
    const { currentUser, startFetchingFollowRequests } = this.props

    if (!prevProps.currentUser && currentUser && currentUser.locked) {
      startFetchingFollowRequests()
    }
  }
  componentWillUnmount () {
    const { currentUser, stopFetchingFollowRequests } = this.props

    if (currentUser.locked) {
      stopFetchingFollowRequests()
    }
  }
  render() {
    const { currentUser, history, unprocessedRequestsCount, unreadNotificationsCount } = this.props

    return (<div>
      <header className='hidden md:block fixed h-full foreground px-4 text-xs sidebar'>
        <div className='container flex items-center flex-col justify-between h-full'>
          <section className='list-reset flex flex-col items-center'>
            <NavLink to='/' exact={true} className='p-5 logo flex'>
              <Icons.Logo className='h-10 w-10' />
            </NavLink>
            <NavLink className='link-navbar p-5 flex' to='/conversations'>
              <Icons.Envelope className='h-10 w-10' />
            </NavLink>
            <NavLink to='/notifications' className='link-navbar p-5 flex relative'>
              <Icons.Bell className='h-8 w-8' />
              {!!unreadNotificationsCount && <IonBadge style={BADGE_STYLE} className='rounded-full absolute'>{unreadNotificationsCount}</IonBadge>}
            </NavLink>
            {currentUser.locked && <NavLink className='link-navbar p-5 flex' to='/follow-requests'>
              <Icons.InviteUser className='h-10 w-10' />
              {!!unprocessedRequestsCount && <IonBadge style={BADGE_STYLE} className='rounded-full absolute'>{unprocessedRequestsCount}</IonBadge>}
            </NavLink>}
            <NavLink
              className='link-navbar p-5 flex'
              to={{ pathname: '/search', state: {prevLocation: history.location.pathname } }}
            >
              <Icons.Search className='h-8 w-8' />
            </NavLink>
            <NavLink
              className='link-navbar p-5 flex'
              to={{ pathname: '/settings/profile', state: {prevLocation: history.location.pathname } }}
            >
              <Icons.Cog className='h-10 w-10' />
            </NavLink>
          </section>
          <section className='pb-10'>
            <NavLink to={{ pathname: `/users/${currentUser.acct}`, state: {prevLocation: history.location.pathname }}} >
              <img className='w-10 h-10 rounded-lg cursor-pointer' alt={currentUser.acct} src={currentUser.avatar_static} />
            </NavLink>
          </section>
        </div>
      </header>
    </div>)
  }
}
const mapStateToProps = ({
  notifications: { notificationsByIds },
  users: { currentUser },
  followRequests: { unprocessedRequestsCount }
}: {
  notifications: { [key: string]: Notification },
  users: { currentUser: Account },
  followRequests: { unprocessedRequestsCount: number }
}) => {
  return {
    currentUser,
    unprocessedRequestsCount,
    unreadNotificationsCount: Object.values(notificationsByIds).filter(item => !item.pleroma.is_seen).length
  }
}
const mapDispatchToProps = (dispatch: Function) => ({
  startFetchingFollowRequests: () => dispatch(Pleroma.thunks.api.startFetchingFollowRequests({})),
  stopFetchingFollowRequests: () => dispatch(Pleroma.thunks.api.stopFetchingFollowRequests({}))
})

export const NavBar = connect(mapStateToProps, mapDispatchToProps)(withRouter(NavBarComponent))
