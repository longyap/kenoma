import React, { Component, SyntheticEvent } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { IonLabel } from '@ionic/react'
import { Input } from '../common/input/input'
import { validate } from '../../utils/validators'
import { generateErrorMessage } from '../input/input'
import { oauthThunks } from '../../thunks/oauth'
import { Error } from '../common/alerts/alerts'
import { withRouter, RouteComponentProps } from 'react-router'

interface ChangePasswordInterface extends RouteComponentProps {
  changePassword: Function,
}
type ProfileSettingsState = {
  isLoading: boolean,
  formData: { [key: string]: any },
  errors: any,
  serverError: string
}

const defaultState = {
  isLoading: false,
  formData: {
    password: '',
    new_password: '',
    new_password_confirmation: ''
  },
  errors: {},
  serverError: ''
}
class ChangePasswordComponent extends Component<ChangePasswordInterface, ProfileSettingsState> {
  constructor(props: ChangePasswordInterface) {
    super(props)
    this.state = { ...defaultState }
  }

  componentDidUpdate(prevProps: ChangePasswordInterface) {
    if (this.props.location !== prevProps.location) {
      this.setState({ ...defaultState })
    }
  }

  validator = {
    password: ['required'],
    new_password: ['required'],
    new_password_confirmation: ['required', ['sameAs', 'new_password']],
  }

  onInputChange = (e: any, field: string) => {
    this.setState({
      formData: { ...this.state.formData, [field]: e.target.value },
    })
  }

  validate = () => {
    const validationRes = validate(this.state.formData, this.validator)
    if (typeof validationRes === 'object') {
      this.setState({ errors: validationRes })
      return false
    } else {
      this.setState({ errors: {} })
      return true
    }
  }

  changePassword = async (e: SyntheticEvent) => {
    this.setState({ isLoading: true })
    e.preventDefault()
    if (this.validate()) {
      try {
        const { changePassword, history } = this.props

        changePassword && await changePassword(this.state.formData)
        history.push('/')
      } catch (e) {
        this.setState({ isLoading: false, serverError: e })
      }
    } else {
      this.setState({ isLoading: false })
    }
  }

  render() {
    const { formData, isLoading, errors, serverError } = this.state

    return (
      <form
        onSubmit={this.changePassword}
        action='changePassword'
        className='w-full'
      >
        <div className='font-bold text mt-4'>
          <FormattedMessage
            defaultMessage='Change password'
            id='settings.editProfile.changePassword'
          />
        </div>
        <p className='font-normal note text-xs'>
          <FormattedMessage
            defaultMessage='You will be logged out after successful password change'
            id='settings.editProfile.changePasswordNote'
          />
        </p>
        {serverError && <Error error={serverError} />}
        <Input
          label={<IonLabel position='floating'>
            <FormattedMessage id='settings.editProfile.currentPassword' defaultMessage='Current password' />
            </IonLabel>}
          value={formData.password}
          onChange={(e: any) => this.onInputChange(e, 'password')}
          type='password'
          error={errors.password}
          errorMessage={generateErrorMessage([...errors.password || []])}
        />
        <Input
          label={<IonLabel position='floating'>
            <FormattedMessage id='settings.editProfile.newPassword' defaultMessage='New password' />
            </IonLabel>}
          value={formData.new_password}
          onChange={(e: any) => this.onInputChange(e, 'new_password')}
          type='password'
          error={errors.new_password}
          errorMessage={generateErrorMessage([...errors.new_password || []])}
        />
        <Input
          label={<IonLabel position='floating'>
            <FormattedMessage id='settings.editProfile.confirmPassword' defaultMessage='Confirm new password' />
            </IonLabel>}
          value={formData.new_password_confirmation}
          onChange={(e: any) => this.onInputChange(e, 'new_password_confirmation')}
          type='password'
          error={errors.new_password_confirmation}
          errorMessage={generateErrorMessage([...errors.new_password_confirmation || []])}
        />
        <button
          disabled={isLoading}
          className='button-primary rounded-lg py-2 px-4 mt-4 font-bold text-sm focus:outline-none leading-tight'>
          <FormattedMessage defaultMessage='Save' id='settings.save' />
        </button>
      </form>
  )}
}

const mapDispatchToProps = (dispatch: Function) => ({
  changePassword: (params: any) => dispatch(oauthThunks.changePassword(params))
})
export const ChangePassword = connect(null, mapDispatchToProps)(withRouter(ChangePasswordComponent))
