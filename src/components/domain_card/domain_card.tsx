import React, { useState } from 'react'
import { ActionButton } from '../action_buttons/action_button'
import Pleroma from 'pleroma-api'
import { connect } from 'react-redux'

interface DomainCardInterface {
  domain: string,
  config: any,
  relationship?: boolean,
  onActionButtonClick?: Function
}
export const DomainCardComponent  = ({ domain, config, relationship = true, onActionButtonClick }: DomainCardInterface) => {
  const [blocked, setBlockedStatus] = useState(relationship)
  const toggleBlockedState = async () => {
    try {
      const result = blocked
        ? await Pleroma.api.blocks.unblockDomain({ config, params: { domain } })
        : await Pleroma.api.blocks.blockDomain({ config, params: { domain } })
      if (result.state === 'ok') {
        setBlockedStatus(!blocked)
        onActionButtonClick && onActionButtonClick()
      }
    } catch (e) {
      // tslint:disable-next-line
      console.log(e.message)
    }
  }
  return (
    <div className='px-4 py-2 flex items-center justify-between cursor-pointer overflow-x-hidden'>
      <a target='_blank' rel='noopener noreferrer' className='link' href={`https://${domain}`} >{domain}</a>
      <ActionButton
        id={domain}
        relationship={blocked}
        toggleButtonState={toggleBlockedState}
        intlMessages={{
          stateOn: {
            hovered: {
              id: 'profileCard.unblock',
              defaultMessage: 'Unblock',
            },
            unhovered: {
              id: 'profileCard.blocked',
              defaultMessage: 'Blocked'
            }
          },
          stateOff: {
            id: 'profileCard.block',
            defaultMessage: 'Block'
          }
        }}
      />
    </div>
  )
}
const mapStateToProps = ({ config }: { config: any }) => ({
  config
})
export const DomainCard = connect(mapStateToProps)(DomainCardComponent)