import { Status } from '../status/status.tsx'
import React, { Component } from 'react'
import Icons from '../common/icons/icons'
import debounce from 'lodash/debounce'
import pleroma from 'pleroma-api'
import { IonPage, IonContent } from '@ionic/react'
import { TimelineNav } from '../nav/timeline_nav.tsx'
import { uiActions } from '../../reducers/ui'
import { FormattedMessage } from 'react-intl'

export const mapTimelineStateToProps = ({ timelineName }) => state => {
  const timeline = state.statuses.timelines[timelineName] || {
    statusIds: []
  }

  return {
    loadingOlder: state.api.timelines[timelineName].loadingOlder,
    statuses: timeline.statusIds.map((id) => state.statuses.statusesByIds[id]),
    timelineName,
    scrolledToTop: state.ui.tabs.timeline,
    isFirstLoad: !!state.statuses.timelines[timelineName]
  }
}

export const mapTimelineDispatchToProps = ({ timelineName, type, queries }) => dispatch => ({
  startFetching: () => dispatch(pleroma.thunks.api.startFetchingTimeline({ timelineName, type, queries })),
  stopFetching: () => dispatch(pleroma.thunks.api.stopFetchingTimeline({ timelineName })),
  loadOlder: () => dispatch(pleroma.thunks.api.loadOlderOnTimeline({ timelineName, type, queries })),
  cropOlder: () => dispatch(pleroma.thunks.statuses.cropOlderStatusesFromTimeline({ timelineName })),
  scrollToTop: () => dispatch(uiActions.setScrollPosition('timeline', true)),
  setScrolledStatus: () => dispatch(uiActions.setScrollPosition('timeline', false))                                                                                    
})

export const didLoadNewerStatuses = (oldStatuses, newStatuses) =>
  oldStatuses[0] && newStatuses[0] && oldStatuses[0].id !== newStatuses[0].id

export class Timeline extends Component {
  constructor (props) {
    super(props)
    this.state = { loading: false }
  }
  scrollElement = React.createRef()

  previousScrollOffset = null

  toggleLoading = (val) => {
    this.setState({ loading: val })
  }

  tryLoadMore = (fn, loading) => (event) => {
    const { current } = this.scrollElement
    this.props.setScrolledStatus && this.props.setScrolledStatus()

    if (loading) return
    if (!current) return
    if (current.scrollHeight - current.scrollTop - current.offsetHeight <= 500) {
      fn()
    }
  }

  ionViewDidEnter () {
    this.toggleLoading(true)
    const { startFetching } = this.props
    startFetching && startFetching()
  }

  ionViewWillLeave () {
    const { stopFetching } = this.props
    stopFetching() && stopFetching()
  }

  componentDidUpdate (previousProps, previousState, snapshot) {
    if ((!previousProps.statuses.length && !!this.props.statuses.length)
    || (!this.props.statuses.length && this.state.loading && this.props.isFirstLoad)) {
      this.toggleLoading(false)
    }
    if (this.previousScrollOffset !== null) {
      if (this.scrollElement.current.scrollTop !== 0) {
        this.scrollElement.current.scrollTop = this.scrollElement.current.scrollHeight - this.previousScrollOffset
        this.previousScrollOffset = null
      }
    }
  }

  shouldComponentUpdate (nextProps, nextState, nextContext) {
    if (didLoadNewerStatuses(this.props.statuses, nextProps.statuses)) {
      // Don't remove older statuses if still loading older, it causes a hole in
      // the timeline.
      if (!nextProps.loadingOlder) {
        if (this.scrollElement.current.scrollTop === 0) this.props.cropOlder()
      }
      this.previousScrollOffset = this.scrollElement.scrollTop !== 0
        ? this.scrollElement.current.scrollHeight - this.scrollElement.current.scrollTop
        : null
    }
    return true
  }

  render () {
    const { loadOlder, loadingOlder, statuses, timelineName, scrolledToTop } = this.props
    const { loading } = this.state

    return (
      <IonPage className='page-margin'>
        <TimelineNav activeLink={timelineName} />
        <span>{scrolledToTop}</span>
        <IonContent>
          <div
            className='overflow-y-auto px-6 md:pl-0 md:pr-4 max-h-full'
            ref={this.scrollElement}
            onScroll={debounce(this.tryLoadMore(loadOlder, loadingOlder), 100)}
          >
          {statuses.map(status =>
            <Status status={status} key={status.id} />
          )}
          {(loadingOlder || loading)
            ? <div className='w-6 h-6 mx-auto my-4 text-secondary'>
                <Icons.Cog className='w-10 h-10 a-loader-spin' />
              </div>
            : !statuses.length && <div className='note'>
              <FormattedMessage id='emptyTimelineMessage' defaultMessage='Looks like you haven’t statuses yet'/>
            </div>
          }
          </div>
        </IonContent>
      </IonPage>
    )
  }
}
