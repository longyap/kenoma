import { connect } from 'react-redux'
import {
  Timeline,
  mapTimelineStateToProps,
  mapTimelineDispatchToProps
} from './timeline.jsx'
import { withIonLifeCycle } from '@ionic/react'

const timelineParams = {
  timelineName: 'public',
  type: 'public'
}

export const PublicTimeline = connect(
  mapTimelineStateToProps(timelineParams),
  mapTimelineDispatchToProps(timelineParams)
)(withIonLifeCycle(Timeline))
