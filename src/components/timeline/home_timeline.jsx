import { connect } from 'react-redux'
import { withIonLifeCycle } from '@ionic/react'
import {
  Timeline,
  mapTimelineStateToProps,
  mapTimelineDispatchToProps
} from './timeline.jsx'

const timelineParams = {
  timelineName: 'home',
  type: 'home'
}

export const HomeTimeline = connect(
  mapTimelineStateToProps(timelineParams),
  mapTimelineDispatchToProps(timelineParams)
)(withIonLifeCycle(Timeline))
