import { connect } from 'react-redux'
import {
  Timeline,
  mapTimelineStateToProps,
  mapTimelineDispatchToProps
} from './timeline.jsx'
import { withIonLifeCycle } from '@ionic/react'

const timelineParams = {
  timelineName: 'local',
  type: 'public',
  queries: { local: true }
}

export const LocalTimeline = connect(
  mapTimelineStateToProps(timelineParams),
  mapTimelineDispatchToProps(timelineParams)
)(withIonLifeCycle(Timeline))
