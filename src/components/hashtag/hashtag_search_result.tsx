import React from 'react'
import { Link } from 'react-router-dom'
import Tag from '../../entities/tag'

export const HashtagSearchResult = ({ tag }: { tag: Tag }) => (
  <Link to={`tag/${tag.name}`} href='#' className='text'>
    {tag.name}
  </Link>
)
