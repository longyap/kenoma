import React, { useState } from 'react'
import Pleroma from 'pleroma-api'
import { ActionButton } from './action_button'
import { connect } from 'react-redux'
import Account from '../../entities/account'
import AccountRelationships from '../../entities/accountRelationships'

type BaseButton = {
  account: Account,
  toggleAccountState?: Function,
  className?: string,
  onStatusChange?: Function
}
type FollowRequestButton = BaseButton & {
  account: Account & { accepted?: boolean, rejected?: boolean }
  acceptFollow: Function,
  rejectFollow: Function
 }

const mapFollowButtonDispatchToProps = (dispatch: Function) => ({
  toggleAccountState: (id: string, following: boolean) => dispatch(Pleroma.thunks.users.toggleFollowState({ params: { id, following } })),
})

const FollowButton = connect(null, mapFollowButtonDispatchToProps)(({ account, toggleAccountState, className, onStatusChange }: BaseButton) => {
  const [requested, setRequested] = useState(false)

  if (account.relationships && account.relationships.requested && !requested) {
    setRequested(true)
  }

  return <ActionButton
    id={account.id}
    relationship={account.relationships ? account.relationships.following : false}
    toggleButtonState={toggleAccountState}
    onStatusChange={onStatusChange}
    className={className}
    intlMessages={{
      stateOn: {
        hovered: {
          id: 'profileCard.unfollow',
          defaultMessage: 'Unfollow',
        },
        unhovered: {
          id: 'profileCard.following',
          defaultMessage: 'Following'
        }
      },
      stateOff: {
        id: `profileCard.${requested ? 'requestSent' : 'follow'}`,
        defaultMessage: requested ? 'Request sent' : 'Follow'
      }
    }}
  />
})

const mapMuteButtonDispatchToProps = (dispatch: Function) => ({
  toggleAccountState: (id: string, muting: boolean) => dispatch(Pleroma.thunks.users.toggleMuteState({ params: { id, muting } })),
})

const MuteButton = connect(null, mapMuteButtonDispatchToProps)(({ account, toggleAccountState, className, onStatusChange }: BaseButton) => (
  <ActionButton
    id={account.id}
    relationship={account.relationships ? account.relationships.muting : false}
    toggleButtonState={toggleAccountState}
    onStatusChange={onStatusChange}
    className={className}
    intlMessages={{
      stateOn: {
        hovered: {
          id: 'profileCard.unmute',
          defaultMessage: 'Unmute',
        },
        unhovered: {
          id: 'profileCard.muted',
          defaultMessage: 'Muted'
        }
      },
      stateOff: {
        id: 'profileCard.mute',
        defaultMessage: 'Mute'
      }
    }}
  />
))

const mapBlockButtonDispatchToProps = (dispatch: Function) => ({
  toggleAccountState: (id: string, blocking: boolean) => dispatch(Pleroma.thunks.users.toggleBlockState({ params: { id, blocking } })),
})
const BlockButton = connect(null, mapBlockButtonDispatchToProps)(({ account, toggleAccountState, className, onStatusChange }: BaseButton) => (
  <ActionButton
    id={account.id}
    relationship={account.relationships ? account.relationships.blocking : false}
    toggleButtonState={toggleAccountState}
    onStatusChange={onStatusChange}
    className={className}
    intlMessages={{
      stateOn: {
        hovered: {
          id: 'profileCard.unblock',
          defaultMessage: 'Unblock',
        },
        unhovered: {
          id: 'profileCard.blocked',
          defaultMessage: 'Blocked'
        }
      },
      stateOff: {
        id: 'profileCard.block',
        defaultMessage: 'Block'
      }
    }}
  />
))

const mapFollowRequestButtonsDispatchToState = (dispatch: Function) => ({
  acceptFollow: (id: string) => dispatch(Pleroma.thunks.followRequests.accept({ params: { id } })),
  rejectFollow: (id: string) => dispatch(Pleroma.thunks.followRequests.reject({ params: { id } }))
})
const FollowRequestButtons = connect(null, mapFollowRequestButtonsDispatchToState)(({
  account, className, onStatusChange, acceptFollow, rejectFollow
}: FollowRequestButton) => {
  const [computedRequest, setRequest] = useState(account)
  const [loading, setLoading] = useState(false)
  const acceptFollowRequest = async () => {
    setLoading(true)
    const result = await acceptFollow(computedRequest.id)

    setRequest({
      ...computedRequest,
      accepted: true,
      rejected: false,
      relationships: new AccountRelationships({
        ...computedRequest.relationships,
        requested: false
      })
    })
    setLoading(false)
    return { result }
  }
  const rejectFollowRequest = async () => {
    setLoading(true)
    const result = await rejectFollow(account.id)

    setRequest({
      ...computedRequest,
      accepted: false,
      rejected: true,
      relationships: new AccountRelationships({
        ...computedRequest.relationships,
        requested: false
      })
    })
    setLoading(false)
    return { result }
  }
  return <div className={className}>
    {!computedRequest.rejected && <ActionButton
      id={computedRequest.id}
      relationship={computedRequest.relationships ? computedRequest.relationships.requested : true}
      toggleButtonState={acceptFollowRequest}
      onStatusChange={onStatusChange}
      disabled={loading || computedRequest.accepted}
      intlMessages={{
        stateOff: {
          id: 'profileCard.accepted',
          defaultMessage: 'Accepted'
        },
        stateOn: {
          hovered: {
            id: 'profileCard.accept',
            defaultMessage: 'Accept'
          },
          unhovered: {
            id: 'profileCard.accept',
            defaultMessage: 'Accept'
          }
        }
      }}
    />}
    {!computedRequest.accepted && <ActionButton
      id={computedRequest.id}
      relationship={computedRequest.relationships ? computedRequest.relationships.requested : true}
      toggleButtonState={rejectFollowRequest}
      onStatusChange={onStatusChange}
      className='ml-4'
      disabled={loading || computedRequest.rejected}
      intlMessages={{
        stateOff: {
          id: 'profileCard.rejected',
          defaultMessage: 'Rejected'
        },
        stateOn: {
          hovered: {
            id: 'profileCard.reject',
            defaultMessage: 'Reject'
          },
          unhovered: {
            id: 'profileCard.reject',
            defaultMessage: 'Reject'
          }
        }
      }}
    />}
  </div>
})

export {
  FollowButton,
  MuteButton,
  BlockButton,
  FollowRequestButtons,
}
