import React, { useState, SyntheticEvent } from 'react'
import { FormattedMessage } from 'react-intl'

export type ActionButtonType = {
  id: string,
  relationship: boolean,
  toggleButtonState?: Function,
  className?: string,
  onStatusChange?: Function,
  disabled?: boolean,
  intlMessages: {
    stateOn: {
      hovered: {
        id: string,
        defaultMessage: string
      },
      unhovered: {
        id: string,
        defaultMessage: string
      }
    },
    stateOff: {
      id: string,
      defaultMessage: string
    }
  }
}
export const ActionButton = ({ id, relationship, toggleButtonState, className, onStatusChange, intlMessages, disabled }: ActionButtonType) => {
  const hoverState = useState(false)
  const actionButtonText = relationship
    ? {
      id: hoverState[0] ? intlMessages.stateOn.hovered.id : intlMessages.stateOn.unhovered.id,
      defaultMessage: hoverState[0] ? intlMessages.stateOn.hovered.defaultMessage : intlMessages.stateOn.unhovered.defaultMessage,
      className: 'button-primary'
    }
    : { id: intlMessages.stateOff.id, defaultMessage: intlMessages.stateOff.defaultMessage, className: 'button-outline overlay' }
  const onActionButtonClick = async(e: SyntheticEvent) => {
    e.preventDefault()
    e.stopPropagation()
    try {
      const { result } = toggleButtonState && await toggleButtonState(id, relationship)

      if (result.state === 'ok') {
        onStatusChange && onStatusChange(result.data)
      }
    } catch (e) {
      // tslint:disable-next-line
      console.warn(e.message)
    }
  }
  return (
    <button
      style={{ 'maxWidth': '120px' }}
      className={`button p-2 rounded focus:outline-none icon-light leading-relaxed ${actionButtonText.className} ${className}`}
      onMouseEnter={() => !disabled && hoverState[1](true)}
      onMouseLeave={() => !disabled && hoverState[1](false)}
      onClick={onActionButtonClick}
      disabled={disabled}
    >
      <FormattedMessage
        id={actionButtonText.id}
        defaultMessage={actionButtonText.defaultMessage}
      />
    </button>
  )
}
