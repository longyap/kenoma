import { Database } from 'emoji-picker-element';
import { CustomEmoji } from 'emoji-picker-element/shared';

export class EmojiDB {
  database: any
  emojis: CustomEmoji[] = []

  constructor({ emojis }: { emojis: CustomEmoji[] }) {
    this.database = new Database({
      customEmoji: emojis
    })
  }

  search = async (q: string) => ({
    res: q.length
      ? await this.database.getEmojiBySearchQuery(q)
      : this.database._custom.all.slice(0, 10),
    q
  })
}
