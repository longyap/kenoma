import React, { Component } from 'react';
import { connect } from 'react-redux';
import { APP_ID, AUTOCOMPLETE_TYPES } from '../../constants';
import { CustomEmoji, EmojiClickEvent } from 'emoji-picker-element/shared';
import { Picker } from 'emoji-picker-element';

interface EmojiPanelType {
  onSelect?: Function
  emojis: CustomEmoji[]
  theme: string,
  noCustomEmojis?: boolean
}

interface EmojiPanelState {
  emojiPicker: Picker | null
}

class EmojiPanelComponent extends Component<EmojiPanelType, EmojiPanelState> {
  constructor(props: EmojiPanelType) {
    super(props)
    this.state = {
      emojiPicker: null
    }
  }
  container: React.RefObject<any> = React.createRef()

  componentDidMount () {
    const { emojis, theme } = this.props

    this.setState({
      emojiPicker: new Picker()
    }, () => {
      if (!this.props.noCustomEmojis) {
        (this.state.emojiPicker as any).customEmoji = emojis
      }
      this.container.current.appendChild(this.state.emojiPicker)
      if (this.props.theme) {
        (this.state.emojiPicker as any).classList.add(theme.split('-')[1])
      }
      (this.state.emojiPicker as any).addEventListener('emoji-click', this.onSelect)
    })
  }

  componentWillUnmount () {
    (this.state.emojiPicker as any).removeEventListener('emoji-click', this.onSelect)
    this.container.current.removeChild(this.state.emojiPicker)
  }

  onSelect = (item: EmojiClickEvent) => this.props.onSelect && this.props.onSelect({ res: item.detail.emoji, type: AUTOCOMPLETE_TYPES.emojis.name })

  render () {
    return (<div
      className='emoji-panel'
      ref={this.container}
    />)
  }
}

export const mapEmojiStateToProps = ({ config }: { config: any }) => ({
  emojis: config.emojis || [],
  theme: config[APP_ID].theme
})
export const EmojiPanel = connect(mapEmojiStateToProps, null, null, { forwardRef: true })(EmojiPanelComponent)
