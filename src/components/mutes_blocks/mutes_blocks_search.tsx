import React, { SyntheticEvent } from 'react'
import { SEARCH_DEBOUNCE_MS } from '../../constants'
import { debounce } from 'lodash'
import { getUserSuggestions, getDomainSuggestions } from '../../utils/suggestor'
import { connect } from 'react-redux'
import Pleroma from 'pleroma-api'
import Account from '../../entities/account'
import { IonSearchbar } from '@ionic/react'
import { WrappedComponentProps, injectIntl } from 'react-intl'
import { ProfileCardSm } from '../profile_card/profile_card_sm'
import { DomainCard } from '../domain_card/domain_card'

interface MutesBlocksSearchInterface extends WrappedComponentProps{
  tab: 'mute' | 'block',
  type?: 'users' | 'domains'
  users: any,
  searchUsers: Function,
  onSuggestionsUpdate?: Function,
  config?: any,
  domainBlocks: String[]
}
interface MutesBlocksSearchState {
  search: string,
  autocompleteRequest: string,
  suggestions: Account[] | String[],
  searchRequestsCounter: number,
  searchResultsMenuOpen: boolean,
  remoteInstances: String[]
}
class MutesBlocksSearchComponent extends React.Component<MutesBlocksSearchInterface, MutesBlocksSearchState>{
  constructor(props: MutesBlocksSearchInterface) {
    super(props)
    this.state = {
      search: '',
      autocompleteRequest: '',
      suggestions: [],
      searchRequestsCounter: 0,
      searchResultsMenuOpen: false,
      remoteInstances: []
    }
  }
  inputRef: React.RefObject<any> = React.createRef();

  componentDidMount () {
    this.loadRemoteInstances()
  }
  componentDidUpdate (prevProps: MutesBlocksSearchInterface) {
    if (prevProps.type !== this.props.type || this.props.tab !== prevProps.tab) {
      this.setState({
        searchRequestsCounter: 0,
        suggestions: [],
        search: ''
      })
    }
  }

  loadRemoteInstances = async () => {
    const { config } = this.props
    const result = await Pleroma.api.configs.getRemoteInstances({ config })
    if (result.state === 'ok') {
      this.setState({ remoteInstances: result.data })
    }
  }

  onTextFieldChange = (e: any) => {
    const newValue = e.target.value

    this.setState({
      search: newValue,
      autocompleteRequest: newValue,
      searchResultsMenuOpen: true
    })
    if (!!newValue.length) {
      const { type } = this.props

      type === 'users' && this.debouncedUsersSearch()
      type === 'domains' && this.debouncedDomainsSearch()
    }
  }

  searchUsers = async () => {
    const { autocompleteRequest } = this.state
    const { searchUsers } = this.props

    this.setState(prevState => ({
      suggestions: getUserSuggestions(autocompleteRequest, this.props.users),
      searchRequestsCounter: prevState.searchRequestsCounter + 1
    }))
    const { q } = searchUsers && await searchUsers(autocompleteRequest)

    this.setState(prevState => ({ searchRequestsCounter: prevState.searchRequestsCounter - 1 }))
    if (q === this.state.autocompleteRequest) {
      const suggestions = getUserSuggestions(this.state.autocompleteRequest, this.props.users)
      this.setState({ suggestions })
    }
  }

  searchDomains = async () => {
    const suggestions = getDomainSuggestions(this.state.autocompleteRequest, this.state.remoteInstances)
    this.setState({ suggestions })
  }

  debouncedUsersSearch = debounce(this.searchUsers, SEARCH_DEBOUNCE_MS)
  debouncedDomainsSearch = debounce(this.searchDomains, SEARCH_DEBOUNCE_MS)

  hideRecentSearches = (e: any) => {
    const path = e.path || (e.composedPath && e.composedPath());

    if (path) {
      if (!path.includes(document.getElementById('search'))) {
        (document.getElementById('root') as any).removeEventListener('click', this.hideRecentSearches)
        this.setState({ searchResultsMenuOpen: false })
      }
    }
  }

  openRecentSearches = async (e: SyntheticEvent) => {
    // HACK to hide recent searches
    (document.getElementById('root') as any).addEventListener('click', this.hideRecentSearches)
    this.setState({ searchResultsMenuOpen: true })
  }

  renderSearchResults = () => (<div className='fixed rounded foreground mutes-blocks-search overflow-y-auto ml-2'>
    { this.state.suggestions
      // @ts-ignore
      .map((item: Account | String, idx: number) => (typeof item === 'string'
      ? <DomainCard
        key={idx}
        domain={item}
        relationship={this.props.domainBlocks.includes(item)}
        onActionButtonClick={this.props.onSuggestionsUpdate}
      />
      : <ProfileCardSm
        key={idx}
        account={item as Account}
        highlighted={false}
        actions={[this.props.tab]}
        onActionButtonClick={this.props.onSuggestionsUpdate}
      />))}
  </div>)

  render() {
    const { intl } = this.props
    const { search } = this.state
    const searchInputPlaceholder = intl.formatMessage({ id: 'search.placeholder' })

    return (
      <div className='relative w-full' id='search'>
        <IonSearchbar
          onIonChange={this.onTextFieldChange}
          onFocus={this.openRecentSearches}
          className='pa-0 w-full z-10'
          debounce={0}
          placeholder={searchInputPlaceholder}
          value={search}
          searchIcon={''}
          ref={this.inputRef}
        />
        {this.state.searchResultsMenuOpen && this.renderSearchResults()}
      </div>
    )
  }
}

const mapStateToProps = ({ users, config }: { users: any, config: any }) => ({
  users: Object.values(users.usersByIds),
  config
})
const mapDispatchToProps = (dispatch: Function) => ({
  searchUsers: (q: string) => dispatch(Pleroma.thunks.api.search({ queries: { q, with_relationships: true } }))
})
export const MutesBlocksSearch = connect(mapStateToProps, mapDispatchToProps)(injectIntl(MutesBlocksSearchComponent))
