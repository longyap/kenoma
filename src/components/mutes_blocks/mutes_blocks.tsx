import React, { Component } from 'react'
import { IonContent, IonPage, IonButtons, IonButton } from '@ionic/react'
import Icons from '../common/icons/icons'
import { withRouter } from 'react-router'
import Pleroma from 'pleroma-api'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux'
import Account from '../../entities/account'
import { ProfileCardSm } from '../profile_card/profile_card_sm'
import { uiActions } from '../../reducers/ui'
import AccountRelationships from '../../entities/accountRelationships'
import { DomainCard } from '../domain_card/domain_card'
import { MutesBlocksSearch } from './mutes_blocks_search'

const BUTTON_CLASS = 'flex-grow cursor-pointer capitalize font-normal'

interface MutesBlocksInterface {
  className?: string,
  config?: any,
  showError: Function
}
type MutesBlocksState = {
  isLoading: boolean,
  tab: 'mute' | 'block',
  mutes: Account[],
  blocks: Account[],
  domainBlocks: String[],
  showBlockedDomains: boolean
}

export const MutesBlocksPage = withRouter(({ history }) => (
  <IonPage className='page-margin'>
    <div className='cursor-pointer ml-6 md:ml-0' onClick={() => history.goBack()}>
      <Icons.ChevronRight rotateDeg={180} className='header text-highlighted w-8 h-8' />
    </div>
    <IonContent>
      <MutesBlocks className='p-4'/>
    </IonContent>
  </IonPage>
))

export class MutesBlocksComponent extends Component<MutesBlocksInterface, MutesBlocksState> {
  constructor(props: MutesBlocksInterface) {
    super(props)
    this.state = {
      isLoading: true,
      tab: 'mute',
      mutes: [],
      blocks: [],
      domainBlocks: [],
      showBlockedDomains: false,
    }
  }
  async componentDidMount() {
    this.setState({ isLoading: true })
    try {
      this.loadMutes()
      this.loadBlocks()
    } catch (e) {
      this.props.showError(e.message || e)
    } finally {
      this.setState({ isLoading: false })
    }
  }

  loadMutes = async () => {
    const { config } = this.props
    const { state, data } = await Pleroma.api.mutes.list({ config })

    this.setState({
      mutes: state === 'ok' ? data.map((account: Account) => {
        account.relationships = new AccountRelationships({ muting: true })
        return Pleroma.emojifyUtils.emojifyAccount(account)
      }) : []
    })
  }

  loadBlocks = async () => {
    const { config } = this.props

    const result = await Promise.all([
      Pleroma.api.blocks.list({ config }),
      Pleroma.api.blocks.listDomainBlocks({ config })
    ])
    this.setState({
      blocks: result[0].state === 'ok' ? result[0].data.map((account: Account) => {
        account.relationships = new AccountRelationships({ blocking: true })
        return Pleroma.emojifyUtils.emojifyAccount(account)
      }) : [],
      domainBlocks: result[1].state === 'ok' ? result[1].data : []
    })
  }

  onSuggestionsUpdate = () => {
    if (this.state.tab === 'mute') {
      this.loadMutes()
    } else {
      this.loadBlocks()
    }
  }

  render() {
    const { className } = this.props
    const { tab, mutes, blocks, domainBlocks, showBlockedDomains } = this.state

    return (
    <div className={`w-full ${className}`}>
      <div className='flex flex-col items-center'>
        <IonButtons className='flex mb-4'>
          <IonButton
            className={`${tab === 'mute' && 'text border-dark'} ${BUTTON_CLASS} text-secondary border-b`}
            onClick={() => this.setState({ tab: 'mute' })}
          >
            <FormattedMessage id='settings.mutesAndBlocks.mutes' defaultMessage='Mutes' />
          </IonButton>
          <IonButton
            className={`${tab === 'block' && 'text border-dark'} ${BUTTON_CLASS} text-secondary border-b`}
            onClick={() => this.setState({ tab: 'block' })}
          >
            <FormattedMessage id='settings.mutesAndBlocks.blocks' defaultMessage='Blocks' />
          </IonButton>
        </IonButtons>
        { tab === 'block' && <IonButtons className='flex self-start items-center text pl-2'>
          <IonButton
            className={BUTTON_CLASS}
            style={{'fontWeight': showBlockedDomains ? 500 : 700}}
            onClick={() => this.setState({ showBlockedDomains: false })}
          >
            <FormattedMessage id='settings.mutesAndBlocks.users' defaultMessage='Users' />
          </IonButton>
          <IonButton
            className={BUTTON_CLASS}
            style={{'fontWeight': showBlockedDomains ? 700 : 500}}
            onClick={() => this.setState({ showBlockedDomains: true })}
          >
            <FormattedMessage id='settings.mutesAndBlocks.domains' defaultMessage='Domains' />
          </IonButton>
        </IonButtons>}
        <MutesBlocksSearch
          tab={tab}
          onSuggestionsUpdate={this.onSuggestionsUpdate}
          type={(tab === 'block' && showBlockedDomains) ? 'domains' : 'users'}
          domainBlocks={domainBlocks}
        />
        <div className='w-full mb-8'>
          { tab === 'block'
            ? showBlockedDomains
              ? domainBlocks.map((item: any, idx: number) => <DomainCard key={idx} domain={item} />)
              : blocks.map((item: any) => <ProfileCardSm account={item} key={item.id} highlighted={false} actions={[tab]} />)
            : mutes.map((item: any) => <ProfileCardSm account={item} key={item.acct} highlighted={false} actions={[tab]} />)
          }
        </div>
      </div>
    </div>
  )}
}

const mapStateToProps = ({ config }: { config: any }) => ({
  config
})
const mapDispatchToProps = (dispatch: Function) => ({
  showError: (errorMessage: string) => dispatch(uiActions.openAlert({
    type: 'danger',
    message: errorMessage
  }))
})
export const MutesBlocks = connect(mapStateToProps, mapDispatchToProps)(MutesBlocksComponent)
