import React, { useState } from 'react'
import dayjs from 'dayjs'
import { connect } from 'react-redux'
import { Link, withRouter, RouteComponentProps } from 'react-router-dom'
import relativeTime from 'dayjs/plugin/relativeTime'
import { StatusActions } from '../status_actions/status_actions'
import { Attachments } from '../attachments/attachments'
import Icons from '../common/icons/icons'
import { mentionMatchesUrl, isTag, extractTagFromUrl } from '../../utils/matchers'
import { generateProfileLink, generateTagLink } from '../../utils/linkGenerators'
import { getStatusMentionsList, containMutedWords } from '../../utils/statusUtils'
import { Poll } from '../poll/poll'
import StatusInterface from '../../entities/status'
import { EmojiReactions } from '../emoji_reactions/emoji_reactions'
import { APP_ID } from '../../constants'

dayjs.extend(relativeTime)

interface StatusComponentType extends RouteComponentProps {
  status?: StatusInterface,
  userId?: string,
  locale?: string,
  toggleThreadFetcher?: Function,
  instance: string,
  history: any,
  muteWords: string[]
}
const StatusComponent = (props: StatusComponentType) => {
  const status = new StatusInterface(props.status)
  const [showMutedWords, toggleShowMutedWords] = useState(true)

  let actualStatus = status
  if (status.reblog) {
    actualStatus = {
      ...status.reblog,
      reblogged_by: status.reblogged_by,
      favourited_by: status.favourited_by
    }
  }

  const pushToThread = (event: any) => {
    const target = event.target.closest('.status-content a')
    event.stopPropagation()
    event.preventDefault()
    if (target) {
      if (target.className.match(/mention/)) {
        const mention = getStatusMentionsList(status).find(mention => mentionMatchesUrl(mention, target.href, props.instance))

        if (mention) {
          props.history.push(generateProfileLink(mention.id, mention.acct))
        }
      } else if (isTag(target)) {
        const tag = extractTagFromUrl(target.href)

        if (tag) {
          // @ts-ignore
          props.history.push(generateTagLink(tag))
        }
      } else {
        window.open(target.href, '_blank')
      }
    } else {
      props.history.push(`/notice/${actualStatus.id}`)
    }
  }
  const includedWords = containMutedWords(status, props.muteWords)
  if (!includedWords.length && showMutedWords) {
    toggleShowMutedWords(false)
  }

  return includedWords.length
    ? null
    : (<div className='mb-12'>
      <div className='flex mb-4'>
        <Link to={`/users/${actualStatus.account.acct}`}>
          <img alt='avatar' className='mr-5 w-12 h-12 rounded-lg' src={actualStatus.account.avatar} style={{minWidth: '3rem'}} />
        </Link>
        <div className='flex justify-between items-start md:items-baseline w-full overflow-hidden'>
          <div className='flex-grow flex-col flex overflow-hidden'>
            <span
              className='font-bold text leading-none md:leading-normal truncate'
              dangerouslySetInnerHTML={{ __html: actualStatus.account.display_name }}
            />
            <div className='flex items-center flex-wrap font-light text-sm leading-none text-secondary truncate'>
              {actualStatus.account.acct}
            </div>
          </div>
          <div className='flex flex-shrink-0 items-center md:pt-0'>
            { (actualStatus.muted || !!includedWords.length) && <span
              className='mr-2 flex items-center cursor-pointer'
              onClick={() => { toggleShowMutedWords(!showMutedWords) }}
            >
              <Icons.VolumeOff classNameName='w-4 h-4 text-grey' />
            </span> }
            <Link to={`/notice/${actualStatus.id}`} className='block text-xs text-right leading-noneflex items-center'>
              <time className='max-w-time md:max-w-xs text'>{dayjs(actualStatus.created_at).locale(props.locale || 'en').fromNow()}</time>
            </Link>
          </div>
        </div>
      </div>

      <div className='rounded-lg foreground pb-3 break-all'>
        { (status.reblog && !showMutedWords) && <span className='font-bold text-sm text-secondary leading-none flex items-center border-b px-4 py-2'>
          <Icons.Repeat classNameName={'h-3 w-3 icon inline-block align-text-bottom mr-1'} />
          <Link to={`/users/${status.account.acct}`}>{status.account.acct}</Link>
        </span>}
        {showMutedWords
          ? <div className='px-4 pt-3 md:pt-4 text-xs text'>{includedWords.join(', ')}</div>
          : <div className='px-4 pt-3 md:pt-4 '>
            <div className='block leading-normal leading-tight mb-3 cursor-pointer status-content' onClick={pushToThread}>
              {actualStatus.spoiler_text && <div className='font-bold text-sm md:text-base text' dangerouslySetInnerHTML={{ __html: actualStatus.spoiler_text }} />}
              <div className='text-sm text' dangerouslySetInnerHTML={{ __html: actualStatus.content }} />
            </div>
            {actualStatus.media_attachments.length > 0 && <Attachments attachments={actualStatus.media_attachments} />}
            {actualStatus.poll && <Poll poll={actualStatus.poll} cssClass='px-0' statusId={actualStatus.id} />}
            {actualStatus.pleroma && <EmojiReactions
              emojiReactions={actualStatus.pleroma.emoji_reactions}
              statusId={status.id}
              reblogStatusId={actualStatus.id}
              userId={props.userId}
            /> }
            <StatusActions status={actualStatus} statusId={status.id} userId={props.userId} toggleThreadFetcher={props.toggleThreadFetcher} />
          </div>}
      </div>
    </div>
  )
}

const mapStateToProps = ({ api: { config }, intl: { locale } }
  : { api: { config: any }, intl: { locale: string } }) => ({
  instance: config.instance,
  locale,
  muteWords: (config && config[APP_ID]) ? config[APP_ID].muteWords : [],
  hideAttachments: (config && config[APP_ID]) ? config[APP_ID].hideAttachments : false
})

export const Status = connect(mapStateToProps)(withRouter(StatusComponent))
