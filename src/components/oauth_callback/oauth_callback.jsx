import { useHistory } from 'react-router-dom'
import { connect } from 'react-redux'
import { oauthThunks } from '../../thunks/oauth'

const getCodeFromSearchParameters = (search) => {
  // TODO: use polyfill maybe, URLSearchParams is not supported in IE
  const params = new URLSearchParams(search)
  return params.get('code')
}

const mapDispatchToProps = (dispatch) => ({
  setCode: search => {
    const code = getCodeFromSearchParameters(search)

    return dispatch(oauthThunks.setCodeAndGetToken({ code }))
  }
})

// Does nothing but dispatches action and redirects, done this way to use
// the react-router, could maybe be done some other way that doesn't require
// a dummy component.

const OauthCallbackComponent = ({ setCode, location }) => {
  let history = useHistory()
  setCode(location.search)
    .then(() => history.push('/'))
  return null
}

export const OauthCallback = connect(null, mapDispatchToProps)(OauthCallbackComponent)

