import React, { Component } from 'react'
import { connect } from 'react-redux'
import { IonPage, withIonLifeCycle, IonContent } from '@ionic/react'
import Pleroma from 'pleroma-api'
import StatusInterface from '../../entities/status'
import Icons from '../common/icons/icons'
import { withRouter, RouteComponentProps } from 'react-router'
import debounce from 'lodash/debounce'
import { Status } from '../status/status'

interface TagPage extends RouteComponentProps {
  statuses?: StatusInterface[]
  startFetchingTagTimeline?: Function,
  stopFetchingTagTimeline?: Function,
  tag?: string,
  loadingOlder?: boolean,
  loadOlder?: Function
}

class TagPageComponent extends Component<TagPage, { isLoading: boolean }> {
  constructor(props: TagPage) {
    super(props)
    this.state = {
      isLoading: false
    }
  }
  private scrollElement = React.createRef<HTMLDivElement>()
  async ionViewDidEnter() {
    this.setState({ isLoading: true })
    this.props.startFetchingTagTimeline && await this.props.startFetchingTagTimeline()
    this.setState({ isLoading: false })
  }
  ionViewWillLeave() {
    this.props.stopFetchingTagTimeline && this.props.stopFetchingTagTimeline()
  }
  async componentDidUpdate(prevProps: TagPage) {
    if (!prevProps.statuses && this.props.statuses) {
      this.setState({ isLoading: false })
    }
    if (prevProps.tag !== this.props.tag) {
      this.setState({ isLoading: true })
      this.props.stopFetchingTagTimeline && await this.props.stopFetchingTagTimeline()
      this.props.startFetchingTagTimeline && await this.props.startFetchingTagTimeline()
      this.setState({ isLoading: false })
    }
  }
  tryLoadMore = (loading?: boolean, fn?: Function) => () => {
    const { current } = this.scrollElement

    if (loading) return
    if (current && current.scrollHeight - current.scrollTop - current.offsetHeight <= 500) {
      fn && fn()
    }
  }
  render() {
    const { statuses, history, tag, loadingOlder, loadOlder } = this.props
    const { isLoading } = this.state

    return (
      <IonPage className='page-margin'>
        <div className='flex items-center'>
          <div className='cursor-pointer ml-6 md:ml-0' onClick={() => history.goBack()}>
            <Icons.ChevronRight rotateDeg={180} className='header text-highlighted w-8 h-8' />
          </div>
          <span className='page-title px-6'>{tag}</span>
        </div>
        <IonContent>
          <div
            className='overflow-y-auto py-8 px-6 md:pl-0 md:pr-4 max-h-full'
            ref={this.scrollElement}
            onScroll={debounce(this.tryLoadMore(loadingOlder, loadOlder), 100)}
          >
            { statuses && statuses.map(status =>
              // @ts-ignore
              (<Status status={status} key={status.id} />)
            ) }
            { (loadingOlder || isLoading) && <div className='w-6 h-6 mx-auto my-4 text-secondary'>
              <Icons.Cog className='w-10 h-10 a-loader-spin' />
            </div>}
          </div>
        </IonContent>
      </IonPage>
    )
  }
}

const mapStateToProps = ({ statuses: { tag, statusesByIds }, api }: any) => ({
  statuses: tag.map((id: string) => statusesByIds[id]),
  loadingOlder: api.tagTimeline.loadingOlder
})
const mapDispatchToProps = (dispatch: Function) => ({ dispatch })
const mergeProps = (stateProps: any, { dispatch }: { dispatch: Function }, ownProps: any) => {
  const tag = ownProps.match.params.tag

  return {
    ...stateProps,
    tag,
    startFetchingTagTimeline: () => dispatch(Pleroma.thunks.api.startFetchingTagTimeline({ params: { tag }})),
    stopFetchingTagTimeline: () => dispatch(Pleroma.thunks.api.stopFetchingTagTimeline()),
    loadOlder: () => dispatch(Pleroma.thunks.api.loadOlderTagTimeline({ params: { tag } }))
  }
}
export const TagPage = connect(mapStateToProps, mapDispatchToProps, mergeProps)(withRouter(withIonLifeCycle(TagPageComponent)))
