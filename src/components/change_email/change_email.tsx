import React, { Component, SyntheticEvent } from 'react'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { IonLabel } from '@ionic/react'
import { Input } from '../common/input/input'
import { validate } from '../../utils/validators'
import { generateErrorMessage } from '../input/input'
import { oauthThunks } from '../../thunks/oauth'
import { Error, Success } from '../common/alerts/alerts'
import { RouteComponentProps, withRouter } from 'react-router'

interface ChangeEmailInterface extends RouteComponentProps {
  changeEmail: Function,
}
type ProfileSettingsState = {
  isLoading: boolean,
  formData: { [key: string]: any },
  errors: any,
  serverError: string,
  success: boolean
}

const defaultState = {
  isLoading: false,
  formData: {
    email: '',
    password: ''
  },
  errors: {},
  serverError: '',
  success: false
}
class ChangeEmailComponent extends Component<ChangeEmailInterface, ProfileSettingsState> {
  constructor(props: ChangeEmailInterface) {
    super(props)
    this.state = { ...defaultState }
  }

  componentDidUpdate(prevProps: ChangeEmailInterface) {
    if (this.props.location !== prevProps.location) {
      this.setState({ ...defaultState })
    }
  }
  validator = {
    email: ['required'],
    password: ['required']
  }

  onInputChange = (e: any, field: string) => {
    this.setState({
      formData: { ...this.state.formData, [field]: e.target.value },
    })
  }

  validate = () => {
    const validationRes = validate(this.state.formData, this.validator)
    if (typeof validationRes === 'object') {
      this.setState({ errors: validationRes })
      return false
    } else {
      this.setState({ errors: {} })
      return true
    }
  }

  changeEmail = async (e: SyntheticEvent) => {
    this.setState({ isLoading: true, success: false, serverError: '' })
    e.preventDefault()
    if (this.validate()) {
      try {
        const { changeEmail } = this.props

        changeEmail && await changeEmail(this.state.formData)
        this.setState({
          success: true,
          formData: {
            email: '',
            password: ''
          }
        })
      } catch (e) {
        this.setState({ isLoading: false, serverError: e })
      }
    }
    this.setState({ isLoading: false })
  }

  render() {
    const { formData, isLoading, errors, serverError, success } = this.state

    return (
      <form
        onSubmit={this.changeEmail}
        action='changeEmail'
        className='w-full'
      >
        <div className='font-bold text mt-4'>
          <FormattedMessage
            defaultMessage='Change email'
            id='settings.editProfile.changeEmail'
          />
        </div>
        { success && <Success>
          <FormattedMessage id='settings.editProfile.emailWasChanged' defaultMessage='Your email was successfully changed'/>
        </Success>
        }
        {serverError && <Error error={serverError} />}
        <Input
          label={<IonLabel position='floating'>
            <FormattedMessage id='settings.editProfile.newEmail' defaultMessage='New Email' />
            </IonLabel>}
          value={formData.email}
          onChange={(e: any) => this.onInputChange(e, 'email')}
          error={errors.email}
          type='email'
          errorMessage={generateErrorMessage([...errors.email || []])}
        />
        <Input
          label={<IonLabel position='floating'>
            <FormattedMessage id='settings.editProfile.password' defaultMessage='Password' />
            </IonLabel>}
          value={formData.password}
          onChange={(e: any) => this.onInputChange(e, 'password')}
          type='password'
          error={errors.password}
          errorMessage={generateErrorMessage([...errors.password || []])}
        />
        <button
          disabled={isLoading}
          className='button-primary rounded-lg py-2 px-4 mt-4 font-bold text-sm focus:outline-none leading-tight'>
          <FormattedMessage defaultMessage='Save' id='settings.save' />
        </button>
      </form>
  )}
}

const mapDispatchToProps = (dispatch: Function) => ({
  changeEmail: (params: any) => dispatch(oauthThunks.changeEmail(params))
})
export const ChangeEmail = connect(null, mapDispatchToProps)(withRouter(ChangeEmailComponent))
