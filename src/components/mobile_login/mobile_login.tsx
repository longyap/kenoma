import React, { Component, SyntheticEvent } from 'react'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'
import { Link, RouteComponentProps } from 'react-router-dom'
import { oauthThunks } from '../../thunks/oauth'
import { FormattedMessage } from 'react-intl'
import { Error } from '../common/alerts/alerts'
import { Input, generateErrorMessage } from '../input/input'
import { validate } from '../../utils/validators'
import Icons from '../common/icons/icons'
import { InstanceSwitcher } from '../instance_selection/instance_switcher'
import { ForgotPasswordLink } from '../forgot_password/forgot_password_link'

interface MobileLoginType extends RouteComponentProps {
  startLogin: Function,
  isMobile: boolean,
  error: string,
  instance: string | null
}
type MobileLoginStateType = {
  username: string,
  password: string,
  loading: boolean,
  errors: any
}
class MobileLoginComponent extends Component<MobileLoginType, MobileLoginStateType> {
  constructor (props: MobileLoginType) {
    super(props)
    this.state = {
      username: '',
      password: '',
      loading: false,
      errors: {}
    }
  }
  validator = {
    username: ['required'],
    password: ['required']
  }
  onSubmit = async (e: SyntheticEvent) => {
    e.preventDefault()
    this.setState({ loading: true })
    const validationRes = validate(this.state, this.validator)
    if (typeof validationRes === 'object') {
      this.setState({ errors: validationRes })
    } else {
      const { username, password } = this.state
      const { startLogin, history } = this.props

      startLogin && await startLogin({ username, password })
      history.push('/')
    }
    this.setState({ loading: false })
  }
  componentDidUpdate(prevProps: MobileLoginType) {
    if (prevProps.isMobile !== this.props.isMobile && !this.props.isMobile) {
      this.props.history.push('/')
    }
  }
  render () {
    const { error, instance } = this.props
    const { username, password, loading, errors } = this.state

    return instance ? (
      <form className='px-8 pt-6 pb-8 mt-10 flex items-center flex-col' onSubmit={this.onSubmit}>
        <InstanceSwitcher />
        <div className='mb-4 w-full'>
          <Input
            value={username}
            className='mb-4'
            placeholder='username'
            onChange={(e: any) => this.setState({ username: e.target.value })}
            error={errors.username}
            errorMessage={generateErrorMessage([...errors.username || []])}
          />
          <Input
            value={password}
            className='mb-4'
            placeholder='password'
            type='password'
            onChange={(e: any) => this.setState({ password: e.target.value })}
            error={errors.password}
            errorMessage={generateErrorMessage([...errors.password || []])}
          />
        </div>
        {error && typeof(error) === 'string' && <Error error={error} />}
        <ForgotPasswordLink />
        <button className='login-btn'>
          {loading
            ? <Icons.Cog className='w-4 h-4 a-loader-spin' />
            : <FormattedMessage defaultMessage='Login' id='auth.login' />
          }
        </button>
        <Link to='/sign-up' className='logo-login cursor-pointer'>Sign up</Link>
      </form>
    )
    : (<Redirect to='/' />)
  }
}

const mapDispatchToProps = (dispatch: Function) => ({
  dispatch
})

const mapStateToProps = ({ oauth: { error }, api: { config: { instance }}}: { oauth: { error: string }, api: { config: { instance: string } } }) => ({
  error,
  instance: instance.indexOf('undefined') !== -1 ? null : instance
})
const mergeProps = (stateProps: any, { dispatch }: { dispatch: Function }, ownProps: any) => ({
  ...stateProps,
  ...ownProps,
  startLogin: async ({ username, password }: { username: string, password: string }) => {
    await dispatch(oauthThunks.setInstance({ instance: stateProps.instance }))
    return dispatch(oauthThunks.loginWithUsername({ params: {
      client_name: `Kenoma_${Date.now()}`,
      instance: stateProps.instance,
      username,
      password
    } }))
  }
})

export const MobileLogin = connect(mapStateToProps, mapDispatchToProps, mergeProps)(withRouter(MobileLoginComponent))
