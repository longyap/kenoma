import React, { Component, SyntheticEvent } from 'react'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router'
import { Link } from 'react-router-dom'
import Pleroma from 'pleroma-api'
import { Input, generateErrorMessage } from '../input/input'
import { oauthThunks } from '../../thunks/oauth'
import { validate } from '../../utils/validators'
import { SignUpData, SignUpType, SignUpStateType } from './sign_up_types'
import { InstanceSwitcher } from '../instance_selection/instance_switcher'
import { Error } from '../common/alerts/alerts'

class SignUpComponent extends Component<SignUpType, SignUpStateType> {
  constructor(props: SignUpType) {
    super(props)
    this.state = {
      isLoading: false,
      errors: {},
      username: '',
      fullname: '',
      email: '',
      bio: '',
      password: '',
      confirm: '',
      captcha: null,
      captcha_solution: ''
    }
  }
  validator = {
    username: ['required'],
    fullname: ['required'],
    email: ['required'],
    password: ['required'],
    confirm: ['required', ['sameAs', 'password']],
    captcha_solution: ['required']
  }
  componentDidMount() {
    this.props.instance && this.setCaptcha()
  }
  componentDidUpdate(prevProps: SignUpType) {
    const { instance, clearError } = this.props

    if (!prevProps.instance && instance) {
      clearError && clearError()
      this.setCaptcha()
    }
  }
  setCaptcha = async () => {

    this.setState({ isLoading: true })
    const { config } = this.props

    const { data } = await Pleroma.api.configs.getCaptcha({ config })
    this.setState({ captcha: data, captcha_solution: '', isLoading: false })
  }

  signUp = async (e: SyntheticEvent) => {
    e.preventDefault()
    if (this.validate()) {
      const { signUp, history } = this.props
      try {
        signUp && await signUp(this.createSignUpApiObject(this.state))
        history.push('/')
      } catch (e) {
        e.captcha && this.setCaptcha()
      }
    }
  }
  validate = () => {
    const validationRes = validate(this.state, this.validator)
    if (typeof validationRes === 'object') {
      this.setState({ errors: validationRes })
      return false
    } else {
      return true
    }
  }
  createSignUpApiObject = (state: SignUpStateType): SignUpData => {
    const params = { ...state }
    delete params.isLoading
    const { token, answer_data } = params.captcha || {}
    delete params.captcha
    return {
      ...params,
      nickname: params.username,
      agreement: true,
      locale: 'en_US',
      captcha_token: token,
      captcha_answer_data: answer_data
    }
  }
  render() {
    const { username, fullname, email, bio, password, confirm, captcha, captcha_solution, isLoading } = this.state
    const { errors, instance } = this.props

    return instance ? (
      <form className='px-8 flex items-center flex-col overflow-y-auto max-h-full' onSubmit={this.signUp}>
        <InstanceSwitcher />
        <div className='mb-4 w-full'>
          <Input
            value={username}
            placeholder='username'
            onChange={(e: any) => this.setState({ username: e.target.value })}
            message='Eg. lain'
            className='mb-4'
            error={errors.nickname || this.state.errors.username}
            errorMessage={generateErrorMessage([...this.state.errors.username || [], ...errors.nickname || []])}
          />
          <Input
            value={fullname}
            onChange={(e: any) => this.setState({ fullname: e.target.value })}
            placeholder='Display name'
            message='Eg. Lain Iwakura'
            className='mb-4'
            error={errors.fullname || this.state.errors.fullname}
            errorMessage={generateErrorMessage([...this.state.errors.fullname || [], ...errors.fullname || []])}
          />
          <Input
            value={email}
            type='email'
            placeholder='Email'
            onChange={(e: any) => this.setState({ email: e.target.value })}
            className='mb-4'
            error={errors.email || this.state.errors.email}
            errorMessage={generateErrorMessage([...this.state.errors.email || [], ...errors.email || []])}
          />
          <Input
            value={password}
            type='password'
            placeholder='Password'
            onChange={(e: any) => this.setState({ password: e.target.value })}
            className='mb-4'
            error={errors.password || this.state.errors.password}
            errorMessage={generateErrorMessage([...this.state.errors.password || [], ...errors.password || []])}
          />
          <Input
            value={confirm}
            type='password'
            placeholder='Password confirm'
            onChange={(e: any) => this.setState({ confirm: e.target.value })}
            className='mb-4'
            error={errors.confirm || this.state.errors.confirm}
            errorMessage={generateErrorMessage([...this.state.errors.confirm || [], ...errors.confirm || []])}
          />
          <Input
            value={bio}
            placeholder='Bio (optional)'
            textarea={true}
            message='Eg. Hi, I am Lain. I am an anime girl living in suburban Japan. You may know me from Wired.'
            onChange={(e: any) => this.setState({ bio: e.target.value })}
            className='mb-4'
            error={errors.bio || this.state.errors.bio}
            errorMessage={generateErrorMessage([...this.state.errors.bio || [], ...errors.confirm || []])}
          />
          {captcha && <div>
            <img
              alt='click to reload'
              src={captcha.url}
              className='block cursor-pointer w-full'
              onClick={this.setCaptcha}
            />
            <Input
              value={captcha_solution}
              placeholder='Captcha'
              error={errors.captcha || this.state.errors.captcha_solution}
              errorMessage={generateErrorMessage([...this.state.errors.captcha_solution || [], ...errors.captcha || []])}
              onChange={(e: any) => this.setState({ captcha_solution: e.target.value })}
            />
          </div>}
        </div>
        {errors && errors.ap_id && <Error error={errors.ap_id} />}
        <button
          className='login-btn'
          disabled={isLoading}
        >
          <FormattedMessage defaultMessage='Sign up' id='auth.signUp' />
        </button>
        <Link to='/' className='logo-login py-2'>Log in</Link>
      </form>
      )
      : (<Redirect to='/' />)
  }
}

const mapStateToProps = ({ api, oauth }: { api: any, oauth: any }) => ({
  config: api.config,
  errors: oauth.error
})
const mapDispatchToProps = (dispatch: Function) => ({ dispatch })
const mergeProps = ({ config, errors }: { config: any, errors: any }, { dispatch }: { dispatch: Function }) => {
  const instance = config.instance.indexOf('undefined') !== -1 ? null : config.instance

  return {
    config,
    instance,
    errors: errors || {},
    signUp: async (account: SignUpData) => (
      dispatch(oauthThunks.signUp({
        ...account,
        nickname: account.username[0]
      }))
    ),
    clearInstance: async () => dispatch(oauthThunks.setInstance({ instance: 'https://undefined' }))
  }
}
export const SignUp = connect(mapStateToProps, mapDispatchToProps, mergeProps)(withRouter(SignUpComponent))
