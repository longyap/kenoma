

import { RouteComponentProps } from 'react-router'
export interface SignUpType extends RouteComponentProps{
  config: any,
  errors: any,
  instance: string,
  signUp: Function,
  clearInstance: Function,
  clearError: Function
}
export interface SignUpStateType {
  isLoading: boolean
  confirm: string
  errors: {[key: string]: string[]}
  captcha: {
    solution: string,
    token: string,
    url: string,
    answer_data?: string
  } | null,
  username: string,
  fullname: string,
  email: string,
  bio: string,
  password: string,
  captcha_solution: string
}
export interface SignUpData {
  nickname: string,
  username: string,
  fullname: string,
  bio: string,
  email: string,
  password: string,
  agreement: boolean,
  locale: string,
  captcha_solution: string,
  captcha_token?: string,
  captcha_answer_data?: string
}
