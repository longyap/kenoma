import React from 'react'
import { IonInput, IonTextarea } from '@ionic/react'

export const generateErrorMessage = (messages: string[]) => messages.reduce((acc: string, curr: string) => acc.concat(curr + ' '), '')

type Input = {
  value: string,
  onChange?: any,
  placeholder?: string,
  message?: string,
  error?: boolean,
  errorMessage?: string,
  textarea?: boolean,
  type?: 'number' | 'time' | 'text' | 'tel' | 'url' | 'email' | 'search' | 'date' | 'password',
  className?: string,
  outlined?: boolean
}
export const Input = ({ value, onChange, placeholder, message, error, errorMessage, type, textarea, className, outlined }: Input) => {
  const computedClass = ['border-b', 'input-secondary-border', 'text-secondary']
  computedClass.push(outlined ? 'border rounded' : 'border-b')
  error && computedClass.push('border-error')
  return (<div className={'input '.concat(className || '')}>
    { textarea
      ? <IonTextarea
        className={computedClass.join(' ')}
        value={value}
        placeholder={placeholder}
        onIonChange={onChange}
      />
      : <IonInput
        className={computedClass.join(' ')}
        value={value}
        type={type}
        placeholder={placeholder}
        onIonChange={onChange}
      />
    }
    <p className={`note text-xs ${error ? 'error' : ''}`}>{error ? errorMessage : message}</p>
  </div>
  )
}