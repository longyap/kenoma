import React, { Component } from 'react'
import Icons from '../common/icons/icons'
import { IonBadge } from '@ionic/react'
import { FormattedMessage } from 'react-intl'
import pleroma from 'pleroma-api'
import { connect } from 'react-redux'
import map from 'lodash/map'

class NotificationsHeader extends Component<{readAll: Function, unread_notifications_count: number, cssClass?: string}> {
  readAll = () => {
    const { readAll } = this.props

    readAll && readAll()
  }
  render() {
    const { unread_notifications_count, cssClass } = this.props

    return (
      <div className={`text flex items-center justify-between ${cssClass}`}>
        <span className='flex items-center'>
          <span className='relative mr-4 flex items-center'>
            <Icons.Bell className='w-8 h-8' />
            {!!unread_notifications_count &&
              <IonBadge style={{minWidth: '1.25rem', left: '1.25rem'}} className='rounded-full absolute top-0'>
                {unread_notifications_count}
              </IonBadge>}
          </span>
          <FormattedMessage defaultMessage='Notifications' id='notifications' />
        </span>
        <span onClick={this.readAll}><Icons.Accept className='w-8 h-8 align-middle' /></span>
      </div>
    )
  }
}

export const mapDispatchToProps = (dispatch: Function) => ({
  readAll: () => dispatch(pleroma.thunks.notifications.read({}))
})
export const mapStateToProps = ({ notifications }: { notifications: any }) => ({
  unread_notifications_count: map(notifications.notificationsByIds, ({ id }) => notifications.notificationsByIds[id]).filter(({ pleroma: { is_seen } }: { pleroma: { is_seen: boolean } }) => !is_seen).length
})
export default connect(mapStateToProps, mapDispatchToProps)(NotificationsHeader)
