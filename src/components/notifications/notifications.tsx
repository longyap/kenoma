import React from 'react'
import { connect } from 'react-redux'
import dayjs from 'dayjs'
import groupBy from 'lodash/groupBy'
import map from 'lodash/map'
import toPairs from 'lodash/toPairs'
import pleroma from 'pleroma-api'
import { Notification } from '../notification/notification'
import NotificationType from '../../entities/notification'

const NotificationsComponent = (
  { notifications, locale }: { notifications: NotificationType[], locale: string }
) => {
  const getGroupedByDayNotificationsList = () => {

    if (!notifications || !notifications.length) return []
    const reversedNotifications = notifications.reverse()
    const groupedNotificationsObject = groupBy(
      reversedNotifications.map(
        notification => ({
          ...notification,
          created_at_date: dayjs(notification.created_at).startOf('day').unix()
        })
      ),
      'created_at_date'
    )

    return toPairs(groupedNotificationsObject).reverse()
  }

  const groupedNotificationsList = getGroupedByDayNotificationsList();

  return (
    <div className='md:pt-0 md:pb-0 pb-10 overflow-y-auto px-6 md:pl-0'>
      {groupedNotificationsList.map(group => (
        <div key={group[0]}>
          <p className='note text-xs mb-3 block'>{dayjs(Number(group[0]) * 1000).locale(locale || 'en').format('dddd, DD MMM YYYY')}</p>
          <div className='rounded-lg foreground mb-5 pt-3'>
            {group[1].map(notification => (
              <Notification
                key={notification.id}
                notification={notification}
              />
            ))}
          </div>
        </div>
      ))}
    </div>
  )
}

const mapStateToProps = ({ intl: { locale } }: { intl: { locale: string } }) => ({ locale })
export const mapNotificationsStateToProps = ({ notifications }: { notifications: any }) => ({
  notifications: map(notifications.notificationsByIds, ({ id }) => notifications.notificationsByIds[id])
})
export const mapNotificationsDispatchToProps = (dispatch: Function) => ({
  resetUnreadNotificationsCount: () => dispatch(pleroma.reducers.users.actions.updateUnreadNotificationsCount({ updateUnreadNotificationsCount: 0 })),
  readAll: () => dispatch(pleroma.thunks.notifications.read({})),
})
export const Notifications = connect(mapStateToProps)(NotificationsComponent)
