import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withIonLifeCycle, IonPage, IonContent } from '@ionic/react'
import { Notifications, mapNotificationsDispatchToProps, mapNotificationsStateToProps } from './notifications'
import NotificationType from '../../entities/notification'
import NotificationsHeader from './notificationsHeader'

export type NotificationsBoxType = {
  resetUnreadNotificationsCount: Function,
  readAll: Function,
  notifications: NotificationType[]
}
class NotificationsPageComponent extends Component<NotificationsBoxType> {
  ionViewDidEnter() {
    const { resetUnreadNotificationsCount } = this.props

    resetUnreadNotificationsCount && resetUnreadNotificationsCount()
  }

  ionViewWillLeave() {
    const { readAll } = this.props

    readAll && readAll()
  }

  render() {
    return (
      <IonPage className='page-margin'>
        <NotificationsHeader cssClass='page-title px-6 mb-4'/>
        <IonContent>
          <Notifications notifications={this.props.notifications} />
        </IonContent>
      </IonPage>
    )
  }
}

const NotificationsIonicComponent = withIonLifeCycle(NotificationsPageComponent)
export const NotificationsPage = connect(mapNotificationsStateToProps, mapNotificationsDispatchToProps)(NotificationsIonicComponent)
