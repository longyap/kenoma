import React, { useState } from 'react'
import Pleroma from 'pleroma-api'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux'
import { IonList, IonItem, IonLabel } from '@ionic/react'
import Icons from '../common/icons/icons'
import Account from '../../entities/account'
import { Popover, openHandler } from '../common/popover/popover'
import { FollowButton } from '../action_buttons/action_buttons'

type ProfileActions = {
  className?: string,
  account: Account,
  toggleBlockState?: Function,
  toggleMuteState?: Function
}

export const ProfileActionsComponent = ({ account, className, toggleBlockState, toggleMuteState }: ProfileActions) => {
  const hoveredItem: any = useState(null)
  const popover: React.RefObject<any> = React.createRef()
  const blockButtonText = account.relationships && account.relationships.blocking
  ? { id: 'profileCard.unblock', defaultMessage: 'Unblock' }
  : { id: 'profileCard.block', defaultMessage: 'Block user' }
  const muteButtonText = account.relationships && account.relationships.muting
  ? { id: 'profileCard.unmute', defaultMessage: 'Unmute' }
  : { id: 'profileCard.mute', defaultMessage: 'Mute' }

  return (
    <React.Fragment>
      <FollowButton account={account} className='mr-4' />
      <div
        className='button-outline rounded h-10 w-10 flex items-center justify-center box-content cursor-pointer overlay'
        onClick={(e) => openHandler(e, popover)}
      >
        <Icons.More className='icon w-4 h-4 p-2 icon-light' rotateDeg={90} />
      </div>
      <Popover ref={popover}>
        <IonList lines='none'>
          <IonItem
            className='cursor-pointer'
            onMouseEnter={() => hoveredItem[1]('mute')}
            onMouseLeave={() => hoveredItem[1](null)}
            onClick={() => toggleMuteState && toggleMuteState(account.id, account.relationships ? account.relationships.muting : false)}
          >
            <Icons.VolumeOff className={`w-8 h-8 mr-2 ${hoveredItem[0] === 'mute' && 'link'}`} />
            <IonLabel><span className={hoveredItem[0] === 'mute' ? 'link' : ''}>
              <FormattedMessage id={muteButtonText.id} defaultMessage={muteButtonText.defaultMessage} />
            </span></IonLabel>
          </IonItem>
          <IonItem
            className='cursor-pointer'
            onMouseEnter={() => hoveredItem[1]('block')}
            onMouseLeave={() => hoveredItem[1](null)}
            onClick={() => toggleMuteState && toggleMuteState(account.id, account.relationships ? account.relationships.blocking : false)}
          >
            <Icons.BlockUser className={`w-8 h-8 mr-2 ${hoveredItem[0] === 'block' && 'link'}`} />
            <IonLabel><span className={hoveredItem[0] === 'block' ? 'link' : ''}>
              <FormattedMessage id={blockButtonText.id} defaultMessage={blockButtonText.defaultMessage} />
            </span></IonLabel>
          </IonItem>
        </IonList>
      </Popover>
    </React.Fragment>
  )
}

const mapDispatchToProps = (dispatch: any) => ({
  toggleBlockState: (id: string, blocking: boolean) => dispatch(Pleroma.thunks.users.toggleBlockState({ params: { id, blocking }})),
  toggleMuteState: (id: string, muting: boolean) => dispatch(Pleroma.thunks.users.toggleMuteState({ params: { id, muting } }))
})

export const ProfileActions = connect(null, mapDispatchToProps)(ProfileActionsComponent)