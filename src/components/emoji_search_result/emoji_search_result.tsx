import React from 'react'
import { EmojiClickEventDetail } from 'emoji-picker-element/shared'

export const EmojiSearchResult = ({ emoji, onClick, highlighted }: { emoji: EmojiClickEventDetail, onClick?: Function, highlighted: boolean }) => (
  <li onClick={() => onClick && onClick()} className='emoji-search-result'>
    <div className={`px-4 py-2 flex items-center cursor-pointer ${highlighted ? 'highlighted' : ''}`}>
      { emoji.unicode
        ? <h2 className='text-4xl w-10 h-10 '>{emoji.unicode}</h2>
        : <div className='custom-emoji bg-contain w-10 h-10 rounded-lg overflow-hidden'  style={{backgroundImage: `url(${(emoji as any).url})`}} />
      }
      <div className='px-4 max-w-full'>
        <p className='text-sm text-secondary truncate profile-acct'>{emoji.name}</p>
      </div>
    </div>
  </li>)
