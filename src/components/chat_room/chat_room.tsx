import React, { Component, SyntheticEvent } from 'react'
import { IonPage, withIonLifeCycle, IonContent } from '@ionic/react'
import { connect } from 'react-redux'
import { injectIntl, IntlShape } from 'react-intl'
import Icons from '../common/icons/icons'
import debounce from 'lodash/debounce'
import Pleroma from 'pleroma-api'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import ChatHeader from './chat_header'
import { MessagesList } from '../message/messages_list'
import { scrollDown } from '../../utils/scrollUtils'
import { notFoundErrorCatcher } from '../../utils/api'

dayjs.extend(relativeTime)

type ChatRoom = {
  getConversation?: Function,
  startFetchingConversation?: Function,
  stopFetchingConversation?: Function,
  loadOlder?: Function,
  loadingOlder?: boolean,
  postStatus?: Function,
  id: string
  conversation?: any,
  currentUserId?: string,
  locale: string,
  intl: IntlShape,
  messagesEnd?: HTMLDivElement
}
type ChatRoomState = {
  status: string,
  loading: boolean
}
type PostStatusParams = {
  visibility: string,
  status: string,
  media_ids: [],
  in_reply_to_id: null,
}
class ChatRoomComponent extends Component<ChatRoom, ChatRoomState> {
  constructor (props: ChatRoom) {
    super(props)
    this.state = {
      status: '',
      loading: false
    }
  }
  private messagesList = React.createRef<HTMLElement>()
  private inputElement = React.createRef<HTMLElement>()

  async ionViewDidEnter () {
    const { getConversation, startFetchingConversation } = this.props

    getConversation && await getConversation()
    startFetchingConversation && startFetchingConversation()
    this.focusInput()
  }

  ionViewWillLeave () {
    const { stopFetchingConversation } = this.props

    stopFetchingConversation && stopFetchingConversation()
  }

  focusInput = () => {
    this.inputElement.current && this.inputElement.current.focus()
  }

  async componentDidUpdate (previousProps: ChatRoom) {
    const { id, getConversation, startFetchingConversation , stopFetchingConversation} = this.props

    if (id !== previousProps.id) {
      stopFetchingConversation && await stopFetchingConversation()
      getConversation && await getConversation()
      startFetchingConversation && startFetchingConversation()
    }
  }

  sendMessage = async (e: SyntheticEvent) => {
    const { loading, status } = this.state

    if (loading || !status.length) {
      return
    }
    const { postStatus, conversation } = this.props

    this.setState({ loading: true })
    e.preventDefault()
    e.stopPropagation()
    postStatus && await postStatus({
      status,
      media_ids: [],
      in_reply_to_id: conversation.last_status.id,
      visibility: 'direct'
    })
    this.setState({ status: '', loading: false })
    this.focusInput()
    // @ts-ignore
    scrollDown(this.messagesList.current.scrolledEl)
  }

  onConversationScroll = (e: SyntheticEvent) => {
    if (this.state.loading || this.props.loadingOlder) {
      return
    }
    this.props.loadOlder && this.props.loadOlder()
  }

  render () {
    const { conversation, intl, loadingOlder, currentUserId } = this.props
    const { loading } = this.state
    const messagePlaceholder = intl.formatMessage({id: 'chatRoom.typeAMessage'})

    return (
      <IonPage>
        <IonContent>
          <div className='foreground flex flex-col pt-4 md:pt-0 h-full'>
            {conversation.accounts && <ChatHeader accounts={conversation.accounts} />}
            {loadingOlder
              && <div className='w-6 h-6 mx-auto my-4 text-secondary'>
                <Icons.Cog className='w-10 h-10 a-loader-spin' />
              </div>
            }
            {conversation && conversation.timeline && conversation.timeline.length
              && <MessagesList
                // @ts-ignore
                ref={this.messagesList}
                messages={conversation.timeline}
                locale={intl.locale}
                currentUserId={currentUserId}
                onScrolledTop={debounce(this.onConversationScroll, 100)}
                showAccountName={conversation.accounts ? conversation.accounts.length > 1 : false}
              />}
            {loading
              && <div className='w-6 h-6 mx-auto my-4 text-secondary'>
                <Icons.Cog className='w-10 h-10 a-loader-spin' />
              </div>
            }
            <div className='p-3 md:px-3 md:py-5 background'>
              <form className='flex rounded-lg border foreground input-secondary-border flex items-center cursor-pointer' onSubmit={this.sendMessage}>
                <Icons.Picture className='absolute h-6 w-6 ml-2 link' />
                <input
                  // @ts-ignore
                  ref={this.inputElement}
                  disabled={this.state.loading}
                  value={this.state.status}
                  onChange={e => this.setState({ status: e.target.value })}
                  className='w-full input h-10 p-1 pl-10 focus:outline-none rounded-l-lg text'
                  placeholder={messagePlaceholder}
                  onFocus={this.focusInput}
                />
                <span onClick={this.sendMessage} className='w-6 h-6 mr-4'>
                  <Icons.Send className='w-6 h-6 text-highlighted text-secondary' />
                </span>
              </form>
            </div>
          </div>
        </IonContent>
      </IonPage>
    )
  }
}

const mapStateToProps = ({ conversations, api: { conversation }, users: { currentUser }, intl: { locale } }: any) => ({
  conversations,
  locale,
  currentUser,
  loadingOlder: conversation ? conversation.loadingOlder : false
})

const mapDispatchToProps = (dispatch: Function) => ({
  dispatch,
  dispatchGetConversation: (id: string) => dispatch(Pleroma.thunks.conversations.fetchConversation({ params: { id } })),
  dispatchStartFetchingConversation: (id: string) => dispatch(Pleroma.thunks.api.startFetchingConversation({ params: { id } })),
  dispatchStopFetchingConversation: () => dispatch(Pleroma.thunks.api.stopFetchingConversation()),
  dispatchLoadOlder: (id: string) => dispatch(Pleroma.thunks.api.loadOlderConversation({ params: { id } })),
  dispatchStatusPosting: (params: PostStatusParams, conversationId: string) => dispatch(Pleroma.thunks.statuses.postStatus({ params, conversationId }))
})

const mergeProps = (stateProps: any, dispatchProps: any, ownProps: any) => {
  const id = ownProps.match.params.id
  const conversation = stateProps.conversations.conversationsByIds[id] || { timeline: [] }

  return {
    ...ownProps,
    currentUserId: stateProps.currentUser.id,
    id,
    conversation,
    locale: stateProps.locale,
    loadingOlder: stateProps.loadingOlder,
    getConversation: () => dispatchProps.dispatchGetConversation(id)
      .catch((e: any) => notFoundErrorCatcher(e, 'chat', dispatchProps.dispatch, ownProps.history, ownProps.intl, '/conversations')),
    startFetchingConversation: () => dispatchProps.dispatchStartFetchingConversation(id),
    stopFetchingConversation: () => dispatchProps.dispatchStopFetchingConversation(),
    loadOlder: () => dispatchProps.dispatchLoadOlder(id),
    postStatus: (params: PostStatusParams) => dispatchProps.dispatchStatusPosting(params, id),
  }
}

const ChatRoomIonicComponent = withIonLifeCycle(ChatRoomComponent)
export const ChatRoom = injectIntl(connect(mapStateToProps, mapDispatchToProps, mergeProps)(ChatRoomIonicComponent))
