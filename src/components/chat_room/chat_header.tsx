import React from 'react'
import { Link } from 'react-router-dom'
import Icons from '../common/icons/icons'

const ChatHeader = ({ accounts } : { accounts: any[] }) => (
  <div className='flex items-center justify-between relative text-center mb-4 p-4'>
    <Link className='absolute left-0 ml-3' to='/conversations'>
      <Icons.ChevronRight rotateDeg={180} className='arrow-right block h-8 w-8 text-secondary text-highlighted' />
    </Link>
    <div className='text text-sm ml-8 text-center w-full max-w-full truncate'>
      {accounts.map(({ acct }: { acct: string }) => <span className='mr-1' key={acct}>{acct}</span>)}
    </div>
    <ul className='list-reset relative avatar-group'>
      { accounts.length > 1
        ? accounts.map(({ avatar, acct }: { avatar: string, acct: string }, index: number) => index < 2 ?
          <li
            key={acct}
            className={`rounded-lg overflow-hidden border-2 border-white absolute avatar ${index ? 'bottom-0 left-0' : 'top-0 right-0'}`}
          >
            <img src={avatar} alt={acct} />
          </li>
          : null
        )
        : <li className='w-10 h-10 rounded-lg overflow-hidden border-2 border-white'>
            {!!accounts.length && <img src={accounts[0].avatar} alt={accounts[0].acct} />}
          </li>
      }
    </ul>
  </div>
)

export default ChatHeader
