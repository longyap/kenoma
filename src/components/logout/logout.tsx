import React from 'react'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { oauthThunks } from '../../thunks/oauth'
import Icons from '../common/icons/icons'
import { FormattedMessage } from 'react-intl'

const mapDispatchToProps = (dispatch: Function) => ({
  clearStore: async () => {
    await dispatch(oauthThunks.setInstance({ instance: 'https://undefined' }))
    return dispatch(oauthThunks.logout())
  }
})

export const Logout = connect(null, mapDispatchToProps)(
  ({ clearStore, showIcon }: { clearStore: Function, showIcon?: boolean }) => {
  const history = useHistory();

  const logout = async () => {
    clearStore && await clearStore()
    history.push('/')
  }
  return (
    <div className={`flex items-center cursor-pointer button-primary rounded-lg leading-tight text mt-2 ${!showIcon ? 'pl-4 py-2' : 'py-1'}`} onClick={() => logout()}>
      { !!showIcon && <Icons.Logout className='h-6 w-6 p-2 mr-2' />}
      <FormattedMessage defaultMessage='Logout' id='logout'/>
    </div>
  )
})
