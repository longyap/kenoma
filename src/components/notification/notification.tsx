import React from 'react'
import { Link } from 'react-router-dom'
import Icons from '../common/icons/icons'
import { FormattedMessage } from 'react-intl'
import NotificationInterface from '../../entities/notification'

export const Mention = ({ notification, isSeen }: { notification: NotificationInterface, isSeen: boolean }) => (
  <div className='break-all mt-1'>
    {notification.account.display_name && <Link
      to={`/users/${notification.account.id}`}
      className={`font-semibold text-sm break-all ${isSeen && 'text-secondary'}`}
      dangerouslySetInnerHTML={{ __html: notification.account.display_name }}
    />}
    { notification.status && <Link
      to={`/notice/${notification.status.id}`}
      className={`leading-normal text-sm block break-all ${isSeen && 'text-secondary'}`}
      dangerouslySetInnerHTML={{ __html: notification.status.content }}
    /> }
  </div>
)

export const EmojiReaction = ({ notification, isSeen }: { notification: NotificationInterface, isSeen: boolean }) => (
  <div className='break-all mt-1'>
    <div className='leading-normal text-sm'>
      {notification.account.display_name && <Link
        to={`/users/${notification.account.id}`}
        className={`font-bold ${isSeen && 'text-secondary'}`}
        dangerouslySetInnerHTML={{ __html: notification.account.display_name }}
      />
      }
      <span className={`${isSeen && 'text-secondary'}`}>
        <FormattedMessage id={`notification.${notification.type}`} defaultMessage={notification.type} values={{emoji: notification.emoji}}/>
        { notification.status &&
          <Link
            to={`/notice/${notification.status.id}`}
            className={`break-all block ${isSeen && 'text-secondary'}`}
            dangerouslySetInnerHTML={{ __html: ` ${notification.status.content}` }}
          />
        }
      </span>
    </div>
  </div>
)

export const NonMention = ({ notification, isSeen }: { notification: NotificationInterface, isSeen: boolean }) => (
  <div className='break-all mt-1'>
    <div className='leading-normal text-sm'>
      {notification.account.display_name && <Link
        to={`/users/${notification.account.id}`}
        className={`font-bold ${isSeen && 'text-secondary'}`}
        dangerouslySetInnerHTML={{ __html: notification.account.display_name }}
      />
      }
      <span className={`${isSeen && 'text-secondary'}`}>
        <FormattedMessage id={`notification.${notification.type}`} defaultMessage={notification.type} />
        { notification.status &&
          <Link
            to={`/notice/${notification.status.id}`}
            className={`break-all block ${isSeen && 'text-secondary'}`}
            dangerouslySetInnerHTML={{ __html: ` ${notification.status.content}` }}
          />
        }
      </span>
    </div>
  </div>
)

const badgeClasses = 'absolute shadow h-6 w-6 rounded -mb-1 -mr-1 right-0 bottom-0 foreground'

export const Notification = ({ notification }: { notification: NotificationInterface }) => {
  const isSeen = notification.pleroma && notification.pleroma.is_seen

  const notificationTypeConfig: {[key: string]: any} = {
    'mention': <Mention notification={notification} isSeen={isSeen} />,
    'pleroma:emoji_reaction': <EmojiReaction notification={notification} isSeen={isSeen} />
  }

  return (<div className='flex pb-4 px-3 items-start justify-between text'>
    <div className='flex items-start'>
      <Link to={`/users/${notification.account.id}`} className='w-12 h-12 mr-3 relative'>
        <img
          className={`h-12 w-12 rounded-lg border-2 border-solid overflow-hidden ${(isSeen) ? 'border-transparent' : 'border-highlighted'}`}
          src={notification.account.avatar_static}
          alt={notification.account.acct}
          style={{'minWidth': '3rem'}}
        />
        {notification.type === 'favourite' && <Icons.Star className={badgeClasses} />}
        {notification.type === 'reblog' && <Icons.Repeat className={badgeClasses} />}
      </Link>
      { notificationTypeConfig[notification.type] || <NonMention notification={notification} isSeen={isSeen} />}
    </div>
  </div>
)}
