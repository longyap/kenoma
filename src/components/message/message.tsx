import React from 'react'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import { Attachments } from '../attachments/attachments'

dayjs.extend(relativeTime)
// todo: replace account by User type
export interface MessageType {
  content: string,
  created_at: string,
  media_attachments: any[],
  account: any,
  id: string
}

type MessageProps = {
  message: MessageType,
  locale?: string,
  isOwnMessage?: boolean,
  showAccountName?: boolean
  showTime?: boolean
}

export const Message = (props: MessageProps) => {
  const { message, locale, isOwnMessage, showAccountName, showTime } = props

  return (
    <div className={`flex flex-col mb-4 ${isOwnMessage ? 'self-end items-end' : 'self-start'}`}>
      {!isOwnMessage && showAccountName && <h4 className='text-xs note font-normal leading-none mb-1 ml-6'>{message.account.acct}</h4>}
      <div className={`flex flex-col justify-start px-1 rounded rounded-br-none ${isOwnMessage ? 'items-end' : 'items-start'}`}>
        <div
          className={`text-right px-3 py-2 leading-tight rounded-lg ${isOwnMessage ? 'own-message' : 'message'}`}
          dangerouslySetInnerHTML={{ __html: message.content }}
        />
        { !!message.media_attachments.length && <Attachments attachments={message.media_attachments} imagesPerRow={2} />}
      </div>
      {showTime && <h4 className='text-xs note font-normal leading-none pl-1 mt-2 mb-0'>{dayjs(message.created_at).locale(locale || 'en').fromNow()}</h4>}
    </div>
  )
}
