import React, { SyntheticEvent } from 'react'
import { Message, MessageType } from './message'
import toPairs from 'lodash/toPairs'
import groupBy from 'lodash/groupBy'
import map from 'lodash/map'
import dayjs from 'dayjs'
import ReactDOM from 'react-dom'
import { scrollDown, scrollTo, isScrolledDown, hasOverflow, isScrolledUp } from '../../utils/scrollUtils'
import { didLoadNewerStatuses } from '../timeline/timeline'

type MessagesList = {
  messages: MessageType[],
  currentUserId?: string,
  locale?: string,
  showAccountName: boolean,
  onScrolled?: Function,
  onScrolledTop?: Function,
}

export class MessagesListComponent extends React.Component<MessagesList> {
  scrolledEl: HTMLDivElement | null = null
  previousScrollHeight: number | null = null
  isScrolledDown: boolean = true
  isScrolledUp: boolean | null = null

  scrollDownIfNeeded(){
    if (this.scrolledEl && this.isScrolledDown && hasOverflow(this.scrolledEl)){
      scrollDown(this.scrolledEl)
    }
  }
  handleScroll(e: SyntheticEvent){
    const { onScrolled, onScrolledTop } = this.props
    if (this.scrolledEl){
      this.isScrolledDown = isScrolledDown(this.scrolledEl)
      if (isScrolledUp(this.scrolledEl)){
        onScrolledTop && onScrolledTop(e)
      }
    }
    onScrolled && onScrolled(e)
  }
  componentDidMount(){
    setTimeout(() => this.scrolledEl && scrollDown(this.scrolledEl), 0)
  }
  shouldComponentUpdate(nextProps: MessagesList){
    if (this.scrolledEl) {
      if (didLoadNewerStatuses(this.props.messages, nextProps.messages)) {
        this.previousScrollHeight = this.scrolledEl.scrollHeight
      }
      this.isScrolledUp = isScrolledUp(this.scrolledEl)
    }
    return true
  }
  componentDidUpdate(){
      /* if the list is scrolled all the way up and new items are added, preserve the current scroll position */
      if (this.scrolledEl && this.isScrolledUp && this.previousScrollHeight !== null){
        this.previousScrollHeight = null
        scrollTo(this.scrolledEl, this.previousScrollHeight ? this.scrolledEl.scrollHeight - this.previousScrollHeight : 0)
      }
      else this.scrollDownIfNeeded()
  }
  render() {
    const { messages, locale, currentUserId, showAccountName } = this.props
    const groupedMessagesList = toPairs(groupBy(
      messages.map(
        message => ({
          ...message,
          created_at_date: dayjs(message.created_at).startOf('day').unix()
        })
      ),
      'created_at_date'
    ))

    return <div
      // @ts-ignore
      ref={ el => this.scrolledEl = ReactDOM.findDOMNode(el) }
      onScroll={ e => this.handleScroll(e) }
      className='flex-grow flex flex-col items-end text-sm pl-3 pr-4 pt-2 overflow-y-scroll'
    >
    {groupedMessagesList.map(([ day, messages ], index) => {
      const notTodayMessageGroup = ++index !== groupedMessagesList.length

      return (<div key={day} className='flex flex-col w-full mb-4'>
        {map(messages, (message: MessageType) =>
          <Message
            key={message.id}
            message={message}
            locale={locale}
            isOwnMessage={message.account.id === currentUserId}
            showAccountName={showAccountName}
            showTime={!notTodayMessageGroup}
          />
        )}
        {notTodayMessageGroup && <h4 className='text-xs text-center note font-normal leading-none my-0'>{dayjs(Number(day) * 1000).locale(locale || 'en').format('DD MMMM, YYYY')}</h4>}
      </div>)
    })}
  </div>
  }
}

export const MessagesList = MessagesListComponent
