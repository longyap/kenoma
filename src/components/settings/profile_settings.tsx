import React, { Component, SyntheticEvent } from 'react'
import { connect } from 'react-redux'
import Pleroma from 'pleroma-api'
import { FormattedMessage } from 'react-intl'
import { IonLabel, IonContent, IonPage } from '@ionic/react'
import { Input } from '../common/input/input'
import { AvatarUpload, AVATAR_SHADOW_OVERLAY } from '../media_upload/avatar_upload'
import { oauthThunks } from '../../thunks/oauth'
import Icons from '../common/icons/icons'
import { withRouter } from 'react-router'
import Account from '../../entities/account'
import { ChangePassword } from '../change_password/change_password'
import { ChangeEmail } from '../change_email/change_email'
import { Checkbox } from '../common/checkbox/checkbox'

interface ProfileSettings {
  loadCurrentUser: Function,
  saveUserProfile: Function,
  currentUser: Account,
  className?: string
}
type ProfileSettingsState = {
  formDataWasSet: boolean,
  isLoading: boolean,
  formData: { [key: string]: any },
  previewAvatar: any,
  previewHeader: any
}

export const ProfileSettingsPage = withRouter(({ history }) => (
  <IonPage className='page-margin'>
    <div className='cursor-pointer ml-6 md:ml-0' onClick={() => history.goBack()}>
      <Icons.ChevronRight rotateDeg={180} className='header text-highlighted w-8 h-8' />
    </div>
    <IonContent>
      <ProfileSettings className='p-4'/>
    </IonContent>
  </IonPage>
))

class ProfileSettingsComponent extends Component<ProfileSettings, ProfileSettingsState> {
  constructor(props: ProfileSettings) {
    super(props)
    this.state = {
      formDataWasSet: false,
      isLoading: true,
      formData: {},
      previewAvatar: null,
      previewHeader: null
    }
  }

  componentDidMount() {
    if (!this.state.formDataWasSet && this.props.currentUser) {
      this.preloadForm()
    }
  }

  componentDidUpdate() {
    if (!this.state.formDataWasSet && this.props.currentUser) {
      this.preloadForm()
    }
  }

  preloadForm = () => {
    const { currentUser: { display_name, note, locked, pleroma } } = this.props
    const newFormData = {
      display_name: display_name || '',
      note: note || '',
      locked,
      hide_followers: pleroma ? pleroma.hide_followers : false,
      hide_followers_count: pleroma ? pleroma.hide_followers_count : false,
      hide_follows: pleroma ? pleroma.hide_follows : false,
      hide_follows_count: pleroma ? pleroma.hide_follows_count : false
    }

    this.setState({
      isLoading: false,
      formData: { ...newFormData },
      formDataWasSet: true
    })
  }

  onInputChange = (e: any, field: string, type?: string) => {
    this.setState({
      formData: {
        ...this.state.formData,
        [field]: type === 'checkbox' ? e.detail.checked : e.target.value
      }
    })
  }

  onAvatarLoad = (avatar: File) => {
    const reader = new FileReader()
    reader.readAsDataURL(avatar)
    reader.onload = (e) => {
      if (avatar.type.includes('image')) {
        this.setState({ previewAvatar: e.target ? e.target.result : null })
      }
    }
    this.setState({
      formData: {...this.state.formData, avatar }
    })
  }

  onHeaderLoad = (header: File) => {
    const reader = new FileReader()
    reader.readAsDataURL(header)
    reader.onload = (e) => {
      if (header.type.includes('image')) {
        this.setState({ previewHeader: e.target ? e.target.result : null })
      }
    }
    this.setState({
      formData: {...this.state.formData, header }
    })
  }

  saveUserProfile = async (e: SyntheticEvent) => {
    this.setState({ isLoading: true })
    e.preventDefault()
    const { currentUser, saveUserProfile } = this.props
    const patchedData: FormData = new FormData()

    patchedData.append('id', currentUser.id)
    Object.keys(this.state.formData).forEach((field: string) => {
      patchedData.append(field, this.state.formData[field])
    })
    saveUserProfile && await saveUserProfile(patchedData)
    this.setState({ isLoading: false })
  }

  renderInputLabel = (field: string, defaultMessage: string, options?: { floating?: boolean, note?: { id: string, defaultMessage: string } }) => (
    <IonLabel
      className={options && options.floating ? '' : 'text'}
      position={options && options.floating ? 'floating' : undefined }
    >
      <FormattedMessage
        id={`settings.editProfile.${field}`}
        defaultMessage={defaultMessage}
      />
      {(options && options.note) && <span className='note text-xs block'>
        <FormattedMessage
          defaultMessage={options.note.defaultMessage}
          id={`settings.editProfile.${options.note.id}`}
        />
      </span>}
    </IonLabel>
  )

  render() {
    const { isLoading, formData, previewAvatar, previewHeader } = this.state
    const { currentUser, className } = this.props

    return (
    <div className={`w-full ${className}`}>
      <form
        onSubmit={this.saveUserProfile}
        action='saveUserProfile'
      >
        <div className='flex flex-col items-center'>
          <div
            className='rounded-lg bg-cover bg-no-repeat bg-center h-48 w-full'
            style={{
              backgroundImage: `url(${ previewHeader || currentUser.header_static})`,
              boxShadow: AVATAR_SHADOW_OVERLAY
            }}
          >
            <AvatarUpload
              className='items-start justify-end'
              onUpload={this.onHeaderLoad}
            />
          </div>
          <div
            className='rounded-lg bg-cover bg-no-repeat bg-center h-20 w-20 mt-10'
            style={{
              backgroundImage: `url(${ previewAvatar || currentUser.avatar_static})`,
              boxShadow: AVATAR_SHADOW_OVERLAY
            }}
          >
            <AvatarUpload
              className='items-center justify-center'
              onUpload={this.onAvatarLoad}
            />
          </div>
        </div>
        <Input
          label={this.renderInputLabel('name', 'Name', { floating: true })}
          value={formData.display_name}
          onChange={(e: any) => this.onInputChange(e, 'display_name')}
        />
        <Input
          label={this.renderInputLabel('bio', 'Bio', { floating: true })}
          value={formData.note}
          onChange={(e: any) => this.onInputChange(e, 'note')}
          textarea={true}
        />
        <Checkbox
          value={formData.locked}
          label={this.renderInputLabel('locked', 'Locked', {
            note: { id: 'lockedNote', defaultMessage: 'Restrict your account to approved followers only' }
          })}
          onChange={(e: any) => this.onInputChange(e, 'locked', 'checkbox')}
        />
        <Checkbox
          value={formData.hide_follows}
          label={this.renderInputLabel('hide_follows', `Don't show who I'm following`)}
          onChange={(e: any) => this.onInputChange(e, 'hide_follows', 'checkbox')}
        />
        <Checkbox
          value={formData.hide_follows_count}
          label={this.renderInputLabel('hide_follows_count', `Don't show follow count`)}
          onChange={(e: any) => this.onInputChange(e, 'hide_follows_count', 'checkbox')}
        />
        <Checkbox
          value={formData.hide_followers}
          label={this.renderInputLabel('hide_followers', `Don't show who's following me`)}
          onChange={(e: any) => this.onInputChange(e, 'hide_followers', 'checkbox')}
        />
        <Checkbox
          value={formData.hide_followers_count}
          label={this.renderInputLabel('hide_followers_count', `Don't show follower count`)}
          onChange={(e: any) => this.onInputChange(e, 'hide_followers_count', 'checkbox')}
        />
        <button
          disabled={isLoading}
          className='button-primary rounded-lg py-2 px-4 mt-4 font-bold text-sm focus:outline-none leading-tight'>
          <FormattedMessage defaultMessage='Save' id='settings.save' />
        </button>
      </form>
      <hr className='mt-4' />
      <ChangePassword />
      <hr className='mt-4' />
      <ChangeEmail />
    </div>
  )}
}

const mapStateToProps = (state: any) => (state)
const mapDispatchToProps = (dispatch: Function) => ({ dispatch })
const mergeProps = (state: any, { dispatch }: { dispatch: Function }, ownProps: any) => ({
  loadCurrentUser: () => dispatch(Pleroma.thunks.users.fetchUser({ params: { id: state.users.currentUser.id } })),
  saveUserProfile: (formData: FormData) => dispatch(oauthThunks.updateCredentials(formData)),
  currentUser: state.users.currentUser,
  ...ownProps
})
export const ProfileSettings = connect(mapStateToProps, mapDispatchToProps, mergeProps)(ProfileSettingsComponent)
