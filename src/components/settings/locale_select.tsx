import React from 'react'
import { connect } from 'react-redux'
import { updateIntl } from 'react-intl-redux'
import { setConfig } from '../../utils/storage'
import { IonPage, IonContent } from '@ionic/react'
import Icons from '../common/icons/icons'
import { withRouter } from 'react-router'
import { FormattedMessage } from 'react-intl'
import Account from '../../entities/account'

const locales = [
  { value: 'en', text: 'En' },
  { value: 'ru', text: 'Ru' }
]
type Locale = {
  value: string,
  text: string
}

export const LocaleSelectPage = withRouter(({ history }) => (
  <IonPage className='page-margin'>
    <div className='cursor-pointer ml-6 md:ml-0' onClick={() => history.goBack()}>
      <Icons.ChevronRight rotateDeg={180} className='header text-highlighted w-8 h-8' />
      <span className='page-title px-6 mb-8'>
        <FormattedMessage defaultMessage='Select language' id='settings.selectLanguage' />
      </span>
    </div>
    <IonContent>
      <div className='p-4'>
        <LocaleSelect />
      </div>
    </IonContent>
  </IonPage>
))
const LocaleSelectComponent = ({ locale, onLocaleChange }: { locale: string, onLocaleChange: Function }) => {
  const setLocale = (locale: Locale) => {
    onLocaleChange && onLocaleChange(locale.value)
  }

  return (<div>
    {locales.map(item => (
      <div
        key={item.value}
        onClick={() => setLocale(item)}
        className='text cursor-pointer flex items-center justify-between'
      >
        { item.text }
        {item.value === locale && <Icons.Check />}
      </div>
    ))}
  </div>)
}


const mapStateToProps = ({ users: { currentUser }, intl: { locale }}: { users: { currentUser: Account }, intl: { locale: string }}) => ({
  currentUser,
  locale
})
const mapDispatchToProps = (dispatch: Function) => ({
  onLocaleChange: async (locale: string) => {
    const messages = await import(`../../i18n/${locale || 'en'}.json`).then(({ default: messages }) => messages)
    dispatch(updateIntl({
      locale,
      messages,
    }))
    setConfig({ locale })
  }
})
export const LocaleSelect = connect(mapStateToProps, mapDispatchToProps)(LocaleSelectComponent)