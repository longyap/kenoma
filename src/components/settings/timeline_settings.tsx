import React, { Component } from 'react'
import { IonPage, IonContent, withIonLifeCycle, IonLabel } from '@ionic/react'
import Icons from '../common/icons/icons'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import Pleroma from 'pleroma-api'
import { FormattedMessage } from 'react-intl'
import { Checkbox } from '../common/checkbox/checkbox'
import { setConfig } from '../../utils/storage'
import { Select } from '../common/select/select'
import { WordsFilterConstructor } from './words_filter_constructor'
import { APP_ID } from '../../constants'

export const NOTIFICATIONS_FILTERING: { key: string, text: string }[] = [
  { key: 'favourite', text: 'likes' },
  { key: 'reblog', text: 'repeats' },
  { key: 'follow', text: 'follows' },
  { key: 'mention', text: 'mentions' },
  { key: 'pleroma:emoji_reaction', text: 'reactions' }
]
type TimelineSettingsType = {
  getConfig: Function,
  apiConfig: Object,
  className?: string,
  settings: any,
  saveTimelineSettings: Function
}

type TimelineSettingsState = {
  formData: any,
  formDataWasSet: boolean,
  isLoading: boolean,
  instanceConfig: any,
}
export const TimelineSettingsPage = withRouter(({ history }) => (
  <IonPage className='page-margin'>
    <div className='cursor-pointer ml-6 md:ml-0' onClick={() => history.goBack()}>
      <Icons.ChevronRight rotateDeg={180} className='header text-highlighted w-8 h-8' />
    </div>
    <IonContent>
      <TimelineSettings className='p-4'/>
    </IonContent>
  </IonPage>
))
export class TimelineSettingsComponent extends Component<TimelineSettingsType, TimelineSettingsState> {
  constructor(props: TimelineSettingsType) {
    super(props)
    this.state = {
      formData: null,
      formDataWasSet: false,
      isLoading: false,
      instanceConfig: {}
    }
  }
  async ionViewDidEnter () {
    const { apiConfig } = this.props

    this.setState({ isLoading: true })
    const instanceConfig = await Promise.all([
      Pleroma.api.configs.getConfig({ config: apiConfig }),
      Pleroma.api.configs.getStatusnetConfig({ config: apiConfig })
    ]).then(res => res.reduce((prev, cur) => ({ ...prev, ...cur.data }), {}))

    this.setState({ instanceConfig, isLoading: false })
  }

  componentDidUpdate() {
    if (this.props.settings && !this.state.formDataWasSet) {
      const { settings } = this.props

      this.setState({
        isLoading: false,
        formDataWasSet: true,
        formData: {
          ...settings,
          notificationsFiltering: settings.notificationsFiltering ? settings.notificationsFiltering : {}
        }
      })
    }
  }

  saveTimelineSettings = () => {
    const { formData } = this.state

    this.props.saveTimelineSettings({
      ...formData,
      muteWords: formData.muteWords ? formData.muteWords.reduce((acc: string[], curr: string) => {
        const word = curr.trim()

        return word.length ? acc.concat(word.toLowerCase()) : acc
      }, []) : []
    })
  }

  onInputChange = (e: any, field: string) => {
    this.setState({
      formData: { ...this.state.formData, [field]: e.detail.checked },
    })
  }
  onNotificationFilteringChange = (e: any, field: string) => {
    this.setState({
      formData: {
        ...this.state.formData,
        notificationsFiltering: {
          ...this.state.formData.notificationsFiltering,
          [field]: e.detail.checked
        }
      }
    })
  }
  onThemeChange = (theme: string) =>  {
    this.setState({ formData: { ...this.state.formData, theme } })
  }

  onWordsFilterChange = (filter: string[]) => {
    this.setState({ formData: { ...this.state.formData, muteWords: filter }})
  }

  renderCheckboxLabel = (field: string, defaultMessage: string, defaultValue?: boolean) => (
    <IonLabel className='text'>
      <FormattedMessage
        id={`settings.timeline.${field}`}
        defaultMessage={defaultMessage}
      />
       (<FormattedMessage
        id='settings.timeline.default'
        defaultMessage='default'
        values={{ answer: (this.state.instanceConfig[field] || defaultValue) ? 'yes' : 'no' }}
      />
    )</IonLabel>
  )

  render() {
    const { formData, isLoading, formDataWasSet } = this.state
    const { className } = this.props
    const themeOptions = [
      { text: 'light', value: 'pleroma-light' },
      { text: 'dark', value: 'pleroma-dark' }
    ]

    return (formData
    ? <div className={`w-full ${className}`}>
        <Checkbox
          label={this.renderCheckboxLabel('hideAttachments', 'Hide attachments previews')}
          value={formData.hideAttachments}
          onChange={(e: any) => this.onInputChange(e, 'hideAttachments')}
        />
        <div className='ml-16 my-2'>
          <label className='text text-sm mr-4'>
            <FormattedMessage id='settings.timeline.theme' defaultMessage='Theme' />
          </label>
          <Select
            defaultValue={formData.theme}
            options={themeOptions}
            onSelect={this.onThemeChange}
            fullWidth={true}
          />
        </div>

        <div className='font-bold text mt-4'>
          <FormattedMessage
            defaultMessage='Types of notifications to show'
            id='settings.timeline.notificationsFiltering'
          />
        </div>
        {NOTIFICATIONS_FILTERING.map(({ key, text }) => (
          <Checkbox
            key={key}
            label={this.renderCheckboxLabel(`notifications.${text}`, text[0].toUpperCase().concat(text.slice(1)), true)}
            value={formData.notificationsFiltering[key]}
            onChange={(e: any) => this.onNotificationFilteringChange(e, key)}
          />
        ))}
        <div className='font-bold text mt-4'>
          <FormattedMessage
            id='settings.timeline.wordsFilter'
            defaultMessage='All statuses containing these words will be muted'
          />
        </div>
        { formDataWasSet && <WordsFilterConstructor
          onChange={this.onWordsFilterChange}
          defaultValue={formData.muteWords}
        /> }
        <button
          disabled={isLoading}
          onClick={this.saveTimelineSettings}
          className='button-primary rounded-lg py-2 px-4 mt-4 font-bold text-sm focus:outline-none leading-tight'>
          <FormattedMessage defaultMessage='Save' id='settings.save' />
        </button>
      </div>
    : <div className='w-6 h-6 mx-auto my-4 text-secondary'>
      <Icons.Cog className='w-10 h-10 a-loader-spin' />
    </div>)
  }
}

const mapStateToProps = ({ api, config }: { api: any, config: any }) => ({
  apiConfig: api.config,
  settings: config[APP_ID]
})
const mapDispatchToProps = (dispatch: Function) => ({
  saveTimelineSettings: (settings: any) => {
    dispatch(Pleroma.reducers.config.actions.addConfig({ config: { [APP_ID]: settings } }))
    setConfig({ settings })
  }
})
export const TimelineSettings = connect(mapStateToProps, mapDispatchToProps)(withIonLifeCycle(TimelineSettingsComponent))