import React from 'react'
import { IonButton } from '@ionic/react'
import { FormattedMessage } from 'react-intl'
import Icons from '../common/icons/icons'
import { ENTER_KEY_CODE } from '../poll/poll_form'
import { Input } from '../input/input'

type WordsFilterConstructorType = {
  onChange: Function,
  defaultValue?: string[]
}
type WordsFilterConstructorState = {
  options: string[],
  focusedOptionIndex: number
}

const defaultFormData = {
  options: [''],
  focusedOptionIndex: 0
}

export class WordsFilterConstructor extends React.Component<WordsFilterConstructorType, WordsFilterConstructorState>{
  constructor(props: WordsFilterConstructorType) {
    super(props)

    this.state = JSON.parse(JSON.stringify(defaultFormData))
  }
  optionsRef: React.RefObject<any> = React.createRef()
  optionInputs: any[] = []

  componentDidMount () {
    this.props.defaultValue && this.props.defaultValue.length && this.setState({ options: [...this.props.defaultValue ]})
    this.createOptionInputsList()
  }

  getFilter = () => {
    return [...this.state.options]
  }

  createOptionInputsList = () => {
    this.optionInputs = this.optionsRef.current.querySelectorAll('ion-input')
  }

  optionChange = (val: string | null | undefined, id: number) => {
    const { onChange } = this.props
    const newOptionsArray: any = this.state.options

    newOptionsArray[id] = val
    this.setState({ options: newOptionsArray })
    onChange && onChange(this.getFilter())
  }

  addOption = () => {
    const { onChange } = this.props

    this.setState({ options: this.state.options.concat('')}, () => {
      this.createOptionInputsList()
      this.optionInputs[this.optionInputs.length - 1].setFocus()
      onChange && onChange(this.getFilter())
    })
  }

  deleteOption = (id: number) => {
    const { onChange } = this.props
    const { options } = this.state

    this.setState({
      options: options.length < 2 ? [''] : options.filter((item, i) => i !== id),
      focusedOptionIndex: 0
    }, () => {
      this.createOptionInputsList()
      onChange && onChange(this.getFilter())
    })
  }

  clear = () => this.setState(JSON.parse(JSON.stringify(defaultFormData)))

  onKeyDown = (e: any) => {
    if (e.keyCode === ENTER_KEY_CODE) {
      const focusedOptionIndex = this.state.focusedOptionIndex
      if (focusedOptionIndex !== this.optionInputs.length - 1) {
        this.setState({ focusedOptionIndex: focusedOptionIndex ? focusedOptionIndex + 1 : 1
        }, () => {
          this.optionInputs[this.state.focusedOptionIndex].setFocus()
        })
      } else {
        this.addOption()
        this.setState({ focusedOptionIndex: focusedOptionIndex + 1})
      }
    }
  }
  render() {
    const { options } = this.state

    return (<div className='w-full mb-2'>
      <div
        ref={this.optionsRef}
        onKeyDown={this.onKeyDown}
      >
        { options.map((option, id: number) => (
          <div className='flex items-center mb-2' key={id}>
            <Input
              className='w-full'
              value={option}
              outlined={true}
              onChange={({ detail: { value } }: { detail: { value: string }}) => this.optionChange(value, id)}
            />
            <span onClick={() => this.deleteOption(id)}>
              <Icons.Close className='w-4 h-4 icon-light cursor-pointer'/>
            </span>
          </div>
        )) }
      </div>
      <div className='flex items-center'>
        <IonButton fill='clear' onClick={this.addOption} className='text-left border input-secondary-border rounded capitalize w-full mr-2'>
          <FormattedMessage id='wordsFilter.add_word' defaultMessage='Add word'/>
        </IonButton>
      </div>
    </div>)
  }
}