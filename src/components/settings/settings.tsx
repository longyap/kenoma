import React, { ReactElement, useState, useEffect } from 'react'
import { IonPage, IonContent } from '@ionic/react'
import { FormattedMessage } from 'react-intl'
import { NavLink } from 'react-router-dom'
import { SETTINGS_LINKS_LIST } from './mobile_settings'
import { connect } from 'react-redux'
import { Logout } from '../logout/logout'
import Icons from '../common/icons/icons'
import Account from '../../entities/account'
import { Version } from '../version/version'
import { oauthThunks } from '../../thunks/oauth'

export const SettingsComponent = ({ children, currentUser, updateCurrentUser }: { children: ReactElement, currentUser: Account, updateCurrentUser: Function }) => {
  const [userDataWasFetched, switchFetchedFlag] = useState(false);

  useEffect(() => {
    if (!userDataWasFetched) {
      switchFetchedFlag(true)
      updateCurrentUser()
    }
  }, [switchFetchedFlag, updateCurrentUser, userDataWasFetched])

  return (<IonPage className='page-margin px-6 md:pl-0 md:pr-4'>
    <div style={{height: 'calc(100% - 4rem)'}}>
      <span className='page-title px-6 mb-8'>
        <FormattedMessage defaultMessage='Settings' id='settings' />
      </span>
      <div className='flex min-h-full'>
        <div className='pr-8 flex flex-col' style={{width: '12rem'}}>
          {SETTINGS_LINKS_LIST.map((link, idx) => (
            <NavLink
              to={link.to.indexOf('/users') !== -1 ? `/users/${currentUser.acct}` : link.to}
              key={link.to}
              activeClassName='link link-menu'
              className={`block border border-t-0 border-l-0 flex items-center justify-between text py-4 break-normal ${idx === SETTINGS_LINKS_LIST.length - 1 ? 'border-b-0' : ''}`}
            >
              <span className='ml-4 break-normal'><FormattedMessage defaultMessage={link.messageDefaultVal} id={link.messageId} /></span>
              <Icons.ChevronRight rotateDeg={0} className='text-secondary w-4 h-4 mr-4 text-highlighted'/>
            </NavLink>))}
            <Logout showIcon={false} />
            <Version cssClass='justify-between'/>
        </div>
        <div className='w-3/4 flex-1 overflow-y-auto mp-4'>
          <IonContent>{children}</IonContent>
        </div>
      </div>
    </div>
  </IonPage>)
}

const mapStateToProps = (state: any) => ({
  currentUser: state.users.currentUser,
})
const mapDispatchToProps = (dispatch: Function) => ({
  updateCurrentUser: () => dispatch(oauthThunks.updateCurrentUser())
})
export const Settings = connect(mapStateToProps, mapDispatchToProps)(SettingsComponent)
