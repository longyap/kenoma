import React from 'react'
import { IonPage, IonContent } from '@ionic/react'
import { FormattedMessage } from 'react-intl'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { ProfileCard } from '../profile_card/profile_card'
import Icons from '../common/icons/icons'
import { Logout } from '../logout/logout'
import Account from '../../entities/account'
import { Version } from '../version/version'

export const SETTINGS_LINKS_LIST = [
  { to: '/users/', icon: 'Eye',  messageDefaultVal: 'View Profile', messageId: 'settings.viewProfile' },
  { to: '/settings/profile', icon: 'User',  messageDefaultVal: 'Edit Profile', messageId: 'settings.editProfile' },
  { to: '/settings/timeline', icon: 'Timeline',  messageDefaultVal: 'Timeline Settings', messageId: 'settings.timelineSettings' },
  { to: '/settings/language', icon: 'Language',  messageDefaultVal: 'Language', messageId: 'settings.languageSettings' },
  { to: '/settings/mutes-blocks', icon: 'Language', messageDefaultVal: 'Mutes and Blocks', messageId: 'settings.mutesAndBlocks' }
]

export const MobileSettingsComponent = ({ currentUser }: { currentUser: Account }) => (
  <IonPage className='page-margin'>
    <span className='page-title mb-8 px-6'>
      <FormattedMessage defaultMessage='Settings' id='settings' />
    </span>
    <IonContent>
      <div className='p-4 min-h-full flex flex-col'>
        <ProfileCard
          account={currentUser}
          isOtherUser={false}
          fullView={false}
          hideActions={true}
          className='mb-0 border-t-0 border-r-0 border-l-0 border'
        />
        {SETTINGS_LINKS_LIST.map(link => (
          <NavLink
            key={link.to}
            to={link.to.indexOf('/users') !== -1 ? `/users/${currentUser.acct}` : link.to}
            className='flex text items-center py-1'
          >
            {React.createElement(Icons[link.icon], { className: 'w-8 h-8 icon mr-4' })}
            <span><FormattedMessage defaultMessage={link.messageDefaultVal} id={link.messageId} /></span>
          </NavLink>
        ))}
        <Logout showIcon={true}/>
        <Version cssClass='py-2'/>
      </div>
    </IonContent>
  </IonPage>
)

const mapStateToProps = (state: any) => ({
  currentUser: state.users.currentUser,
})
export const MobileSettings = connect(mapStateToProps)(MobileSettingsComponent)
