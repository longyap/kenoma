import React from 'react'

interface ProgressBar extends React.HTMLAttributes<HTMLDivElement> {
  buffer: number,
  value: number,
  label: string
}

export const ProgressBar = ({ value, buffer, label }: ProgressBar) => (
  <div className='w-full flex items-center pr-8'>
    <div className='w-full h-8 border border-secondary'>
      <div className='h-full progress-bar max-w-full' style={{width: `${buffer ? value / buffer * 100 : 0}%`}}/>
      <div className='-mt-6 ml-2 font-bold'>{label}</div>
    </div>
    <div className='ml-2 w-6 absolute right-0 text-right'>{value}</div>
  </div>
)