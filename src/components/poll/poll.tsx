import React, { Component } from 'react'
import { Dispatch } from 'redux'
import Pleroma from 'pleroma-api'
import { connect } from 'react-redux'
import { IonCheckbox, IonLabel, IonItem, IonList, IonButton, IonRadio, IonRadioGroup, withIonLifeCycle } from '@ionic/react'
import { uniq, without } from 'lodash'
import { FormattedMessage } from 'react-intl'
import { ProgressBar } from './progress_bar'
import dayjs from 'dayjs'

import './poll.css'

interface PollComponent extends React.HTMLAttributes<HTMLDivElement> {
  vote: Function,
  startFetchingPoll: Function,
  stopFetchingPoll: Function,
  poll: Poll,
  statusId: string,
  cssClass: string
}
type PollComponentState = {
  votes: any[],
  showResults: boolean
}
type Poll = {
  id: string,
  options: Option[],
  votes_count: number,
  voted: boolean,
  expired: boolean,
  expired_at: string,
  multiple: boolean
}
type Option= {
  title: string,
  votes_count: number
}
class PollComponent extends Component<PollComponent, PollComponentState> {
  constructor(props: PollComponent) {
    super(props)

    this.state = {
      votes: [],
      showResults: false
    }
  }

  componentDidMount() {
    this.loadPoll()
  }

  ionViewDidEnter() {
    this.loadPoll()
  }

  loadPoll = () => {
    const { startFetchingPoll, poll, statusId } = this.props

    startFetchingPoll && startFetchingPoll(poll.id, statusId)
  }

  ionViewWillLeave() {
    const { stopFetchingPoll, statusId } = this.props

    stopFetchingPoll(statusId)
  }

  voteOption = ({ detail: { value } }: any) => {
    const { poll } = this.props
    const { votes } = this.state

    poll.multiple
      ? this.setState({ votes: votes.includes(value)
        ? without(votes, value)
        : uniq([...votes, value]) })
      : this.setState({ votes: [value] })
  }

  votePoll = async () => {
    const { poll, vote, statusId } = this.props
    const { votes } = this.state

    vote && vote(poll.id, votes.map(option => +option), statusId)
      .then(() => this.setState({ showResults: true }))
  }

  renderResults = (poll: Poll) => poll.options.map(option => (
    <IonItem key={option.title} className='text-xs flex items-center mb-4 h-8' lines='none'>
      <ProgressBar value={option.votes_count} buffer={poll.votes_count} label={option.title} />
    </IonItem>
  ))

  renderSingleChoicePoll = (poll: Poll) => (<IonRadioGroup>
    { poll.options.map((option, id) => (
      <IonItem key={option.title} className='text-xs' lines='none'>
        <IonRadio
          value={id.toString()}
          className='mr-2 my-2'
          disabled={poll.voted || poll.expired}
          onIonSelect={this.voteOption}
        />
        {this.state.showResults
          ? <ProgressBar value={option.votes_count} buffer={poll.votes_count} label={option.title} />
          : <IonLabel>{option.title}</IonLabel>
        }
      </IonItem>
    ))}
  </IonRadioGroup>)

  renderMultiplePoll = (poll: Poll) => poll.options && poll.options.map((option, id) => (
    <IonItem key={option.title} className='text-xs' lines='none'>
      <IonCheckbox
        className='mr-2 my-2'
        color='primary'
        value={id.toString()}
        disabled={poll && poll.voted}
        onIonChange={this.voteOption}
      />
      {this.state.showResults
        ? <ProgressBar value={option.votes_count} buffer={poll.votes_count} label={option.title} />
        : <IonLabel>{option.title}</IonLabel>
      }
    </IonItem>
  ))

  renderActionsPanel = () => (<div className='flex -mx-4 mb-4'>
    <IonButton
      className='link cursor-pointer border text-center normal-case font-normal m-0 border-l-0 w-full'
      fill='clear'
      style={{minWidth: '50%'}}
      onClick={this.votePoll}>
        <FormattedMessage id='poll.vote' defaultMessage='Vote' />
      </IonButton>
    {!this.state.showResults && <IonButton
      className='link cursor-pointer border text-center normal-case font-normal m-0 border-r-0 border-l-0 w-full'
      fill='clear'
      style={{minWidth: '50%'}}
      onClick={() => this.setState({ showResults: true })}>
        <FormattedMessage defaultMessage='See Results' id='poll.seeResults' />
      </IonButton>}
  </div>)

  render() {
    const { poll, cssClass } = this.props

    return (<div className={`poll foreground mb-4 ${cssClass || 'px-4'}`}>
      <IonList className='foreground'>
        {poll && (poll.voted || poll.expired)
          ? this.renderResults(poll)
          : poll.multiple
            ? this.renderMultiplePoll(poll)
            : this.renderSingleChoicePoll(poll)
        }
      </IonList>
      {poll && (!poll.voted && !poll.expired) && this.renderActionsPanel()}
      <div className='note text-xs text-right'>
        {poll.expired
          ? <FormattedMessage id='poll.expired' defaultMessage='expired' values={{date: dayjs(poll.expired_at).format('DD MM YYYY')}}/>
          : <FormattedMessage id='poll.expires' defaultMessage='will continue' values={{date: dayjs(poll.expired_at).format('DD MM YYYY')}}/>
        }
      </div>
    </div>)
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  vote: (id: string, choices: string[], statusId: string) => dispatch(Pleroma.thunks.polls.vote({ params: { id, choices, statusId }})),
  startFetchingPoll: (id: string, statusId: string) => dispatch(Pleroma.thunks.api.startFetchingPoll({ params: { id, statusId, interval: 8000 } })),
  stopFetchingPoll: (statusId: string) => dispatch(Pleroma.thunks.api.stopFetchingPoll({ params: { statusId } }))
})

export const Poll = connect(null, mapDispatchToProps)(withIonLifeCycle(PollComponent))
