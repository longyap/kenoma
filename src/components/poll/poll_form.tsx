import React from 'react'
import { IonButton, IonList, IonItem, IonLabel, IonToggle } from '@ionic/react'
import { FormattedMessage } from 'react-intl'
import Icons from '../common/icons/icons'
import dayjs from 'dayjs'
import { Datepicker } from '../common/datepicker/datepicker'
import { Popover, openHandler } from '../common/popover/popover'
import { Input } from '../input/input'
import { DATEPICKER_MIN_MAX_FORMAT } from '../../constants'

export const ENTER_KEY_CODE = 13

type PollFormType = {
  onChange: Function
}
type PollFormState = {
  options: string[],
  multiple: boolean,
  expires_in: number,
  focusedOptionIndex: number
}

const defaultPollFormData = {
  options: ['', ''],
  multiple: false,
  expires_in: 600,
  focusedOptionIndex: 0
}

export class PollForm extends React.Component<PollFormType, PollFormState>{
  constructor(props: PollFormType) {
    super(props)

    this.state = JSON.parse(JSON.stringify(defaultPollFormData))
  }
  optionsRef: React.RefObject<any> = React.createRef()
  optionInputs: any[] = []
  popover: React.RefObject<any> = React.createRef()

  componentDidMount () {
    this.createOptionInputsList()
    if (this.optionInputs.length) {
      this.optionInputs[0].setFocus()
    }
  }

  getPoll = () => {
    const { options, multiple, expires_in } = this.state

    return {
      options: [...options],
      multiple,
      expires_in
    }
  }

  createOptionInputsList = () => {
    this.optionInputs = this.optionsRef.current.querySelectorAll('ion-input')
  }

  optionChange = (val: string | null | undefined, id: number) => {
    const { onChange } = this.props
    const newOptionsArray: any = this.state.options

    newOptionsArray[id] = val
    this.setState({ options: newOptionsArray })
    onChange && onChange(this.getPoll())
  }

  addOption = () => {
    const { onChange } = this.props

    this.setState({ options: this.state.options.concat('')}, () => {
      this.createOptionInputsList()
      this.optionInputs[this.optionInputs.length - 1].setFocus()
      onChange && onChange(this.getPoll())
    })
  }

  deleteOption = (id: number) => {
    const { onChange } = this.props
    const { options } = this.state

    if (options.length < 3) return
    this.setState({ options: options.filter((item, i) => i !== id) })
    onChange && onChange(this.getPoll())
  }

  switchMultiple = ({ detail }: { detail: { checked: boolean }}) => {
    const { onChange } = this.props

    this.setState({ multiple: detail.checked })
    onChange && onChange(this.getPoll())
  }

  onExpiresDateChange = (val: string) => {
    const { onChange } = this.props
    const unixDate =  dayjs(val).unix()
    const unixNow = dayjs(new Date()).unix()

    this.setState({ expires_in: unixDate - unixNow })
    onChange && onChange(this.getPoll())
  }

  clear = () => this.setState(JSON.parse(JSON.stringify(defaultPollFormData)))

  onKeyDown = (e: any) => {
    if (e.keyCode === ENTER_KEY_CODE) {
      if (this.state.focusedOptionIndex !== this.optionInputs.length - 1) {
        this.setState({ focusedOptionIndex: this.state.focusedOptionIndex + 1}, () => {
          this.optionInputs[this.state.focusedOptionIndex].setFocus()
        })
      } else {
        this.addOption()
        this.setState({ focusedOptionIndex: this.state.focusedOptionIndex + 1})
      }
    }
  }
  render() {
    const { options } = this.state
    const min = dayjs(new Date()).format(DATEPICKER_MIN_MAX_FORMAT)

    return (<div className='w-full mb-2'>
      <div
        ref={this.optionsRef}
        onKeyDown={this.onKeyDown}
      >
        { options.map((option, id: number) => (
          <div className='flex items-center mb-2' key={id}>
            <Input
              className='w-full'
              value={option}
              outlined={true}
              onChange={({ detail: { value } }: { detail: { value: string }}) => this.optionChange(value, id)}
            />
            <span onClick={() => this.deleteOption(id)}>
              <Icons.Close className='w-4 h-4 icon-light cursor-pointer'/>
            </span>
          </div>
        )) }
      </div>
      <div className='flex items-center'>
        <IonButton
          fill='clear'
          onClick={this.addOption}
          className='text-left border input-secondary-border rounded capitalize w-full mr-2'
        >
          <FormattedMessage id='postStatusForm.poll.addOption' defaultMessage='Add option'/>
        </IonButton>
        <span onClick={(e) => openHandler(e, this.popover)}><Icons.Cog className='w-4 h-4 icon-light cursor-pointer' /></span>
      </div>
      <Popover ref={this.popover}>
        <IonList>
          <IonItem lines='none'>
            <IonLabel>
              <FormattedMessage id='postStatusForm.poll.multiple' defaultMessage='Allow users to choose multiple options' />
            </IonLabel>
            <IonToggle onIonChange={this.switchMultiple} mode='ios' />
          </IonItem>

          <IonItem lines='none'>
            <IonLabel>
              <FormattedMessage id='postStatusForm.poll.endDate' defaultMessage='End date' />
            </IonLabel>
            <Datepicker onChange={this.onExpiresDateChange} options={{ min }} />
          </IonItem>
        </IonList>
      </Popover>
    </div>)
  }
}
