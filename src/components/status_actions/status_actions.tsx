import React, { useState, useEffect, SyntheticEvent } from 'react'
import Icons from '../common/icons/icons'
import Pleroma from 'pleroma-api'
import { connect } from 'react-redux'
import { PostStatusModal } from '../post_status/post_status_modal'
import { FormattedMessage } from 'react-intl'
import { Popover, openHandler } from '../common/popover/popover'
import { Tooltip } from '../common/tooltip/tooltip'
import { STATUS_VISIBILITY_OPTIONS } from '../../constants'
import Status from '../../entities/status'
import Account from '../../entities/account'
import { EmojiPanel } from '../emoji_panel/emoji_panel'
import { UsersListTooltip } from '../users_list_tooltop/users_list_tooltip'
const ICON_CLASSES = 'h-8 w-8 text-secondary flex-shrink-0'

interface StatusActionsInterface {
  status: Status,
  // we need statusId because it can be reblog of actual status, but they are stored in pleroma-api separately
  statusId?: String,
  userId?: string,
  currentUserId: string,
  toggleThreadFetcher?: Function,
  toggleRebloggedStatus: Function,
  toggleFavouritedStatus: Function,
  toggleEmojiReaction: Function,
  toggleMutedStatus: Function,
  toggleStatusDelete: Function,
  getStatusFullInfo: Function
}

const StatusActionsComponent = ({
  status,
  statusId,
  userId,
  currentUserId,
  toggleThreadFetcher,
  toggleRebloggedStatus,
  toggleFavouritedStatus,
  toggleEmojiReaction,
  toggleMutedStatus,
  toggleStatusDelete,
  getStatusFullInfo
}: StatusActionsInterface) => {
  const [showReplyForm, setShowReplyForm] = useState(false)
  const [belongsToCurrentUser, setBelongsToCurrentUser] = useState(false)
  const [reblogged, setReblogged] = useState(false)
  const [reblogAnimating, setReblogAnimating] = useState(false)
  const [favourited, setFavourited] = useState(false)
  const [favAnimating, setFavAnimating] = useState(false)
  const [emojiPanelAnimating, setEmojiPanelAnimating] = useState(false)
  const [muted, setMuted] = useState(false)
  const [loading, toggleLoading] = useState(false)
  const [statusList, showStatusList] = useState('')
  const extraActions: React.RefObject<any> = React.createRef()
  const emojiPanelRef: React.RefObject<any> = React.createRef()

  useEffect(() => {
    setBelongsToCurrentUser(currentUserId === status.account.id)
    setReblogged(status.reblogged)
    setFavourited(status.favourited)
    setMuted(status.muted)
  }, [status, currentUserId])

  const toggleReplyForm = () => {
    toggleThreadFetcher && toggleThreadFetcher()
    setShowReplyForm(!showReplyForm)
  }

  const toggleEmojiPanel = (e: SyntheticEvent) => {
    setEmojiPanelAnimating(true)
    openHandler(e, emojiPanelRef)
    setTimeout(() => setEmojiPanelAnimating(false), 1000)
  }

  const reblog = () => {
    const { id, reblogged, reblog  } = status
    if (!reblogged) {
      setReblogAnimating(true)
      setTimeout(() => setReblogAnimating(false), 1000)
    }
    setReblogged(!reblogged)
    toggleRebloggedStatus(reblog ? reblog.id : id, reblogged)
  }

  const favourite = async () => {
    const { id, favourited, reblog } = status
    if (!favourited) {
      setFavAnimating(true)
      setTimeout(() => setFavAnimating(false), 1000)
    }
    setFavourited(!favourited)
    toggleFavouritedStatus(reblog ? reblog.id : id, favourited)
  }

  const onEmojiSelect = async ({ res }: any) => {
    const selectedEmoji = res.unicode || res.name
    const existedReaction = status.pleroma ? status.pleroma.emoji_reactions.find((item) => item.name.codePointAt(0) === selectedEmoji.codePointAt(0)) : null;
    const reacted = existedReaction ? existedReaction.me : false

    toggleEmojiReaction(selectedEmoji, statusId, reacted);
    (emojiPanelRef.current as any).close()
  }

  const muteConversation = () => {
    const { id, muted, reblog } = status

    setMuted(!muted)
    toggleMutedStatus(reblog ? reblog.id : id, muted);
    (extraActions.current as any).close()
  }

  const deleteStatus = () => {
    const { id } = status

    toggleStatusDelete(id, userId);
    (extraActions.current as any).close()
  }

  const getFullStatusInfo = async(entity: '' | 'favourited_by' | 'reblogged_by' | 'replied_by') => {
    showStatusList(entity)
    if (loading) return
    toggleLoading(true)
    await getStatusFullInfo(statusId || status.id, userId)
    toggleLoading(false)
  }

  const renderVisibilityIndicator = () => {
    const visibility = STATUS_VISIBILITY_OPTIONS.find(({ value }) => value === status.visibility)

    return visibility ? <Tooltip
      target={<span className='ml-auto flex-shrink-0 align-middle'>
        {React.createElement((visibility.icon as any), ({ className: 'h-8 w-8 text-secondary opacity-50' } as any))}
      </span>}
    >
      <FormattedMessage id={visibility.textId} />
    </Tooltip>
    : null
  }

  const renderExtraActionsList = () => (
    <ul className='list-reset py-2'>
      <li className='text-sm button-inline cursor-pointer px-2 flex items-center text' onClick={muteConversation}>
        <Icons.VolumeOff className={`h-8 w-8 text-secondary ${muted ? 'link' : 'text-highlighted'}`} />
        <FormattedMessage
          defaultMessage={muted ? 'Unmute conversation' : 'Mute conversation'}
          id={`statusActions.${muted ? 'unmute' : 'mute'}Conversation`}
        />
      </li>
      {belongsToCurrentUser &&
      <li className='text-sm button-inline cursor-pointer px-2 flex items-center text' onClick={deleteStatus}>
        <Icons.Delete className='text-secondary w-8 h-8 mr-1 text-highlighted' />
        <FormattedMessage defaultMessage='Delete' id='statusActions.delete' />
      </li>}
    </ul>
  )

  const renderStatusStat = (onClick: any, state: boolean, itemsCount: number, entity: '' | 'favourited_by' | 'reblogged_by' | 'replied_by', icon: any, animationState: boolean) => (
    <div
      onClick={onClick}
      onMouseEnter={() => getFullStatusInfo(entity)}
      className='cursor-pointer flex mr-2 h-8 w-full'
      style={{maxWidth: '3.5rem'}}
    >
      {React.createElement(icon, { className: `${ICON_CLASSES} ${state ? 'link' : 'text-highlighted'} ${animationState && 'a-bloom'}` }) }
      {itemsCount ? <UsersListTooltip
        loading={loading}
        // @ts-ignore
        accounts={statusList ? status[statusList] : null}
      >
        <span className='text-s text-secondary leading-9 ml-1'>
          {itemsCount}
        </span>
      </UsersListTooltip> : null}
    </div>)

  return (
    <div>
      <div className='flex items-start'>
        { renderStatusStat(toggleReplyForm, false, 0, 'replied_by', Icons.Reply, false) }
        { status.visibility !== 'private' && status.visibility !== 'direct' && renderStatusStat(reblog, reblogged, status.reblogs_count, 'reblogged_by', Icons.Repeat, reblogAnimating) }
        { renderStatusStat(favourite, favourited, status.favourites_count, 'favourited_by', Icons.Star, favAnimating) }
        { renderStatusStat(toggleEmojiPanel, false, 0, '', Icons.EmojiReaction, emojiPanelAnimating) }
        <div className='cursor-pointer flex mr-2 h-8 w-full' style={{maxWidth: '3.5rem'}}>
          <Popover ref={extraActions}>
            {renderExtraActionsList()}
          </Popover>
          <span className='h-8 w-8' onClick={(e) => openHandler(e, extraActions)} >
          <Icons.More className={ICON_CLASSES + ' text-highlighted'} />
          </span>
        </div>
      {renderVisibilityIndicator()}
      </div>
      { showReplyForm && <PostStatusModal
        onPost={toggleReplyForm}
        onClose={toggleReplyForm}
        repliedStatus={status}
      /> }
      <Popover ref={emojiPanelRef} cssClass='emoji-picker'>
        <EmojiPanel onSelect={onEmojiSelect} noCustomEmojis={true} />
      </Popover>
    </div>
  )
}

const mapStateToProps = ({ users: { currentUser } }: { users: { currentUser: Account } }) => ({ currentUser })

const mapDispatchToProps = (dispatch: Function) => ({ dispatch })

const mergeProps = (stateProps: { currentUser: Account }, { dispatch }: { dispatch: Function }, ownProps: any) => ({
  ...ownProps,
  currentUserId: stateProps.currentUser.id,
  toggleFavouritedStatus: (id: string, favourited: boolean) => dispatch(Pleroma.thunks.statuses.toggleFavouritedStatus({ params: { id }, favourited })),
  toggleRebloggedStatus: (id: string, reblogged: boolean) => dispatch(Pleroma.thunks.statuses.toggleRebloggedStatus({
    params: { id },
    reblogged,
    user: stateProps.currentUser
  })),
  toggleEmojiReaction: (emoji: String, statusId: String, reacted: boolean) => dispatch(Pleroma.thunks.reactions.toggleReaction({ params: { statusId, emoji }, reacted })),
  toggleStatusDelete: (id: string, userId: string) => dispatch(Pleroma.thunks.statuses.deleteStatus({ params: { id, userId } })),
  toggleMutedStatus: (id: string, muted: boolean) => dispatch(Pleroma.thunks.statuses.toggleMutedStatus({ params: { id }, muted })),
  getStatusFullInfo: (id: string, userId: string) => dispatch(Pleroma.thunks.statuses.getStatusLists({ params: { id, userId } })),
})

export const StatusActions = connect(mapStateToProps, mapDispatchToProps, mergeProps)(StatusActionsComponent)
