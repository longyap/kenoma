import React, { useContext } from 'react';
import TooltipTrigger from 'react-popper-tooltip';
import { IsMobileContext } from '../../../isMobileContext';

export const Tooltip = (props: any) => {
  const { isMobile } = useContext(IsMobileContext)

  return (
    <TooltipTrigger
      placement={props.placement || 'auto' }
      trigger={isMobile ? 'click' : 'hover'}
      tooltip={({
        tooltipRef,
        getTooltipProps
      }: any) => (
        <div
          {...getTooltipProps({
            ref: tooltipRef,
            className: `tooltip-container foreground rounded p-2 ${props.containerCssClass}`
          })}
        >
          {props.children}
        </div>
      )}>
      {({getTriggerProps, triggerRef}: any) => React.cloneElement(props.target, getTriggerProps({ ref: triggerRef }))}
    </TooltipTrigger>
  )
}
