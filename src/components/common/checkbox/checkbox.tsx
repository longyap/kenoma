import React from 'react'
import { IonItem, IonCheckbox } from '@ionic/react'

type Input = {
  value: boolean,
  onChange?: any,
  message?: string,
  error?: boolean,
  errorMessage?: string,
  label: any,
}
export const Checkbox = ({ value, onChange, message, error, errorMessage, label }: Input) => (
  <IonItem className='checkbox'>
    { label }
    <IonCheckbox
      className={`${error && 'border-error'}`}
      checked={value}
      slot='start'
      onIonChange={onChange}
    />
    <p className={`note text-xs ${error && 'error'}`}>{error ? errorMessage : message}</p>
  </IonItem>
)
