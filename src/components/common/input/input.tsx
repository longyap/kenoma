import React from 'react'
import { IonInput, IonTextarea, IonItem } from '@ionic/react'

export const generateErrorMessage = (messages: string[]) => messages.reduce((acc: string, curr: string) => acc.concat(curr + ' '), '')

type Input = {
  value: string,
  onChange?: any,
  placeholder?: string,
  message?: string,
  error?: boolean,
  errorMessage?: string,
  textarea?: boolean,
  type?: 'number' | 'time' | 'text' | 'tel' | 'url' | 'email' | 'search' | 'date' | 'password',
  className?: string,
  label: any
}
export const Input = ({ value, onChange, placeholder, message, error, errorMessage, type, textarea, className, label }: Input) => (
  <IonItem className={'input '.concat(className || '')}>
    { label }
    { textarea
      ? <IonTextarea
        className={`input-secondary-border ${error && 'border-error'}`}
        value={value}
        placeholder={placeholder}
        onIonChange={onChange}
      />
      : <IonInput
        className={`input-secondary-border ${error && 'border-error'}`}
        value={value}
        type={type}
        placeholder={placeholder}
        onIonChange={onChange}
      />
    }
    <p className={`note text-xs ${error && 'error'}`}>{error ? errorMessage : message}</p>
  </IonItem>
)
