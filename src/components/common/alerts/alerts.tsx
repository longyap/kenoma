import React from 'react'
import { connect } from 'react-redux'
import { IonToast } from '@ionic/react'
import { uiActions } from '../../../reducers/ui'

export const Error: React.FunctionComponent<{cssClass?: string, error?: string}> = props => (
  <div className={`pop-out background-error my-4 text-center ${props.cssClass}`}>{props.error}</div>
)

export const Success: React.FunctionComponent<{cssClass?: string, label?: string}> = props => (
  <div className='background-success pop-out my-4 text-center'>
    {props.children}
  </div>
)

const ToastComponent: React.FunctionComponent<{isOpen: boolean, type: string, message: string, closeAlert: Function}> = props => {
  const { isOpen, message, closeAlert, type } = props

  return (
    <IonToast
      position='top'
      color={type}
      isOpen={isOpen}
      onDidDismiss={() => closeAlert()}
      message={message}
      duration={3000}
    />
  )
}
const mapStateToProps = ({ ui: { alert } }: { ui: { alert: { isOpen: boolean, type: string, message: string }}}) => ({
  ...alert
})
const mapDispatchToProps = (dispatch: Function) => ({
  closeAlert: () =>  dispatch(uiActions.closeAlert())
})
export const Toast = connect(mapStateToProps, mapDispatchToProps)(ToastComponent)
