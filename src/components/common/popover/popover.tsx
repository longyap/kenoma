import React, { Component, SyntheticEvent } from 'react'
import { IonPopover } from '@ionic/react'
import { IsMobileContext } from '../../../isMobileContext'

export const openHandler = async (e: SyntheticEvent, ref: React.RefObject<any>) => {
  e.persist()
  await ref.current.open(e)
}
type PopoverProps = {
  onClose?: Function,
  showBackdrop?: boolean,
  cssClass?: string,
  width?: string
}
type PopoverState = {
  event?: any,
  isOpen: boolean
}

class Popover extends Component<PopoverProps, PopoverState> {
  constructor (props: PopoverProps) {
    super(props)
    this.state = {
      event: null,
      isOpen: false
    }
  }
  open = async (event: SyntheticEvent) => {
    event.persist()
    return this.setState({
      isOpen: true,
      event
    })
  }
  close = () => {
    this.setState({ isOpen: false, event: null })
    const { onClose } = this.props

    onClose && onClose()
  }
  render() {
    const { isOpen, event } = this.state
    const { isMobile } = this.context
    const { showBackdrop, cssClass } = this.props

    return (<IonPopover
      isOpen={isOpen}
      event={event}
      onDidDismiss={this.close}
      cssClass={`${isMobile ? 'popover-bottom' : ''} ${cssClass}`}
      showBackdrop={showBackdrop}
    >
      {this.props.children}
    </IonPopover>)
  }
}

Popover.contextType = IsMobileContext
export { Popover }
