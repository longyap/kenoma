import React, { useState, useEffect } from 'react'
import { IonSelect, IonSelectOption } from '@ionic/react'

import './select.css'

interface SelectInterface {
  defaultValue?: any
  options: any[]
  multiple?: boolean
  fullWidth?: boolean
  onSelect: Function,
  className?: string,
  disabled?: boolean
}
export const Select = ({ defaultValue, options, multiple, fullWidth, onSelect, className, disabled }: SelectInterface) => {
  const [ value, setValue ] = useState(null)
  const [defaultValueWasSet, toggleDefaultValueWasSet] = useState(false)
  useEffect(() => {
    if (defaultValue && !defaultValueWasSet) {
      setValue(defaultValue)
      toggleDefaultValueWasSet(true)
    } else if (options.length && !defaultValueWasSet) {
      setValue(options[0].value)
      toggleDefaultValueWasSet(true)
    }
  }, [ defaultValue, toggleDefaultValueWasSet, defaultValueWasSet, options ])

  const select = (e: any) => {
    setValue(e.detail.value)
    onSelect && onSelect(e.detail.value)
  }
  const computedOptions = options || []

  return (
    <IonSelect
      interface='popover'
      multiple={multiple}
      value={value}
      onIonChange={select}
      disabled={disabled}
      className={`select border input-border rounded p-2 link flex ${fullWidth && 'w-full md:w-64'} ${className}`}
    >
      {computedOptions.map((option, id) =>
        <IonSelectOption
          key={id}
          value={option.value}
        >
          {option.text}
        </IonSelectOption>
      )}
    </IonSelect>
  )
}
