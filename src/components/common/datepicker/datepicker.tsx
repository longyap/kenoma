import React, { useState } from 'react'
import { IonDatetime } from '@ionic/react'
import { DatetimeChangeEventDetail } from '@ionic/core'
import dayjs from 'dayjs'
import { injectIntl, WrappedComponentProps } from 'react-intl'
import { DATEPICKER_MIN_MAX_FORMAT } from '../../../constants'

interface Datepicker extends WrappedComponentProps {
  onChange?: Function,
  options?: DatepickerOptions,
}
type DatepickerOptions = {
  displayFormat?: string,
  minuteValues?: string,
  min?: string,
  max?: string,
  cancelText?: string,
  doneText?: string
}
const defaultDatepickerOptions = {
  displayFormat: 'MM/DD/YYYY, HH:mm',
  minuteValues: '0,10,15,20,30,35,40,45,50,55',
  value: dayjs(new Date()).add(10, 'minute').format(DATEPICKER_MIN_MAX_FORMAT),
  max: dayjs(new Date()).add(100, 'year').format(DATEPICKER_MIN_MAX_FORMAT),
}

export const DatepickerComponent = ({ onChange, options, intl }: Datepicker) => {
  const valueState = useState(defaultDatepickerOptions.value)
  const datepickerOptions: DatepickerOptions = {
    cancelText: intl.formatMessage({ id: 'datepicker.cancel' }),
    doneText: intl.formatMessage({ id: 'datepicker.done' }), ...options, ...defaultDatepickerOptions}

  const datepickerHandle = (e: CustomEvent<DatetimeChangeEventDetail>) => {
    valueState[1](dayjs(e.detail.value + '').format(DATEPICKER_MIN_MAX_FORMAT))
    onChange && onChange(e.detail.value)
  }

  return (
    <IonDatetime
      display-format={datepickerOptions.displayFormat}
      minuteValues={datepickerOptions.minuteValues}
      onIonChange={datepickerHandle}
      min={datepickerOptions.min}
      max={datepickerOptions.max}
      value={valueState[0]}
      cancelText={datepickerOptions.cancelText}
      doneText={datepickerOptions.doneText}
    />
  )
}

export const Datepicker = injectIntl(DatepickerComponent)
