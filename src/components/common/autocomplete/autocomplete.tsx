import React, { Component } from 'react'
import { Popover, openHandler } from '../popover/popover'
import Account from '../../../entities/account'
import { ProfileCardSm } from '../../profile_card/profile_card_sm'
import Icons from '../icons/icons'
import { AUTOCOMPLETE_TYPES } from '../../../constants'
import { EmojiSearchResult } from '../../emoji_search_result/emoji_search_result'
import { EmojiClickEventDetail } from 'emoji-picker-element/shared'

interface AutocompleteType {
  children?: any,
  ref: any,
  _component?: any,
  results: Account[] | EmojiClickEventDetail[],
  onClose?: Function,
  onSelect?: Function,
  onSuggestionsUpdate?: Function,
  keydown?: any,
  isLoading?: boolean,
  actions?: ('mute' | 'block' | 'follow')[],
  width?: string
}
interface AutocompleteState {
  selectedIdx: number,
  isOpen: boolean,
  type: string,
}

export class Autocomplete extends Component<AutocompleteType, AutocompleteState> {
  constructor(props: AutocompleteType) {
    super(props)
    this.state = {
      selectedIdx: 0,
      isOpen: false,
      type: AUTOCOMPLETE_TYPES.users.name,
    }
  }
  autocomplete: React.RefObject<any> = React.createRef()
  resultsContainer: React.RefObject<any> = React.createRef()
  open = async (e: any, type: string) => {
    this.setState({ isOpen: true, type })
    await openHandler(e, this.autocomplete)
  }
  close = () => {
    this.autocomplete.current.close()
    this.setState({
      selectedIdx: 0,
      isOpen: false,
      type: '',
    })
  }
  onSelect = (item?: Account | EmojiClickEventDetail) => {
    const { onSelect, results } = this.props
    const { selectedIdx, type } = this.state

    onSelect && onSelect({ res: item || results[selectedIdx], type })
    this.close()
  }
  setScrollPosition = () => {
    const resultNodesList = this.resultsContainer.current.querySelectorAll(AUTOCOMPLETE_TYPES[this.state.type].searchComponentClass)
    const container = document.querySelectorAll('.autocomplete .popover-content')

    container[0].scrollTop  = resultNodesList[this.state.selectedIdx].offsetTop
  }
  moveUp = () => {
    const { selectedIdx } = this.state

    this.setState({ selectedIdx: selectedIdx - 1 < 0
      ? this.props.results.length - 1
      : selectedIdx - 1 }, this.setScrollPosition)
  }

  moveDown = () => {
    const { selectedIdx } = this.state
    this.setState({ selectedIdx: selectedIdx + 1 === this.props.results.length
      ? 0
      : selectedIdx + 1 }, this.setScrollPosition)
  }

  onKeyDown = (e: any) => {
    if (!this.state.isOpen) return
    e.persist()
    switch (e.key) {
      case 'ArrowDown': {
        e.preventDefault()
        this.moveDown()
        break;
      }
      case 'ArrowUp': {
        e.preventDefault()
        this.moveUp()
        break;
      }
      case 'Enter': {
        e.preventDefault()
        this.onSelect()
        break;
      }
      case ' ': {
        this.close()
      }
    }
  }

  renderUserResult = (user: Account, key: number) => {
    const { actions, onSuggestionsUpdate } = this.props

    return <ProfileCardSm
      onClick={() => this.onSelect(user)}
      key={key}
      account={user}
      disableLink={true}
      highlighted={key === this.state.selectedIdx}
      actions={actions}
      onActionButtonClick={onSuggestionsUpdate}
    />
  }

  renderEmojiResult = (emoji: EmojiClickEventDetail, key: number) => (<EmojiSearchResult
    onClick={() => this.onSelect(emoji)}
    key={key}
    emoji={emoji}
    highlighted={key === this.state.selectedIdx}
  />)

  render() {
    const { results, onClose, isLoading, children, width } = this.props
    const { type } = this.state

    return (
      <div onKeyDown={this.onKeyDown}>
        {children}
        <Popover
          ref={this.autocomplete}
          onClose={onClose}
          showBackdrop={false}
          cssClass='autocomplete'
          width={width}
        >
          {isLoading
            && <div className='w-6 h-6 mx-auto my-2 text-secondary'>
              <Icons.Cog className='w-6 h-6 a-loader-spin' />
            </div>}
            {type === AUTOCOMPLETE_TYPES.users.name && <ul ref={this.resultsContainer}>
              {(results as Account[]).map((item: Account, idx: number) => this.renderUserResult(item, idx))}
            </ul>}
            {type === AUTOCOMPLETE_TYPES.emojis.name && <ul ref={this.resultsContainer}>
              {(results as EmojiClickEventDetail[]).map((item: EmojiClickEventDetail, idx: number) => this.renderEmojiResult(item, idx))}
            </ul>}
        </Popover>
      </div>

    )
  }
}
