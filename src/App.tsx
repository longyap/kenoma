import React, { useState, useEffect } from 'react';
import { Route } from 'react-router-dom';
import { IonApp } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import './main.css';
import { LoginLayout } from './components/layout/login_layout';
import { connect } from 'react-redux';
import { OauthCallback } from './components/oauth_callback/oauth_callback';
import { Layout } from './components/layout/layout';
import { IsMobileContext } from './isMobileContext';
import { APP_ID } from './constants';
import Notification from './entities/notification'

export const THREAD_URL = '/notice/:id';

const App: React.FC<{loggedIn: boolean, theme: string, unread_notifications_count: number}> = ({ loggedIn, theme, unread_notifications_count }) => {
  const [isMobile, switchLayout] = useState(window.innerWidth < 768)

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return function cleanup () {
      window.removeEventListener('resize', handleWindowSizeChange)
    }
  })

  useEffect(() => {
    document.title = unread_notifications_count > 0 ? `(${unread_notifications_count}) Kenoma` : `Kenoma`
  }, [unread_notifications_count])

  const handleWindowSizeChange = () => {
    switchLayout(window.innerWidth < 768)
  }

  const app = (
    <div className='h-full max-h-full overflow-y-auto flex flex-col text text-base'>
      <Layout />
    </div>
  )
  const instanceSelection = (
    <div>
      <Route path='/oauth-callback' component={OauthCallback} />
      <LoginLayout/>
    </div>
  )

  return (
    <IonApp className={ theme === 'pleroma-dark' ? 'dark' : '' }>
      <IonReactRouter>
        <IsMobileContext.Provider value={{isMobile}}>
          {loggedIn ? app : instanceSelection}
        </IsMobileContext.Provider>
      </IonReactRouter>
    </IonApp>
  );
};

const mapStateToProps = ({ oauth, config, notifications: { notificationsByIds} }
  : { oauth: any, config: any, notifications: { notificationsByIds: {[key: string]: Notification}} }) => ({
  loggedIn: !!oauth.userToken,
  theme: (config[APP_ID] || { theme: 'pleroma-light' }).theme,
  unread_notifications_count: Object.values(notificationsByIds).filter(item => !item.pleroma.is_seen).length
})

export default connect(mapStateToProps)(App);

