import { generateProfileLink, generateTagLink } from '../../utils/linkGenerators'

describe('generateProfileLink', () => {
  it('external user', () => {
    const link = generateProfileLink('1', 'alice@instance')
    expect(link).toBe('/users/1')
  })
  it(`screen name didn't sent`, () => {
    const link = generateProfileLink('1')
    expect(link).toBe('/users/1')
  })
  it('internal user', () => {
    const link = generateProfileLink('1', 'alice')
    expect(link).toBe('/users/alice')
  })
})

describe('generateTagLink', () => {
  it('should return tag link', () => {
    const link = generateTagLink('cofe')
    expect(link).toBe('/tag/cofe')
  })
})