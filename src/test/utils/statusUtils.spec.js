import { createAutocompleteRequest, insertAutocompleteResult, getStatusMentionsList } from '../../utils/statusUtils'
import Mention from '../../entities/mention'
const autocompleteType = 'users'

describe('createAutocompleteRequest', () => {
  it('should return nothing on empty request', () => {
    const res = createAutocompleteRequest('', 0, autocompleteType)
    expect(res).toBe(null)
  })
  it('should return name at the start of the string', () => {
    const res = createAutocompleteRequest('@nd', 0, autocompleteType)
    expect(res).toEqual({ request: 'nd', type: autocompleteType })
  })
  it('should return name@instance', () => {
    const res = createAutocompleteRequest('hi @lain@lain', 13, autocompleteType)
    expect(res).toEqual({ request: 'lain@lain', type: autocompleteType })
  })
  it('should return the last entered word', () => {
    const res = createAutocompleteRequest('hi @lain@lain @nd', 15, autocompleteType)
    expect(res).toEqual({ request: 'nd', type: autocompleteType })
  })
  it('should return current word', () => {
    const res = createAutocompleteRequest('hi @lain@lain @nd', 10, autocompleteType)
    expect(res).toEqual({ request: 'lain@lain', type: autocompleteType })
  })
  it('should return part of word after autocomplete char', () => {
    const res = createAutocompleteRequest('party:pa time', 8, autocompleteType)
    expect(res).toEqual({ request: 'pa', type: 'emojis' })
  })
})

describe('insertAutocompleteResult', () => {
  it('should insert result to the end of the string', () => {
    const res = insertAutocompleteResult('How are you, @la', 'lain', 16, autocompleteType)
    expect(res.text).toBe('How are you, @lain ')
    expect(res.selectionIndex).toBe(19)
  })
  it('should insert result in the middle of the string', () => {
    const res = insertAutocompleteResult('test @nas string', 'nastassiad', 9, autocompleteType)
    expect(res.text).toBe('test @nastassiad string')
    expect(res.selectionIndex).toBe(17)
  })
  it('should insert the second autocomplete result', () => {
    const res = insertAutocompleteResult('test @test @nas string', 'nastassiad', 15, autocompleteType)
    expect(res.text).toBe('test @test @nastassiad string')
    expect(res.selectionIndex).toBe(23)
  })
  it('should insert emoji', () => {
    const res = insertAutocompleteResult('test @test :sm string', 'smile', 14, 'emojis')
    expect(res.text).toBe('test @test smile string')
    expect(res.selectionIndex).toBe(17)
  })
  it('should insert emoji into text (simpleInsert: true) from emojis panel', () => {
    const res = insertAutocompleteResult('123456', 'smile', 3, 'emojis', true)
    expect(res.text).toBe('123 smile 456')
    expect(res.selectionIndex).toBe(10)
  })
})


describe('getStatusMentionsList', () => {
  it('should build mentions list for repeated status, which contains mention of another user', () => {
    const user1 = new Mention({ acct: 'alice', id: '1' })
    const user2 = new Mention({ acct: 'Bob', id: '2' })
    const user3 = new Mention({ acct: 'John', id: '3' })
    const status = {
      mentions: [user1, user2],
      reblog: {
        account: user3,
        pleroma: {
          in_reply_to_account_acct: 'Nike'
        },
        in_reply_to_account_id: '4'
      }
    }
    const res = getStatusMentionsList(status)
    const expectedRes = [user1, user2, user3, new Mention({ acct: 'Nike', id: '4' })]
    expect(res).toEqual(expectedRes)
  })
  it('should build mentions list for repeated status', () => {
    const user1 = new Mention({ acct: 'alice', id: '1' })
    const user2 = new Mention({ acct: 'Bob', id: '2' })
    const user3 = new Mention({ acct: 'John', id: '3' })
    const status = {
      mentions: [user1, user2],
      reblog: {
        account: user3
      }
    }
    const res = getStatusMentionsList(status)
    const expectedRes = [user1, user2, user3]
    expect(res).toEqual(expectedRes)
  })
  it('should build mentions list for repeated status with mentions in original status', () => {
    const user1 = new Mention({ acct: 'alice', id: '1' })
    const user2 = new Mention({ acct: 'Bob', id: '2' })
    const user3 = new Mention({ acct: 'John', id: '3' })
    const user4 = new Mention({ acct: 'Nike', id: '4' })
    const status = {
      mentions: [user1, user2],
      reblog: {
        account: user3,
        mentions: [user4]
      }
    }
    const res = getStatusMentionsList(status)
    const expectedRes = [user1, user2, user3, user4]
    expect(res).toEqual(expectedRes)
  })
  it('should build mentions list for repeated status without any other mentions', () => {
    const user3 = new Mention({ acct: 'John', id: '3' })
    const status = {
      reblog: {
        account: user3
      }
    }
    const res = getStatusMentionsList(status)
    const expectedRes = [user3]
    expect(res).toEqual(expectedRes)
  })
  it('build mentions for simple status', () => {
    const user1 = new Mention({ acct: 'alice', id: '1' })
    const user2 = new Mention({ acct: 'Bob', id: '2' })
    const user3 = new Mention({ acct: 'John', id: '3' })
    const status = {
      mentions: [user1, user2],
      account: user3
    }
    const res = getStatusMentionsList(status)
    const expectedRes = [user1, user2, user3]
    expect(res).toEqual(expectedRes)
  })
})
