import { mentionMatchesUrl } from '../../utils/matchers'

describe('mentionMatchesUrl', () => {
  it('should match mention of internal user with composite name', () => {
    const res = mentionMatchesUrl({ acct: 'alice@instance' }, 'https://instance/users/alice', 'instance')
    expect(res).toBe(true)
  })
  it('should match mention of internal user', () => {
    const res = mentionMatchesUrl({ acct: 'alice' }, 'https://instance/users/alice', 'instance')
    expect(res).toBe(true)
  })
  it('should not match mention of external user', () => {
    const res = mentionMatchesUrl({ acct: 'alice' }, 'https://instance/users/alice', 'anotherinstance')
    expect(res).toBe(false)
  })
  it('should not match mention of external user but with the same name', () => {
    const res = mentionMatchesUrl({ acct: 'alice@anotherInstance' }, 'https://instance/users/alice', 'anotherinstance')
    expect(res).toBe(false)
  })
})
