import { getDomainSuggestions } from '../../utils/suggestor'

// TODO: getUsersSuggestions

describe('getDomainSuggestions', () => {
  it('should return a list of suitable domains', () => {
    const domains = [
      'social.test',
      'test',
      'Social.test.l',
      'Test.Social.domain',
      'Test.social.domain',
      'so.test.cial',
    ]
    const suggestions = getDomainSuggestions('soc', domains)
    expect(suggestions).toStrictEqual([
      'social.test',
      'Social.test.l',
      'Test.Social.domain',
      'Test.social.domain'
    ])
  })
})
