import { Component } from "react"
import Icons from "./components/common/icons/icons"

export const APP_ID = 'kenoma'

export const GITLAB_REPO_URL = 'https://git.pleroma.social/lambadalambda/kenoma/commit'

export const STATUS_VISIBILITY_OPTIONS: { value: string, text?: string, textId: string, icon: Component }[] = [
  { value: 'public', textId: 'postStatusForm.scope.public', icon: Icons.Globe },
  { value: 'unlisted', textId: 'postStatusForm.scope.unlisted', icon: Icons.OpenLock },
  { value: 'private', textId: 'postStatusForm.scope.private', icon: Icons.Lock },
  { value: 'direct', textId: 'postStatusForm.scope.direct', icon: Icons.Envelope }
]

export const DATEPICKER_MIN_MAX_FORMAT = 'YYYY-MM-DDTHH:mm'

export const AUTOCOMPLETE_TYPES: { [key: string]: { name: string, char: string, searchComponentClass: string } } = {
  users: { name: 'users', char: '@', searchComponentClass: '.profile-search-result' },
  emojis: { name: 'emojis', char: ':', searchComponentClass: '.emoji-search-result' }
}

export const SEARCH_DEBOUNCE_MS = 200
